using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;


public class MoPubAndroidEventListener : MonoBehaviour
{

	void OnEnable()
	{
		// Listen to all events for illustration purposes
		MoPubAndroidManager.onAdLoadedEvent += onAdLoadedEvent;
		MoPubAndroidManager.onAdFailedEvent += onAdFailedEvent;
		MoPubAndroidManager.onAdClickedEvent += onAdClickedEvent;
		MoPubAndroidManager.onAdExpandedEvent += onAdExpandedEvent;
		MoPubAndroidManager.onAdCollapsedEvent += onAdCollapsedEvent;
		MoPubAndroidManager.onInterstitialLoadedEvent += onInterstitialLoadedEvent;
		MoPubAndroidManager.onInterstitialFailedEvent += onInterstitialFailedEvent;
		MoPubAndroidManager.onInterstitialShownEvent += onInterstitialShownEvent;
		MoPubAndroidManager.onInterstitialClickedEvent += onInterstitialClickedEvent;
		MoPubAndroidManager.onInterstitialDismissedEvent += onInterstitialDismissedEvent;
	}


	void OnDisable()
	{
		// Remove all event handlers
		MoPubAndroidManager.onAdLoadedEvent -= onAdLoadedEvent;
		MoPubAndroidManager.onAdFailedEvent -= onAdFailedEvent;
		MoPubAndroidManager.onAdClickedEvent -= onAdClickedEvent;
		MoPubAndroidManager.onAdExpandedEvent -= onAdExpandedEvent;
		MoPubAndroidManager.onAdCollapsedEvent -= onAdCollapsedEvent;
		MoPubAndroidManager.onInterstitialLoadedEvent -= onInterstitialLoadedEvent;
		MoPubAndroidManager.onInterstitialFailedEvent -= onInterstitialFailedEvent;
		MoPubAndroidManager.onInterstitialShownEvent -= onInterstitialShownEvent;
		MoPubAndroidManager.onInterstitialClickedEvent -= onInterstitialClickedEvent;
		MoPubAndroidManager.onInterstitialDismissedEvent -= onInterstitialDismissedEvent;
	}



	void onAdLoadedEvent()
	{
		Debug.Log( "onAdLoadedEvent" );
	}

	void onAdFailedEvent()
	{
		Debug.Log( "onAdFailedEvent" );
	}
	
	void onAdClickedEvent()
	{
		Debug.Log( "onAdClickedEvent" );
	}
	
	void onAdExpandedEvent()
	{
		Debug.Log( "onAdExpandedEvent" );
	}
	
	void onAdCollapsedEvent()
	{
		Debug.Log( "onAdCollapsedEvent" );
	}

	void onInterstitialLoadedEvent()
	{
		Debug.Log( "onInterstitialLoadedEvent" );
	}

	void onInterstitialFailedEvent()
	{
		Debug.Log( "onInterstitialFailedEvent" );
	}
	
	void onInterstitialShownEvent()
	{
		Debug.Log( "onInterstitialShownEvent" );
	}
	
	void onInterstitialClickedEvent()
	{
		Debug.Log( "onInterstitialClickedEvent" );
	}
	
	void onInterstitialDismissedEvent()
	{
		Debug.Log( "onInterstitialDismissedEvent" );
	}
}


