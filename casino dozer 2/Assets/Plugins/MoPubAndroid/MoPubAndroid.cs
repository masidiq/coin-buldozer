using UnityEngine;
using System.Collections;
using System.Collections.Generic;


#if UNITY_ANDROID
public enum MoPubAdPlacement
{
	TopLeft,
	TopCenter,
	TopRight,
	Centered,
	BottomLeft,
	BottomCenter,
	BottomRight
}


public class MoPubAndroid
{
	private static AndroidJavaObject _plugin;
	
		
	static MoPubAndroid()
	{
		if( Application.platform != RuntimePlatform.Android )
			return;

		// find the plugin instance
		using( var pluginClass = new AndroidJavaClass( "com.mopub.mobileads.MoPubUnityPlugin" ) )
			_plugin = pluginClass.CallStatic<AndroidJavaObject>( "instance" );
	}
	

	// Creates a banner with the given ad unit placed based on the position parameter
	public static void createBanner( string adUnitId, MoPubAdPlacement position )
	{
		if( Application.platform != RuntimePlatform.Android )
			return;
		
		_plugin.Call( "showBanner", adUnitId, (int)position );
	}


	// Hides/shows the ad banner
	public static void hideBanner( bool shouldHide )
	{
		if( Application.platform != RuntimePlatform.Android )
			return;
		
		_plugin.Call( "hideBanner", shouldHide );
	}


	// Sets the search keywords for the banner
	public static void setBannerKeywords( string keywords )
	{
		if( Application.platform != RuntimePlatform.Android )
			return;
		
		_plugin.Call( "setBannerKeywords", keywords );
	}


	// Destroys the banner and removes it from view
	public static void destroyBanner()
	{
		if( Application.platform != RuntimePlatform.Android )
			return;
		
		_plugin.Call( "destroyBanner" );
	}


	// Starts loading an interstitial ad
	public static void requestInterstitalAd( string adUnitId )
	{
		if( Application.platform != RuntimePlatform.Android )
			return;
		
		_plugin.Call( "requestInterstitalAd", adUnitId );
	}


	// If an interstitial ad is loaded this will take over the screen and show the ad
	public static void showInterstitalAd()
	{
		if( Application.platform != RuntimePlatform.Android )
			return;
		
		_plugin.Call( "showInterstitalAd" );
	}


	// Reports an app download to MoPub
	public static void reportApplicationOpen()
	{
		if( Application.platform != RuntimePlatform.Android )
			return;
		
		_plugin.Call( "reportApplicationOpen" );
	}

}
#endif
