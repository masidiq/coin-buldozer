using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;


public class MoPubAndroidManager : MonoBehaviour
{
	// Fired when a new banner ad is loaded
	public static event Action onAdLoadedEvent;
	
	// Fired when a banner ad fails to load
	public static event Action onAdFailedEvent;
	
	// Fired when a banner ad is clicked
	public static event Action onAdClickedEvent;
	
	// Fired when a banner ad expands to encompass a greater portion of the screen
	public static event Action onAdExpandedEvent;
	
	// Fired when a banner ad collapses back to its initial size
	public static event Action onAdCollapsedEvent;
	
	// Fired when an interstitial ad is loaded
	public static event Action onInterstitialLoadedEvent;
	
	// Fired when an interstitial ad fails to load
	public static event Action onInterstitialFailedEvent;
	
	// Fired when an interstitial ad is displayed
	public static event Action onInterstitialShownEvent;
	
	// Fired when an interstitial ad is clicked
	public static event Action onInterstitialClickedEvent;
	
	// Fired when an interstitial ad is dismissed
	public static event Action onInterstitialDismissedEvent;


	void Awake()
	{
		// Set the GameObject name to the class name for easy access from Obj-C
		gameObject.name = this.GetType().ToString();
		DontDestroyOnLoad( this );
	}


	public void onAdLoaded( string empty )
	{
		if( onAdLoadedEvent != null )
			onAdLoadedEvent();
	}

	public void onAdFailed( string empty )
	{
		if( onAdFailedEvent != null )
			onAdFailedEvent();
	}
	
	public void onAdClicked( string empty )
	{
		if ( onAdClickedEvent != null )
			onAdClickedEvent();
	}
	
	public void onAdExpanded( string empty )
	{
		if ( onAdExpandedEvent != null )
			onAdExpandedEvent();
	}
	
	public void onAdCollapsed( string empty )
	{
		if ( onAdCollapsedEvent != null )
			onAdCollapsedEvent();
	}

	public void onInterstitialLoaded( string empty )
	{
		if( onInterstitialLoadedEvent != null )
			onInterstitialLoadedEvent();
	}

	public void onInterstitialFailed( string empty )
	{
		if( onInterstitialFailedEvent != null )
			onInterstitialFailedEvent();
	}

	public void onInterstitialShown( string empty )
	{
		if ( onInterstitialShownEvent != null )
			onInterstitialShownEvent();
	}
	
	public void onInterstitialClicked( string empty )
	{
		if ( onInterstitialClickedEvent != null )
			onInterstitialClickedEvent();
	}
	
	public void onInterstitialDismissed( string empty )
	{
		if ( onInterstitialDismissedEvent != null )
			onInterstitialDismissedEvent();
	}
}

