//
//  ALAdDisplayWrapper.m
//  AppLovin-iPhone-SDK
//
//  Created by Matt Szaro on 11/6/13.
//
//

#import "ALAdDisplayWrapper.h"

@implementation ALAdDisplayWrapper
@synthesize interstitialCurrentlyShowing;

-(void) ad:(ALAd *)ad wasDisplayedIn:(UIView *)view
{
    interstitialCurrentlyShowing = true;
}

-(void) ad:(ALAd *)ad wasHiddenIn:(UIView *)view
{
    interstitialCurrentlyShowing = false;
}

-(void) ad:(ALAd *)ad wasClickedIn:(UIView *)view {}

@end
