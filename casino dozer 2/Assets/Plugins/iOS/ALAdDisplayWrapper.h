//
//  ALAdDisplayWrapper.h
//  AppLovin-iPhone-SDK
//
//  Created by Matt Szaro on 11/6/13.
//
//

#import <Foundation/Foundation.h>
#import "ALAdDisplayDelegate.h"

@interface ALAdDisplayWrapper : NSObject <ALAdDisplayDelegate>

// Please note that this property is intentionally c bool and not Obj-c BOOL.
// This is done to meet Unity requirements. Do not change the type.

@property (assign, atomic) bool interstitialCurrentlyShowing;

@end
