﻿using UnityEngine;
using System.Collections;

public class StringData
{
		public static string gameName = "Casino Dozer";
		public static string freeGames;
		public static string freeCoins;
}

public class Facebook_API
{
	public static string facebook_appid_iphone = "985892711440342";
	public static string facebook_secret_iphone = "e4af726960ec6027c361891dac7f2001";
	public static string facebook_appid_ipad = "985892711440342";
	public static string facebook_secret_ipad = "e4af726960ec6027c361891dac7f2001";
	public static string facebook_appid_android = "985892711440342";
	public static string facebook_secret_android = "e4af726960ec6027c361891dac7f2001";
	public static string facebook_appid_amazon = "985892711440342";
	public static string facebook_secret_amazon = "e4af726960ec6027c361891dac7f2001";
	public static string facebook_appid_amazonhd = "985892711440342";
	public static string facebook_secret_amozonhd = "e4af726960ec6027c361891dac7f2001";
	public static string osx_facebook_url = "https://www.facebook.com/vegasdozer";
}

public class Playhaven_API
{
	public static string playhaven_appid_iphone = "182060";
	public static string playhaven_signature_iphone = "88e893378132452ba1f1400f5a83caad";
	public static string playhaven_appid_ipad = "182060";
	public static string playhaven_signature_ipad = "88e893378132452ba1f1400f5a83caad";
	public static string playhaven_appid_android = "182058";
	public static string playhaven_signature_android = "ab6eea520d2d40dbb0eb5b3656af47a5";
}

public class Chartboost_API
{
	public static string chartboost_appid_iphone = "549e53cec909a63591b70b9f";
	public static string chartboost_signature_iphone = "5da72959d430ef283bb1ce1ef61b5fe29fa2a70d";
	public static string chartboost_appid_ipad = "549e53cec909a63591b70b9f";
	public static string chartboost_signature_ipad = "5da72959d430ef283bb1ce1ef61b5fe29fa2a70d";
	public static string chartboost_appid_android = "549e535f04b0160e3aaec7ba";
	public static string chartboost_signature_android = "b2f5129a5eef245b939d01b883ffd424c78aac8d";
		public static string chartboost_appid_amazon = "51257bd417ba475c50000039";
		public static string chartboost_signature_amazon = "4c2399d3031c862bc6f0de0959e0d70d1d1c1639";
		public static string chartboost_appid_amazonhd = "51257b9d17ba47ba5000002b";
		public static string chartboost_signature_amazonhd = "37d714940eaae22bf0cdc81128abd4f64430f8de";
		public static string[] chartboost_names = new string[10] {"Parrot", "Hook", "Pirate",
				"Watch", "Hat", "Chest", "Shark", "Ship",
				"Monkey", "Mermaid"
		};
}

public class Mopub_API
{
	public static string mopub_appid_iphone = "6759b1c55ab149faaddf4b4a4d6c36c3";
	public static string mopub_appid_ipad = "6759b1c55ab149faaddf4b4a4d6c36c3";
	public static string mopub_appid_android = "bc5a73a9d503489581728481325eac6c";
}

public class Flurry_API
{
	public static string flurry_appkey_iphone = "HDS5PQCX54YH754BSQFN";
	public static string flurry_appkey_ipad = "6CT7W73DP6SWMYGCV2FX";
	    public static string flurry_appkey_android = "453WBV767R33H4F4BFQC";
	    public static string flurry_appkey_amazon = "VTW9FD6YRC334WXXRN6G";
	    public static string flurry_appkey_amazonhd = "STB37MC3KZNF8PNCVQJ4";
}

public class Inapp_API
{
	public static string inapp_100coins_iphone = "com.microappsion.vegasdozer.120coins";
	public static string inapp_350coins_iphone = "com.microappsion.vegasdozer.455coins";
	public static string inapp_750coins_iphone = "com.microappsion.vegasdozer.1050coins";
	public static string inapp_1200coins_iphone = "com.microappsion.vegasdozer.1800coins";
	public static string inapp_100coins_iphone_paid = "com.microappsion.vegasdozer.120coins";
	public static string inapp_350coins_iphone_paid = "com.microappsion.vegasdozer.455coins";
	public static string inapp_750coins_iphone_paid = "com.microappsion.vegasdozer.1050coins";
	public static string inapp_1200coins_iphone_paid = "ccom.microappsion.vegasdozer.1800coins";
	public static string inapp_100coins_ipad = "com.microappsion.vegasdozer.120coins";
	public static string inapp_350coins_ipad = "com.microappsion.vegasdozer.455coins";
	public static string inapp_750coins_ipad = "com.microappsion.vegasdozer.1050coins";
	public static string inapp_1200coins_ipad = "com.microappsion.vegasdozer.1800coins";
	public static string inapp_100coins_ipad_paid = "com.microappsion.vegasdozer.120coins";
	public static string inapp_350coins_ipad_paid = "com.microappsion.vegasdozer.455coins";
	public static string inapp_750coins_ipad_paid = "com.microappsion.vegasdozer.1050coins";
	public static string inapp_1200coins_ipad_paid = "com.microappsion.vegasdozer.1800coins";
	public static string inapp_100coins_google = "com.microappsion.vegasdozer.120coins";
	public static string inapp_350coins_google = "com.microappsion.vegasdozer.455coins";
	public static string inapp_750coins_google = "com.microappsion.vegasdozer.1050coins";
	public static string inapp_1200coins_google = "com.microappsion.vegasdozer.1800coins";
	public static string inapp_100coins_amazon = "com.microappsion.vegasdozer.coins120";
	public static string inapp_350coins_amazon = "com.microappsion.vegasdozer.coins455";
	public static string inapp_750coins_amazon = "com.microappsion.vegasdozer.coins1050";
	public static string inapp_1200coins_amazon = "com.microappsion.vegasdozer.coins1800";
	public static string inapp_100coins_amazon_paid = "com.microappsion.vegasdozer.coins120";
	public static string inapp_350coins_amazon_paid = "com.microappsion.vegasdozer.coins455";
	public static string inapp_750coins_amazon_paid = "com.microappsion.vegasdozer.coins1050";
	public static string inapp_1200coins_amazon_paid = "com.microappsion.vegasdozer.coins1800";
	public static string inapp_key_android = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAno2yFv/jjDI5OSBdGvuelcKq/61GSW+2K7m2myP3kAr0qTZVj0nV3S9yhXwqRmiAXUq6cahuF5/TV6fW1JouKyeV9JZNG9zPl4Y0slhN2XnynkOW6PFb8Laz0XIBLOZd0ijJXETdDashQM3skKwbsasw5vMzRlS5AWsGhsIxUwj3CrqNHsa/RtUnVVWx2xKfh8vg5pVoDBw5Psx+EKpL31vex6imHO90kP0OrKKzSlgk5HFsuda1UKc0pz7uiZT25P77HTWov7BfUpy5JkJ3m0h1LDuqMfiffo9B9TWNefXDCgpmLDhnJqGggNMMGWvJeUzozR8VcuhO46WnD3pKJwIDAQAB";
		public static string appid_iphone = "605993057";
		public static string appid_ipad = "605994328";
		public static string appid_iphone_paid = "622799679";
		public static string appid_ipad_paid = "622804819";
}

public class Playnomics_API
{
		public static string playnomics_appid_iphone = "123456";
		public static string playnomics_appid_ipad = "123456";
		public static string playnomics_appid_android = "123456";
		public static string playnomics_appid_amazon = "123456";
		public static string playnomics_appid_amazonhd = "123456";
}

public class Leaderboard_API
{
	static public string leaderboard_iphone = "com.appgroup.dozerfrenzy.Leaderboard";
	static public string leaderboard_iphone_paid = "com.appgroup.dozerfrenzy.Leaderboard";
	static public string leaderboard_ipad = "com.appgroup.dozerfrenzy.Leaderboard";
	static public string leaderboard_ipad_paid = "com.appgroup.dozerfrenzy.Leaderboard";
		static public string leaderboard_android = "CgkIqoOwio8bEAIQBw";
		static public string leaderboard_amazon = "com_appgroup_dozerfrenzy_leaderboard";
		static public string leaderboard_amazon_paid = "com_appgroup_dozerfrenzypro_leaderboard";
		static public string leaderboard_osx = "com.appgroup.dozerfrenzyMAC.Leaderboard";
}
