using UnityEngine;
using System.Collections.Generic;
using Prime31;


public class MoPubGUIManager : MonoBehaviourGUI
{
#if UNITY_IPHONE
	
	// Test ad unit Id. Replace with your own!
	private string _adUnitId = "agltb3B1Yi1pbmNyDAsSBFNpdGUYkaoMDA";
	

	void OnGUI()
	{
		beginColumn();
	
	
		if( GUILayout.Button( "Create Banner (bottom right)" ) )
		{
			MoPubBinding.createBanner( MoPubBannerType.Size320x50, MoPubAdPosition.BottomRight, _adUnitId );
		}
		
		
		if( GUILayout.Button( "Create Banner (top center)" ) )
		{
			MoPubBinding.createBanner( MoPubBannerType.Size320x50, MoPubAdPosition.TopCenter, _adUnitId );
		}
	
	
		if( GUILayout.Button( "Destroy Banner" ) )
		{
			MoPubBinding.destroyBanner();
		}
	
	
		if( GUILayout.Button( "Show Banner" ) )
		{
			MoPubBinding.showBanner( true );
		}
	
	
		if( GUILayout.Button( "Hide Banner" ) )
		{
			MoPubBinding.showBanner( false );
		}

		
	
		endColumn( true );
		
		
		if( GUILayout.Button( "Request Interstitial" ) )
		{
			MoPubBinding.requestInterstitialAd( _adUnitId, null );
		}
		
		
		if( GUILayout.Button( "Show Interstitial" ) )
		{
			MoPubBinding.showInterstitialAd( _adUnitId );
		}


		if( GUILayout.Button( "Report App Open" ) )
		{
			MoPubBinding.reportApplicationOpen( "ITUNES_APP_ID" );
		}
		
		
		if( GUILayout.Button( "Refresh Ad" ) )
		{
			MoPubBinding.refreshAd( "coffee" );
		}
		
		
		if( GUILayout.Button( "Enable Location Support" ) )
		{
			MoPubBinding.enableLocationSupport( true );
		}
		
		endColumn();
	}
#endif
}
