using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;


public class MoPubEventListener : MonoBehaviour
{
#if UNITY_IPHONE
	void OnEnable()
	{
		// Listen to all events for illustration purposes
		MoPubManager.adViewDidFailToLoadAdEvent += adViewDidFailToLoadAdEvent;
		MoPubManager.adViewDidLoadAdEvent += adViewDidLoadAdEvent;
		MoPubManager.interstitialDidLoadAdEvent += interstitialDidLoadAdEvent;
		MoPubManager.interstitialDidFailToLoadAdEvent += interstitialDidFailToLoadAdEvent;
		MoPubManager.interstitialDidDismissEvent += interstitialDidDismissEvent;
		MoPubManager.interstitialDidExpireEvent += interstitialDidExpireEvent;
	}


	void OnDisable()
	{
		// Remove all event handlers
		MoPubManager.adViewDidFailToLoadAdEvent -= adViewDidFailToLoadAdEvent;
		MoPubManager.adViewDidLoadAdEvent -= adViewDidLoadAdEvent;
		MoPubManager.interstitialDidLoadAdEvent -= interstitialDidLoadAdEvent;
		MoPubManager.interstitialDidFailToLoadAdEvent -= interstitialDidFailToLoadAdEvent;
		MoPubManager.interstitialDidDismissEvent -= interstitialDidDismissEvent;
		MoPubManager.interstitialDidExpireEvent -= interstitialDidExpireEvent;
	}



	void adViewDidFailToLoadAdEvent()
	{
		Debug.Log( "adViewDidFailToLoadAdEvent" );
	}


	void adViewDidLoadAdEvent()
	{
		Debug.Log( "adViewDidLoadAdEvent" );
	}


	void interstitialDidLoadAdEvent()
	{
		Debug.Log( "interstitialDidLoadAdEvent" );
	}


	void interstitialDidFailToLoadAdEvent()
	{
		Debug.Log( "interstitialDidFailToLoadAdEvent" );
	}
	
	
	void interstitialDidDismissEvent()
	{
		Debug.Log( "interstitialDidDismissEvent" );
	}
	
	
	void interstitialDidExpireEvent()
	{
		Debug.Log( "interstitialDidExpireEvent" );
	}
#endif
}


