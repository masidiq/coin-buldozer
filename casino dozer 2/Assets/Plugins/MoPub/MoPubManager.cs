using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Prime31;


#if UNITY_IPHONE
public class MoPubManager : AbstractManager
{
	// Fired when an ad fails to load for the banner
	public static event Action adViewDidFailToLoadAdEvent;
	
	// Fired when an ad loads in the banner
	public static event Action adViewDidLoadAdEvent;
	
	// Fired when an interstitial ad is loaded and ready to be shown
	public static event Action interstitialDidLoadAdEvent;
	
	// Fired when an interstitial ad fails to load
	public static event Action interstitialDidFailToLoadAdEvent;
	
	// Fired when an interstitial ad is dismissed
	public static event Action interstitialDidDismissEvent;
	
	// Fired when an interstitial ad expires
	public static event Action interstitialDidExpireEvent;


	static MoPubManager()
	{
		AbstractManager.initialize( typeof( MoPubManager ) );
	}


	public void adViewDidFailToLoadAd( string empty )
	{
		if( adViewDidFailToLoadAdEvent != null )
			adViewDidFailToLoadAdEvent();
	}


	public void adViewDidLoadAd( string empty )
	{
		if( adViewDidLoadAdEvent != null )
			adViewDidLoadAdEvent();
	}


	public void interstitialDidLoadAd( string empty )
	{
		if( interstitialDidLoadAdEvent != null )
			interstitialDidLoadAdEvent();
	}


	public void interstitialDidFailToLoadAd( string empty )
	{
		if( interstitialDidFailToLoadAdEvent != null )
			interstitialDidFailToLoadAdEvent();
	}
	
	
	public void interstitialDidDismiss( string empty )
	{
		if( interstitialDidDismissEvent != null )
			interstitialDidDismissEvent();
	}

	
	public void interstitialDidExpire( string empty )
	{
		if( interstitialDidExpireEvent != null )
			interstitialDidExpireEvent();
	}

}
#endif
