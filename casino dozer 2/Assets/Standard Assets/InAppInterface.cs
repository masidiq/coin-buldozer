using UnityEngine;
using System.Collections;

public class InAppInterface : MonoBehaviour {
	public GameObject g_global;
	
	// Use this for initialization
	void Start () {
		// Global g = g_global.GetComponent(typeof(Global));
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	public void PurchaseSuccessed( string id )
	{
		g_global.SendMessage("PurchaseSuccessed", id );
	}
	
	public void PurchaseFailed ( string error )
	{
		g_global.SendMessage("PurchaseFailed", error );
	}
	
	public void PurchaseCancelled( string error )
	{
		g_global.SendMessage("PurchaseCancelled", error );
	}	
}
