using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;


public class GoogleIABListener : MonoBehaviour
{
#if UNITY_ANDROID
	public GameObject inappInterface;
	public GameObject g_global;
	
	void OnEnable()
	{
		// Listen to all events for illustration purposes
		GoogleIABManager.billingSupportedEvent += billingSupportedEvent;
		GoogleIABManager.billingNotSupportedEvent += billingNotSupportedEvent;
		GoogleIABManager.queryInventorySucceededEvent += queryInventorySucceededEvent;
		GoogleIABManager.queryInventoryFailedEvent += queryInventoryFailedEvent;
		GoogleIABManager.purchaseCompleteAwaitingVerificationEvent += purchaseCompleteAwaitingVerificationEvent;
		GoogleIABManager.purchaseSucceededEvent += purchaseSucceededEvent;
		GoogleIABManager.purchaseFailedEvent += purchaseFailedEvent;
		GoogleIABManager.consumePurchaseSucceededEvent += consumePurchaseSucceededEvent;
		GoogleIABManager.consumePurchaseFailedEvent += consumePurchaseFailedEvent;
	}


	void OnDisable()
	{
		// Remove all event handlers
		GoogleIABManager.billingSupportedEvent -= billingSupportedEvent;
		GoogleIABManager.billingNotSupportedEvent -= billingNotSupportedEvent;
		GoogleIABManager.queryInventorySucceededEvent -= queryInventorySucceededEvent;
		GoogleIABManager.queryInventoryFailedEvent -= queryInventoryFailedEvent;
		GoogleIABManager.purchaseCompleteAwaitingVerificationEvent += purchaseCompleteAwaitingVerificationEvent;
		GoogleIABManager.purchaseSucceededEvent -= purchaseSucceededEvent;
		GoogleIABManager.purchaseFailedEvent -= purchaseFailedEvent;
		GoogleIABManager.consumePurchaseSucceededEvent -= consumePurchaseSucceededEvent;
		GoogleIABManager.consumePurchaseFailedEvent -= consumePurchaseFailedEvent;
	}



	void billingSupportedEvent()
	{
		Debug.Log( "billingSupportedEvent" );
		
		g_global.SendMessage("IAB_billingSupportedEvent", null, SendMessageOptions.DontRequireReceiver);
	}


	void billingNotSupportedEvent( string error )
	{
		Debug.Log( "billingNotSupportedEvent: " + error );
	}


	void queryInventorySucceededEvent( List<GooglePurchase> purchases, List<GoogleSkuInfo> skus )
	{
		Debug.Log( "queryInventorySucceededEvent" );
		Prime31.Utils.logObject( purchases );
		Prime31.Utils.logObject( skus );
		
		CSGlobal.google_purchases = purchases;
		CSGlobal.google_skus = skus;
		
		// added by me 20130317
		g_global.SendMessage("IAB_queryInventorySucceededEvent", null, SendMessageOptions.DontRequireReceiver);
	}


	void queryInventoryFailedEvent( string error )
	{
		Debug.Log( "queryInventoryFailedEvent: " + error );
	}


	void purchaseCompleteAwaitingVerificationEvent( string purchaseData, string signature )
	{
		Debug.Log( "purchaseCompleteAwaitingVerificationEvent. purchaseData: " + purchaseData + ", signature: " + signature );
	}
	
	void purchaseSucceededEvent( GooglePurchase purchase )
	{
		Debug.Log( "purchaseSucceededEvent: " + purchase );
		
		InAppInterface ipi = (InAppInterface)inappInterface.GetComponent(typeof(InAppInterface));
		ipi.PurchaseSuccessed( purchase.productId );
		
		// add to purchase list. 20130318
		if( CSGlobal.google_purchases == null ) {
			CSGlobal.google_purchases = new List<GooglePurchase>();
		}
		CSGlobal.google_purchases.Add(purchase);
	}


	void purchaseFailedEvent( string error )
	{
		Debug.Log( "purchaseFailedEvent: " + error );
		
		InAppInterface ipi = (InAppInterface)inappInterface.GetComponent(typeof(InAppInterface));
		ipi.PurchaseFailed( error );
	}


	void consumePurchaseSucceededEvent( GooglePurchase purchase )
	{
		Debug.Log( "consumePurchaseSucceededEvent: " + purchase );
		
		InAppInterface ipi = (InAppInterface)inappInterface.GetComponent(typeof(InAppInterface));
		ipi.PurchaseSuccessed( purchase.productId );
	}


	void consumePurchaseFailedEvent( string error )
	{
		Debug.Log( "consumePurchaseFailedEvent: " + error );
		
		InAppInterface ipi = (InAppInterface)inappInterface.GetComponent(typeof(InAppInterface));
		ipi.PurchaseFailed( error );
	}


#endif
}


