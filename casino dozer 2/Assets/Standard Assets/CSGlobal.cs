//#define Amazon_Platform

// 20130402
#define VERSION_2_TAPJOY

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CSGlobal : MonoBehaviour
{	
	static public string tapjoy_appid_iphone = 				"983c0238-ba78-4265-bc78-a79511155227";
	static public string tapjoy_secret_iphone = 			"o9lGXl4g88En5x3BtWZ2";
	static public string tapjoy_action_iphone = 			"84213d19-a577-4f7e-97b9-c6b6610632cd";
	static public string tapjoy_currency_iphone = 			"22300bae-352e-452e-9805-93836359f78c";
	static public string tapjoy_appid_ipad = 				"983c0238-ba78-4265-bc78-a79511155227";
	static public string tapjoy_secret_ipad = 				"o9lGXl4g88En5x3BtWZ2";
	static public string tapjoy_action_ipad = 				"0bb854c3-4881-4444-a120-c81aa5b9868c";
	static public string tapjoy_currency_ipad = 			"3d779428-432b-4186-bdc1-5d2924fa009c";
	static public string tapjoy_appid_android = 			"74ea69d8-7e1f-4de6-8163-783a6f0385b9";
	static public string tapjoy_secret_android = 			"NSxZkTVvjUv7mkdvjZqY";
	static public string tapjoy_action_android = 			"53b17144-1763-41ef-bad2-7736df67a66a";
	static public string tapjoy_currency_android = 			"1805320c-8419-404a-9478-427e672003a5";
	
	static public string leaderboard_iphone = 				"com.kingdom.coinspirate.Leaderboard";
	static public string leaderboard_iphone_paid = 			"com.kingdom.coinspiratePRO.Leaderboard";
	static public string leaderboard_ipad = 				"com.kingdom.coinshdpirate.Leaderboard";
	static public string leaderboard_ipad_paid = 			"com.kingdom.coinshdpiratePRO.Leaderboard";
	static public string leaderboard_android = 				"com.kingdom.coinspirate.Leaderboard";
	static public string leaderboard_amazon = 				"KingdomPirate_Leader_Regular";
	static public string leaderboard_amazon_paid = 			"KingdomPiratePRO_Leader_Regular";
	static public string leaderboard_amazonhd = 			"KingdomPirateHD_Leader_Regular";
	static public string leaderboard_amazonhd_paid = 		"KingdomPirateHDPRO_Leader_Regular";
	static public string leaderboard_osx = 					"com.kingdom.coinsMACpirate.Leaderboard";
	
	// added 20130318
#if UNITY_ANDROID
	static public List<GooglePurchase> google_purchases = null;
	static public List<GoogleSkuInfo> google_skus = null;
#endif
	
//#if Amazon_Platform && ANDROID_AMAZONHD
//	We cannot declare Amazon and Amazon HD at the same time.
//#endif
	
	static public bool IsIOS()
	{
		return Application.platform == RuntimePlatform.IPhonePlayer;
	}
	
	static public bool IsAndroid()
	{
		return Application.platform == RuntimePlatform.Android;
	}

	static public bool IsIphone() 
	{
#if UNITY_IPHONE
		if( IsIOS() ) {
			if( Screen.width == 640 || Screen.width == 320 ) {
				return true;
			}
			
			iPhoneGeneration g = iPhone.generation;
			return ( g == iPhoneGeneration.iPhone || g == iPhoneGeneration.iPhone3G || g == iPhoneGeneration.iPhone3GS
				|| g == iPhoneGeneration.iPodTouch1Gen || g == iPhoneGeneration.iPodTouch2Gen || g == iPhoneGeneration.iPodTouch3Gen ||
				g == iPhoneGeneration.iPodTouch4Gen || g == iPhoneGeneration.iPodTouch5Gen || g == iPhoneGeneration.iPhone4 || 
				g == iPhoneGeneration.iPhone4S || g == iPhoneGeneration.iPhone5);
		}
#endif
		return false;
	}
	
	static public bool IsIphone5()
	{
		if( CSGlobal.IsIphone() ){
			if( Screen.height == 568 || Screen.height == 568 * 2 ) {
				return true;
			}
		}
		
		return false;
	}
	
	static public bool IsIpad() 
	{
		// modified 20130509 for ver3
#if UNITY_IPHONE
		return !CSGlobal.IsIphone();
#endif
		return false;
	}
	
	static public bool IsOSX()
	{
	#if UNITY_STANDALONE_OSX
		return true;
	#endif 
		return Application.platform == RuntimePlatform.OSXPlayer;
	}
	
	static public bool IsAmazon()
	{
		#if UNITY_ANDROID && Amazon_Platform
		return Application.platform == RuntimePlatform.Android;
		#else
		return false;
		#endif
	}
	
	static public bool IsAmazonHD()
	{
		#if Amazon_PlatformHD && UNITY_ANDROID
		return Application.platform == RuntimePlatform.Android;
		#endif
		
		return false;		
	}
	
	static public bool IsAmazonOS()
	{
		return IsAmazon() || IsAmazonHD();
	}
	
	static public bool IsPaidVersion()
	{
		if( CSGlobal.IsIOS() || CSGlobal.IsAmazonOS() )
		{
#if PAID_VERSION
			return true;
#else
			return false;
#endif
		}
		else
		{
			return false;
		}
	}
	
	static public bool IsFreeVersion()
	{
		return !IsPaidVersion();
	}
	
	// 20130402
	static public bool IsVersion2Tapjoy()
	{
#if VERSION_2_TAPJOY
		// version_2 tapjoy is enabled in GoogleAndroid and iOS version only(not Amazon, MacOSX)
		if( (CSGlobal.IsAndroid() && !CSGlobal.IsAmazonOS()) || CSGlobal.IsIOS())
			return true;
#endif
		
		return false;
	}
	
	static public object GetStaticValByPlatform(  System.Type t, string name )
	{
		string pfix_org = "";
		string pfix = "";
		if( IsIphone() ) {
			pfix = "iphone";
		}
		else if( IsIpad() ) {
			pfix = "ipad";
		}
		else if( IsAndroid()) {
			if( IsAmazon()) {
				pfix = "amazon";
			}
			else if( IsAmazonHD() ) {
				pfix = "amazonhd";
			}
			else {
				pfix = "android";
			}
		}
		else if( IsOSX() ) {
			pfix = "osx";
		}
		else {
			pfix = "iphone";
		}
		
		object o = null;
		System.Reflection.FieldInfo f = null;
		pfix_org = pfix;
		
		// check paid version variable.
		if( IsPaidVersion() ) {
			pfix = pfix_org + "_paid";
		
			f = t.GetField(name + pfix);
			if( f != null ) {
				o = f.GetValue(null);
			}
		
			// restore postfix.
			pfix = pfix_org;
		}
		
		if( o == null ) {
			f = t.GetField(name + pfix);
			if( f != null ) {
				o = f.GetValue(null);
			}
		}
		
		// if there is no amazon field, then use android value.
		if( o == null && IsAmazonOS()) 
		{
			pfix = "android";
			f = t.GetField(name + pfix);
			if( f != null ) {
				o = f.GetValue(null);
			}
		}
		
		return o;
	}
	
	static public object GetValByPlatform( Object o, string name )
	{
		string pfix_org = "";
		string pfix = "";
		if( IsIphone() ) {
			pfix = "iphone";
		}
		else if( IsIpad() ) {
			pfix = "ipad";
		}
		else if( IsAndroid()) {
			if( IsAmazon()) {
				pfix = "amazon";
			}
			else if( IsAmazonHD() ) {
				pfix = "amazonhd";
			}
			else {
				pfix = "android";
			}
		}
		else if( IsOSX() ) {
			pfix = "osx";
		}
		else {
			pfix = "iphone";
		}
		
		object res = null; 
		System.Reflection.FieldInfo f = null;
		pfix_org = pfix;
		
		// check paid version variable.
		if( IsPaidVersion() ) {
			pfix = pfix_org + "_paid";
			
			f = o.GetType().GetField(name + pfix);
			if( f != null ) {
				res = f.GetValue(o);
			}
		
			// restore postfix.
			pfix = pfix_org;
		}
		
		if( res == null ) {
			f = o.GetType().GetField(name + pfix);
			if( f != null ) {
				res = f.GetValue(o);
			}
		}
		
		// if there is no amazon field, then use android value.
		if( res == null && IsAmazonOS()) 
		{
			pfix = "android";
			f = o.GetType().GetField(name + pfix);
			if( f != null ) {
				res = f.GetValue(o);
			}
		}
		
		return res;
	}	
	
	
	// added 20130318
//#if UNITY_ANDROID
//	public static void consumeGoogleIABProduct( string id ) {
//		if( CSGlobal.google_purchases == null ) {
//			GoogleIAB.purchaseProduct( id );
//		}
//		else {
//			bool purchased = false;
//			// find the item.
//			for( int i = 0 ; i < CSGlobal.google_purchases.Count ; i ++ ) {
//				GooglePurchase p = CSGlobal.google_purchases[i];
//				if( p.productId.CompareTo(id) == 0 && p.purchaseState == GooglePurchase.GooglePurchaseState.Purchased ) {
//					purchased = true;
//					GoogleIAB.consumeProduct( id );
//				}
//			}
//			
//			if( !purchased ) {
//				GoogleIAB.purchaseProduct( id );
//			}
//		}
//	}
//#endif
}
