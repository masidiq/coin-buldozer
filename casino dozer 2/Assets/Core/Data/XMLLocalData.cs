using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class XMLLocalData : MonoBehaviour
{
    public List<LocalNode> LocalNodes = new List<LocalNode>();
}
[Serializable]
public class LocalNode
{
    public string NodeName;
    public List<string> LocalItemId = new List<string>();
}
