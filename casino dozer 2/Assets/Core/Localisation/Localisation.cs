﻿using UnityEngine;
using System.Xml;
using System.Text;
using System.IO;
using System.Collections;

public class Localisation : MonoBehaviour
{
    public static Localisation local;
    public enum State
    {
        Automatic,
        Manual
    }
    public State state;
    public enum LangState
    {
        Eng
    }
    public LangState langState;
    public Object[] LandXML;
    private static XmlDocument xmlDocument;
    private static XmlElement rootElement;
    private static XmlNode rootNode;

    void Awake()
    {
        local = this;
    }

    void Start()
    {
        IdentLang();
    }

    void IdentLang()
    {
        if (state == State.Manual)
        {
            if (xmlDocument == null) xmlDocument = new XmlDocument();
            IdentXML((int)langState);
        }
        else
        {
            IdentXML(IdentSystemLang());
        }
    }
    void IdentXML(int index)
    {
        var xmlText = LandXML[index].ToString();
        xmlDocument.Load(new StringReader(xmlText));
        rootElement = xmlDocument.DocumentElement;
        rootNode = (XmlNode)rootElement;
    }

    int IdentSystemLang()
    {
        int _intLang = 0;
        var lang = Application.systemLanguage.ToString();
        switch (lang)
        {
            case "English":
                _intLang = 0;
                break;
            case "Russian":
                _intLang = 1;
                break;
        }
        return _intLang;
    }
}
