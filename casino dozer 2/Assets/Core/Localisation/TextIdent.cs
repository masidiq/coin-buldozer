﻿using UnityEngine;
using System.Collections;

public class TextIdent : MonoBehaviour
{
	public string Text;
	GUIText guiText1;
	Localisation localisation;

    public string[] ListText;
    public int RootIndex;
    public int TextIndex;
    public string resultRoot = "";
    public string rezultText = "";
	
	void OnEnable()
	{
		guiText1 = GetComponent<GUIText> ();
		if (guiText1)
			guiText1.text = Text;
	}	
}
