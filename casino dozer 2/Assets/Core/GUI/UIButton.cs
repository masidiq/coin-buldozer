using UnityEngine;
using System.Collections;
using System;

//[ExecuteInEditMode]
public class UIButton : UIBase
{
		protected override void CheckElement ()
		{		
				if (UI_Data != null) {
						if (UI_Data.Style.normal.background) {														
								if (GUI.Button (RectElement, UI_Data.Text, UI_Data.Style)) {
										Press ();										
								}
						}
				}
		}

		SendMessageOptions messageOptions = SendMessageOptions.DontRequireReceiver;

		void Press ()
		{
				gameObject.SendMessage ("SetPress", messageOptions);
		}
}


