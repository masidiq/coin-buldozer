using System;
using System.Xml;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class IdentTextLabel : MonoBehaviour
{
		public string[] ListText;
		public int RootIndex;
		public int TextIndex;
		public  string resultRoot = "";
		public string rezultText = "";
		private XmlDocument textXML;
		private XmlElement element;
		private XmlNode root;

		void Load_XML ()
		{
				if (root == null) {
						textXML = new XmlDocument ();
						textXML.Load (Application.streamingAssetsPath + "/" + "LocalBase" + ".xml");
						element = textXML.DocumentElement;
						root = (XmlNode)element;
				}
		}

		string RecurseXMLDocument (XmlNode root, string rootNode, int index)
		{
				string getText = "";
				if (root == null)
						return null;

				if (root.HasChildNodes) {
						for (int i = 0; i < root.ChildNodes.Count; i++) {
								if (root.ChildNodes [i].Name == rootNode) {
										XmlNode child = root.ChildNodes [i];
										if (child.HasChildNodes) {																								
												getText = child.ChildNodes [index].InnerText;			
										}						
										break;
								}
						}
				}
				return getText;
		}

		public void SetText (int index)
		{
			try {
				Load_XML ();
				gameObject.GetComponent<UILabel> ().UI_Data.Text = RecurseXMLDocument (root, resultRoot, index);
			} catch (Exception e) {
			}
		}
}

