using System;
using UnityEngine;
using System.Collections;

public class UILabel : UIBase
{
		protected override void CheckElement ()
		{
				if (UI_Data != null) {
						if (UI_Data != null) {
								if (UI_Data.Image)
										GUI.Label (RectElement, UI_Data.Image);
								else if (UI_Data.Text != "")
										GUI.Label (RectElement, UI_Data.Text, UI_Data.Style);							
						}
				}
		}
}
