using System;
using UnityEngine;
using System.Collections;

//[ExecuteInEditMode]
public abstract class UIBase : MonoBehaviour
{

		#region value

		[HideInInspector]
		public Rect RectElement;
		public UIData UI_Data;
		bool reposition = false;

		public enum RectIdentification
		{
				Automatic,
				Manual
		}

		public RectIdentification rectIdentification = RectIdentification.Automatic;
		float Depth;
		Vector3 pos;
		Rect rect = new Rect (0f, 0f, 0f, 0f);
		Vector2 elementSize;
		Vector3 startPosition;
		int startFontSize;
		bool init = false;
		bool initSize = false;
		Vector3 defaultScale;

		#endregion

		protected abstract void CheckElement ();

		public void Check ()
		{				
				RectElement = CalculateRect ();
				CheckElement ();
		}

		void OnEnable ()
		{
				Init ();
		}

		private void OnDisable ()
		{						
				if (UIManager.GUIElements.Count > 0) {
						int index = UIManager.GUIElements.IndexOfValue (this);
						UIManager.RemoveGuiElement (index);
				}			
		}

		void Init ()
		{			
				if (!init) {
						init = true;
						startPosition = transform.localPosition;	
						if (UI_Data != null)
								startFontSize = UI_Data.Style.fontSize;
						var pos = transform.position;
						Depth = -pos.z;	
						RectElement = CalculateRect ();	
				}
				AddElement ();						
		}

		void Awake ()
		{
				defaultScale = transform.localScale;
		}

		void Start ()
		{
				InitSize ();
				InitPosition ();
		}

		public void InitSize ()
		{
				if (!initSize) {
						initSize = true;
						if (UIManager.instance) {
								var scale = UIManager.instance.GetScale ();
								SetScale (scale);	
								SetFontSize (scale);
						}
				}
		}

		void InitPosition ()
		{
				if (UIManager.instance) {
						float ratio = UIManager.instance.GetRatio ();					
						float width = Screen.width;
						float height = Screen.height;
						float realmRatio = width / height;
						float delta = (realmRatio) / ratio;
						if (delta != 1) {
								SetPosition (delta);
						}
				}
		}

		void AddElement ()
		{
				try {
						UIManager.GUIElements.Add (Depth, this);					
				} catch (System.Exception ex) {
						Depth += 0.01f;
						AddElement ();
				}
		}

		Rect CalculateRect ()
		{
				if (UIManager.UICamera) {					
						pos = UIManager.UICamera.WorldToScreenPoint (transform.position);
				
						if (rectIdentification == RectIdentification.Manual) {
								rect = new Rect (pos.x - Screen.width / 2, Screen.height / 2 - pos.y, UI_Data.ManualSize.x * transform.localScale.x, UI_Data.ManualSize.y * transform.localScale.y);
						} else {
								if (elementSize == Vector2.zero)
										elementSize = GetElementSize ();
								rect = new Rect (pos.x - Screen.width / 2, Screen.height / 2 - pos.y, elementSize.x * transform.localScale.x, elementSize.y * transform.localScale.y);
						}
				}
				return rect;
		}

		public void SetRect (Rect rect)
		{
				RectElement = rect;
		}

		public Rect GetRect ()
		{
				return RectElement;
		}

		Vector2 GetElementSize ()
		{
				Vector2 size = Vector2.zero;
				if (GetType () == typeof(UIDrawTexture)) {
						if (UI_Data != null) {
								if (UI_Data.Image)
										size = new Vector2 (UI_Data.Image.width, UI_Data.Image.height);
						} 
				} else if (GetType () == typeof(UIButton)) {
						if (UI_Data != null) {
								if (UI_Data.Style.normal.background) {
										size = new Vector2 (UI_Data.Style.normal.background.width, UI_Data.Style.normal.background.height);
								}
						}
				}
				return size;

		}

		public void SetScale (float scale)
		{
				transform.localScale = new Vector3 (transform.localScale.x * scale, transform.localScale.y * scale, transform.localScale.z);							
		}

		public void SetPosition (float delta)
		{
				float yTempPos = 0;
				float xTempPos = 0;

				yTempPos = startPosition.y + startPosition.y * (delta - 1);
				xTempPos = startPosition.x + startPosition.x * (delta - 1);			

				transform.localPosition = new Vector3 (xTempPos, yTempPos, transform.position.z);
		}

		void SetFontSize (float scale)
		{
				float tempSize = startFontSize;
				UI_Data.Style.fontSize = (int)(tempSize * scale);
		}
}

[System.Serializable]
public class UIData
{
		public Vector2 ManualSize;
		public string Text;
		public Texture Image;
		public GUIContent Content;
		public GUIStyle Style;
}


