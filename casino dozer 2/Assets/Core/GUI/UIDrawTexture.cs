using UnityEngine;
using System.Collections;

public class UIDrawTexture : UIBase
{
		public enum SizeSate
		{
				None,
				FullScreen,
				Width,
				Height
		}

		public SizeSate sizeSate;

		protected override void CheckElement ()
		{     			
				if (sizeSate == SizeSate.FullScreen) {
						if (RectElement.width != Screen.width) {
								RectElement.width = Screen.width;
								RectElement.height = Screen.height;
						}
				}
				else if (sizeSate == SizeSate.Width)
				{
					if (RectElement.width != Screen.width) {
						RectElement.width = Screen.width;
						RectElement.height = Screen.width * 4/3.0f;
					}
				}
				else if (sizeSate == SizeSate.Height)
				{
					if (RectElement.width != Screen.width) {
						RectElement.height = Screen.height;
						RectElement.width = Screen.height * 3/4.0f;
					}
				}

				if (UI_Data != null)
						GUI.DrawTexture (RectElement, UI_Data.Image);

		}
}
