#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

#if UNITY_EDITOR
//[ExecuteInEditMode]
#endif
public class UIManager : MonoBehaviour
{
		public static SortedList<float, UIBase> GUIElements = new SortedList<float, UIBase> ();
		public static UIManager instance;
		public static Camera UICamera;
		public Vector2 TargetResolution;
		[HideInInspector]
		public float ratio;

		void OnGUI ()
		{
				if (GUIElements.Count > 0) {
						if (GUIElements.Count > 0) {
								foreach (var element in GUIElements) {
										element.Value.Check ();
								}										
						}
				}			
		}

		void OnEnable ()
		{
				instance = this;
				UICamera = GetComponent<Camera> ();
				ratio = TargetResolution.x / TargetResolution.y;

		}

		public float GetScale ()
		{
				return Screen.width / TargetResolution.x;
		}

		public float GetRatio ()
		{
				return ratio;
		}

		public static void AddGuiElement (float depth, UIBase element)
		{
				AddElement (depth, element);
		}

		public static void AddElement (float depth, UIBase element)
		{
				try {
						UIManager.GUIElements.Add (depth, element);
				} catch (System.Exception ex) {
						depth += 0.01f;
						AddElement (depth, element);
				}
		}

		public static void RemoveGuiElement (int index)
		{
				GUIElements.RemoveAt (index);
		}
}