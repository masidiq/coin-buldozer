﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UIPanel : MonoBehaviour
{
		public enum ConsideYReposition
		{
				None,
				Left,
				Right,
				Top,
				Down,
				VCenter
		}

		public ConsideYReposition consideYReposition;

		void Start ()
		{
				switch (consideYReposition) {
				case ConsideYReposition.Left:
						break;
				case ConsideYReposition.Right:
						break;
				case ConsideYReposition.Top:
						break;
				case ConsideYReposition.Down:
						if (UIManager.instance) {
								float ratio = UIManager.instance.GetRatio ();
								float width = Screen.width;
								float height = Screen.height;
								float realmRatio = width / height;
								float delta = realmRatio - ratio;
								transform.localPosition = new Vector3 (transform.localPosition.x, transform.localPosition.y + delta * 280f, transform.localPosition.z);
						}
						break;
				case ConsideYReposition.VCenter:
					if (UIManager.instance) {
						float ratio = UIManager.instance.GetRatio ();
						float width = Screen.width;
						float height = Screen.height;
						float realmRatio = width / height;
						float delta = (realmRatio - ratio)/2;
				transform.localPosition = new Vector3 (transform.localPosition.x, transform.localPosition.y + delta * 100f, transform.localPosition.z);
					}
				break;
			}
		}

		public bool ClickIsContains (Vector3 inputPosition)
		{
				bool isContains = false;
			
				return isContains;
		}

		void CheckUIElements (float scale)
		{				
			
		}

		bool SetYReposition ()
		{
				return(consideYReposition == ConsideYReposition.Top) ? true : false;
		}
}
