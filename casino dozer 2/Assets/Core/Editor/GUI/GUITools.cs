﻿using UnityEditor;
using UnityEngine;
using System.Collections;

public class GUITools : EditorWindow
{
		private static GUITools guiTools;
		private static NewGUI newGUI;

		[MenuItem ("Tools/GUI/Create new UI", priority = 1)]

		private static void NewUI ()
		{
				var camera = new GameObject ();
				camera.name = "UICamera";
				camera.AddComponent<Camera> ();
				camera.AddComponent<UIManager> ();
				var component = camera.GetComponent<Camera> ();
				component.orthographic = true;
				component.clearFlags = CameraClearFlags.Depth;
				component.depth = 0;
				var controller = new GameObject ();
				controller.name = "UIController";
				controller.AddComponent<UIController> ();
				controller.transform.parent = camera.transform;
				var panel = new GameObject ();
				panel.name = "UIPanel";
				panel.AddComponent<UIPanel> ();
				panel.transform.parent = controller.transform;
		}

		[MenuItem ("Tools/GUI/Add Panel", priority = 2)]

		private static void AddPanel ()
		{
				var obj = new GameObject ();
				obj.name = "UIPanel";
				obj.AddComponent<UIPanel> ();
				obj.layer = SetLayer (obj);
				var controller = FindObjectOfType (typeof(UIController)) as UIController;
				if (controller != null) {
						obj.transform.parent = controller.transform;						
				}
		}

		[MenuItem ("Tools/GUI/Create Button", priority = 2)]

		private static void CreateButton ()
		{
				var obj = new GameObject ();
				obj.name = "UIButton";
				obj.AddComponent<UIButton> ();
				obj.layer = SetLayer (obj);
				AttachToPanel (obj);
		}

		[MenuItem ("Tools/GUI/Create Label", priority = 3)]

		private static void CreateLabel ()
		{
				var obj = new GameObject ();
				obj.name = "UILabel";
				obj.AddComponent<UILabel> ();
				obj.layer = SetLayer (obj);
				AttachToPanel (obj);
		}

		[MenuItem ("Tools/GUI/Create Draw Texture", priority = 4)]

		private static void CreateDrawTexture ()
		{
				var obj = new GameObject ();
				obj.name = "UIDrawTexture";
				obj.AddComponent<UIDrawTexture> ();
				obj.layer = SetLayer (obj);
				AttachToPanel (obj);
		}

		private static int SetLayer (GameObject obj)
		{
				int layer = obj.layer;
				var camComponent = FindObjectOfType (typeof(UICamera)) as UICamera;
				if (camComponent != null)
						layer = camComponent.transform.gameObject.layer;
				return layer;
		}

		private static void AttachToPanel (GameObject obj)
		{
				if (Selection.gameObjects.Length == 1) {
						if (Selection.gameObjects [0].GetComponent<UIPanel> ()) {
								obj.transform.parent = Selection.gameObjects [0].transform;								
						}
						obj.transform.localPosition = Vector3.zero;
				}
		}
}
