using System;
using System.Xml;
using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Object = UnityEngine.Object;

[CustomEditor (typeof(IdentTextLabel))]

public class IdentText : Editor
{
		private int rootNodeIndex;
		private List<string> RootNodesName = new List<string> ();
		private string[] RootNodesArray;
		private int textIndex;
		List<string> TextNodeName = new List<string> ();
		private string[] TextNodeArray;
		private string textInGame;
		private GameObject gameLocalData;
		private IdentTextLabel buttonTextSelection;

		void OnEnable ()
		{
				buttonTextSelection = (IdentTextLabel)base.target;
				rootNodeIndex = buttonTextSelection.RootIndex;
				textIndex = buttonTextSelection.TextIndex;
				gameLocalData = (GameObject)AssetDatabase.LoadAssetAtPath ("Assets/Core/Data/" + "XMLLocalData.prefab", typeof(GameObject));
				InitRootNode ();
				Load_XML ();			

				//if (buttonTextSelection.GetComponent<UILabel> ())
				//	buttonTextSelection.GetComponent<UILabel> ().UI_Data.Text = RecurseXMLDocument (root, buttonTextSelection.resultRoot, buttonTextSelection.rezultText);				
		}

		private XMLLocalData component;
		private string rootName;
		private string childName;

		override public void OnInspectorGUI ()
		{
				rootNodeIndex = EditorGUILayout.Popup ("Select root node:", rootNodeIndex, RootNodesArray);
				buttonTextSelection.RootIndex = rootNodeIndex;

				InitChildNode ();
				textIndex = EditorGUILayout.Popup ("Select text:", textIndex, TextNodeArray);
				buttonTextSelection.TextIndex = textIndex;
        
				if (GUI.changed) {
						rootName = component.LocalNodes [rootNodeIndex].NodeName;
						childName = component.LocalNodes [rootNodeIndex].LocalItemId [textIndex];
						if (buttonTextSelection.GetComponent<UILabel> ())
								buttonTextSelection.GetComponent<UILabel> ().UI_Data.Text = RecurseXMLDocument (root, rootName, childName);						
						
						InitRootNode ();
						InitChildNode ();
						buttonTextSelection.RootIndex = rootNodeIndex;
						buttonTextSelection.resultRoot = rootName;
						buttonTextSelection.rezultText = childName;
						buttonTextSelection.TextIndex = textIndex;			
						EditorUtility.SetDirty (buttonTextSelection);
				}
		}

		private int nodesCount;

		void InitRootNode ()
		{
				buttonTextSelection = (IdentTextLabel)base.target;
     
				if (gameLocalData) {
						if (component == null)
								component = gameLocalData.GetComponent<XMLLocalData> ();

						int count = gameLocalData.GetComponent<XMLLocalData> ().LocalNodes.Count;
						for (int i = 0; i < count; i++) {
								RootNodesName.Add (component.LocalNodes [i].NodeName);
						}
						RootNodesArray = RootNodesName.ToArray ();
						if (rootNodeIndex != nodesCount) {
								TextNodeName.Clear ();
								if (textIndex > component.LocalNodes [rootNodeIndex].LocalItemId.Count)
										textIndex = 0;
								nodesCount = rootNodeIndex;
						}
				}
		}

		void InitChildNode ()
		{
				if (TextNodeName.Count == 0) {
						if (RootNodesArray.Length > 0) {
								int count = component.LocalNodes [rootNodeIndex].LocalItemId.Count;
								for (int i = 0; i < count; i++) {
										TextNodeName.Add (component.LocalNodes [rootNodeIndex].LocalItemId [i]);
								}
								TextNodeArray = TextNodeName.ToArray ();
						}
				}
		}

		private XmlDocument textXML;
		private XmlElement element;
		private XmlNode root;

		void Load_XML ()
		{
				textXML = new XmlDocument ();
				textXML.Load (Application.streamingAssetsPath + "/" + "LocalBase" + ".xml");
				element = textXML.DocumentElement;
				root = (XmlNode)element;
		}

		string RecurseXMLDocument (XmlNode root, string rootNode, string childNode)
		{
				string getText = "";
				if (root == null)
						return null;

				if (root.HasChildNodes) {
						for (int i = 0; i < root.ChildNodes.Count; i++) {
								if (root.ChildNodes [i].Name == rootNode) {
										XmlNode child = root.ChildNodes [i];

										if (child.HasChildNodes) {
												for (int j = 0; j < child.ChildNodes.Count; j++) {
														if (child.ChildNodes [j].Attributes [0].Value == childNode) {
																getText = child.ChildNodes [j].InnerText;
																break;
														}
												}
										}
										break;
								}
						}
				}
				return getText;
		}
}
