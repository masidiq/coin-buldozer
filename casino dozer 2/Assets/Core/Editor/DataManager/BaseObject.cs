using UnityEditor;
using UnityEngine;
using System.Collections;

public class BaseCoreObject
{
    enum CoreState { Base, ActivZone, Animator, HiddenGame, ZoomZone,Inventory }

    private CoreState coreState;

    public void ViewButton()
    {
        ViewSelected();
    }
    void ViewSelected()
    {
        switch (coreState)
        {
            case CoreState.Base:
                ViewBase();
                break;
            case CoreState.ActivZone:
                ViewActiwZone();
                break;
            case CoreState.HiddenGame:
                ViewHiddenObject();
                break;
            case CoreState.Animator:
                ViewAnimator();
                break;
            case CoreState.ZoomZone:
                ViewZoomZone();
                break;
            case CoreState.Inventory:
                ViewInventory();
                break;
        }
    }

    void ViewBase()
    {
         
    }

    void ViewActiwZone()
    {
    }

    void ViewHiddenObject()
    {
    }

    void ViewAnimator()
    {
    }
    void ViewZoomZone()
    {
    }
   void ViewInventory()
   {
       
   }
}
public class BaseCoreVoid : MonoBehaviour
{

}
