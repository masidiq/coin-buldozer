﻿using UnityEngine;
using UnityEditor;
using System.IO;
using System.Xml;
using System.Text;
using System.Collections;

public class XMLTools : EditorWindow
{
		enum EditorState
		{			
				Create,
				Edit
		}

		EditorState editorState = EditorState.Create;

		enum LocalEditState
		{
				Root,
				Child
		}

		LocalEditState localEditState = LocalEditState.Root;
		private static XMLTools window;
		private static XmlDocument xmlDocument;
		private static XmlElement rootElement;
		private static XmlNode rootNode;
		private static XmlNode selectedNode;

		[MenuItem("Tools/XML Localisation Tools", priority = 1)]

		static void OpenWindow ()
		{
				window = (XMLTools)EditorWindow.GetWindow (typeof(XMLTools));
		}

		void OnGUI ()
		{
				EditorGUILayout.BeginHorizontal (GUILayout.Height (25));
				EditorGUILayout.BeginVertical ();
				var _color = GUI.color;
				GUI.color = InitCreate (editorState, _color);
				if (GUILayout.Button ("Create Local File")) {
						editorState = EditorState.Create;
						localEditState = LocalEditState.Root;
				}
				GUI.color = _color;
				EditorGUILayout.EndVertical ();
				EditorGUILayout.BeginVertical ();
				GUI.color = InitEdit (editorState, _color);
				if (GUILayout.Button ("Edit/ Add Local data")) {
						editorState = EditorState.Edit;
				}
				GUI.color = _color;
				EditorGUILayout.EndVertical ();
				EditorGUILayout.EndHorizontal ();

				switch (editorState) {
				case EditorState.Create:
						EditorGUILayout.BeginHorizontal ();
						if (!File.Exists (GetPath ())) {
								if (GUILayout.Button ("Create Base local file")) {
										if (!File.Exists (GetPath ())) {												
												CreateLocalFile ();
												InitXML ();												
										}
								}
						}
						EditorGUILayout.EndHorizontal ();
						break;
				case EditorState.Edit:
						EditLocalFile ();
						break;
				}				
		}

		void CreateLocalFile ()
		{
				if (PathExists ()) {
						xmlDocument = new XmlDocument ();
						XmlTextWriter textWritter = new XmlTextWriter (GetPath (), Encoding.UTF8);
						textWritter.WriteStartDocument ();
						textWritter.WriteStartElement ("data");
						textWritter.WriteEndElement ();
						textWritter.Close ();
						AssetDatabase.Refresh ();

				} else {
						Directory.CreateDirectory (Application.streamingAssetsPath);
						CreateLocalFile ();
				}
		}

		bool PathExists ()
		{
				return Directory.Exists (Application.streamingAssetsPath);
		}

		string GetPath ()
		{
				return Application.streamingAssetsPath + "/" + "LocalBase.xml";
		}
		#region Button Color
		Color InitCreate (EditorState state, Color _color)
		{
				if (editorState == EditorState.Create)
						return Color.green;
				else
						return _color;
		}

		Color InitEdit (EditorState state, Color _color)
		{
				if (editorState == EditorState.Edit)
						return Color.green;
				else
						return _color;
		}
		#endregion
		void EditLocalFile ()
		{
				if (File.Exists (GetPath ())) {
						if (xmlDocument == null) {
								InitXML ();
						}
				} else
						xmlDocument = null;
				if (xmlDocument != null) {
						if (localEditState == LocalEditState.Root) {
								if (selectedNode != rootNode)
										selectedNode = rootNode;
								EditRoot ();
						} else {
								EditChild ();
						}
				} 
		}

		void InitXML ()
		{
				xmlDocument = null;
				xmlDocument = new XmlDocument ();
				xmlDocument.Load (GetPath ()); 
				rootElement = xmlDocument.DocumentElement;
				rootNode = (XmlNode)rootElement;
		}

		void SaveXML ()
		{
				xmlDocument.Save (GetPath ());										
				AssetDatabase.Refresh ();										
				InitXML ();
		}

		string newNodeName;

		void EditRoot ()
		{
				AddNode (localEditState);
				if (rootNode != null) {
						if (rootNode.HasChildNodes) {
								for (int i = 0; i < rootNode.ChildNodes.Count; i++) {
										ViewRootNode (i);
								}						
						}
				}
		}

		void EditChild ()
		{
				AddNode (localEditState);
				
				if (selectedNode != null) {
						EditorGUILayout.BeginHorizontal ();
						EditorGUILayout.LabelField ("You are editing " + selectedNode.Name + " node");
						EditorGUILayout.EndHorizontal ();
						if (selectedNode.HasChildNodes) {
								for (int i = 0; i < selectedNode.ChildNodes.Count; i++) {
										ViewChildNode (i);
								}						
						}
				}	
		}

		bool NodeNameIsValid ()
		{
				bool valid = true;
				if (selectedNode.HasChildNodes) {
						foreach (XmlNode _node in selectedNode.ChildNodes) {
								if (localEditState != LocalEditState.Root) {
										if (_node.Attributes [0].Name == newNodeName) {
												valid = false;
												break;
										}
								} else {
										if (_node.Name == newNodeName) {
												valid = false;
												break;
										}
								}
						}
				}
				return valid;
		}

		void AddNode (LocalEditState state)
		{
				string name = (state == LocalEditState.Root) ? "Root " : "Child ";
				var width = (state == LocalEditState.Root) ? 130 : 100;
				
				EditorGUILayout.BeginHorizontal (GUILayout.Width (400), GUILayout.Height (30));
				EditorGUILayout.BeginVertical ();
				EditorGUIUtility.LookLikeControls (100);
				newNodeName = EditorGUILayout.TextField (name + "Node name:", newNodeName, GUILayout.MaxWidth (250));
				EditorGUILayout.EndVertical ();
				EditorGUILayout.BeginVertical ();
				if (GUILayout.Button ("Add " + name + "node", GUILayout.Width (width))) {
						if (newNodeName != "") {
								if (NodeNameIsValid ()) {
										if (state == LocalEditState.Root)
												AddRootNode ();
										else
												AddChildNode ();										
										SaveXML ();
										newNodeName = "";
										GUIUtility.keyboardControl = -1; 
										Repaint ();
								} else
										Debug.LogError ("Name is used!");
						} else
								Debug.LogError ("Node name is not assigned!");
				}
				EditorGUILayout.EndVertical ();
				EditorGUILayout.BeginVertical ();
				if (state != LocalEditState.Root) {
						if (GUILayout.Button ("Back")) {
								localEditState = LocalEditState.Root;
								selectedNode = null;
						}
				}
				EditorGUILayout.EndVertical ();
				EditorGUILayout.EndHorizontal ();				
		}

		void AddRootNode ()
		{
				XmlNode element = xmlDocument.CreateElement (newNodeName);
				xmlDocument.DocumentElement.AppendChild (element);				
		}

		void AddChildNode ()
		{
				XmlNode subElement = xmlDocument.CreateElement ("localeitem");
				selectedNode.AppendChild (subElement);
				XmlAttribute attribute = xmlDocument.CreateAttribute ("id");
				attribute.Value = "Enter ID";
				subElement.Attributes.Append (attribute);
				subElement.InnerText = "Enter Text";				
		}

		void ViewRootNode (int index)
		{
				EditorGUILayout.BeginHorizontal (GUILayout.Width (400));
				var item = rootNode.ChildNodes [index];
				EditorGUILayout.BeginVertical ();
				if (GUILayout.Button (item.Name, GUILayout.MaxWidth (250))) {
						localEditState = LocalEditState.Child;
						selectedNode = item;
				}
				EditorGUILayout.EndVertical ();
				EditorGUILayout.BeginVertical ();
				if (GUILayout.Button ("Delete", GUILayout.MaxWidth (70))) {
						rootNode.RemoveChild (item);
						SaveXML ();
				}
				EditorGUILayout.EndVertical ();
				EditorGUILayout.EndHorizontal ();
		}

		void ViewChildNode (int index)
		{
				EditorGUILayout.BeginHorizontal (GUILayout.Width (450));
				var item = selectedNode.ChildNodes [index];
				EditorGUILayout.BeginVertical ();
				if (GUILayout.Button (item.Name, GUILayout.MaxWidth (70))) {						
				}
				EditorGUILayout.EndVertical ();
				EditorGUILayout.BeginVertical ();
				EditorGUIUtility.LookLikeControls (20);
				item.Attributes [0].Value = EditorGUILayout.TextField ("id:", item.Attributes [0].Value, GUILayout.Width (130));
				EditorGUILayout.EndVertical ();
				EditorGUILayout.BeginVertical ();
				EditorGUIUtility.LookLikeControls (30);
				item.InnerText = EditorGUILayout.TextField ("Text:", item.InnerText, GUILayout.Width (170));
				EditorGUILayout.EndVertical ();
				EditorGUILayout.BeginVertical ();
				if (GUILayout.Button ("Save", GUILayout.Width (70))) {	
						SaveXML ();
						CheckRootData ();
				}
				EditorGUILayout.EndVertical ();
				EditorGUILayout.BeginVertical ();
				if (GUILayout.Button ("Delete", GUILayout.Width (70))) {	
						selectedNode.RemoveChild (item);
				}
				EditorGUILayout.EndVertical ();
				EditorGUILayout.EndHorizontal ();
		}

		string corePath = "/Core/Data/";
		GameObject localData;
		GameObject prefabData;

		string GetRootDataPath ()
		{
				return "Assets" + corePath + "XMLLocalData.prefab";
		}

		void CreateCoreDorectory ()
		{
				if (!Directory.Exists (Application.dataPath + corePath)) {
						Directory.CreateDirectory (Application.dataPath + corePath);
						AssetDatabase.Refresh ();
				}
				if (File.Exists (Application.dataPath + corePath + "XMLLocalData.prefab")) {
						localData = (GameObject)Resources.LoadAssetAtPath ("Assets" + corePath + "XMLLocalData.prefab", typeof(GameObject));
				} else
						CreateLocalData ();
		}

		void CreateLocalData ()
		{
				if (localData == null) {
						prefabData = new GameObject ();
						prefabData.AddComponent<XMLLocalData> ();
						prefabData.name = "XMLLocalData";
				}
				PrefabUtility.CreatePrefab ("Assets" + corePath + prefabData.name + ".prefab", prefabData);
				GameObject.DestroyImmediate (prefabData);
				localData = (GameObject)Resources.LoadAssetAtPath ("Assets" + corePath + "XMLLocalData.prefab", typeof(GameObject));
				AssetDatabase.Refresh ();

		}

		void CheckRootData ()
		{
				CreateCoreDorectory ();				
				if (rootNode.HasChildNodes) {
						localData.GetComponent<XMLLocalData> ().LocalNodes.Clear ();
						for (int i = 0; i < rootNode.ChildNodes.Count; i++) {
								if (rootNode.ChildNodes [i].Name != "#comment" &&
										rootNode.ChildNodes [i].Name != "#text" && rootNode.ChildNodes [i].HasChildNodes) {
										LocalNode localNode = new LocalNode ();
										localNode.NodeName = rootNode.ChildNodes [i].Name;
										XmlNode childNode = rootNode.ChildNodes [i];
										for (int j = 0; j < childNode.ChildNodes.Count; j++) {
												if (childNode.ChildNodes [j].Name != "#comment" &&
														childNode.ChildNodes [j].Name != "#text") {
														if (childNode.ChildNodes [j].Attributes.Count > 0) {
																localNode.LocalItemId.Add (
																		childNode.ChildNodes [j].Attributes [0].Value);
														}
												}
										}
										localData.GetComponent<XMLLocalData> ().LocalNodes.Add (localNode);
								}
						}
						EditorUtility.SetDirty (localData);
						AssetDatabase.Refresh ();
						Debug.Log ("XML Checked");
				}
		}
}

