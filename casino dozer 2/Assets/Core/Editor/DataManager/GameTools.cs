using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using UnityEditor;
using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Object = UnityEngine.Object;
using JsonFx.Json;

public class GameTools : EditorWindow
{
		enum EditorState
		{
				BaseObject,
				Collectons,
				Text,
				Check,
				XML,
				Consol
		}

		static GameTools window;
		private EditorState _editorState = EditorState.BaseObject;

		[MenuItem("Tools/Open games tools", priority = 1)]

		static void OpenWindow ()
		{
				window = (GameTools)EditorWindow.GetWindow (typeof(GameTools));
		}

		private Transform[] selectedRoot = null;
		private bool defaultText = false;
		private bool parseJson = false;
		private Object data;
		#region XML
		#region  root element XML
		private Object xmlData;
		private static XmlDocument localXML;
		private XmlElement element;
		public static XmlNode root;
		#endregion
		public static XmlNode twoRootNode;
		public XmlNode childNode;
		public static XmlNode childListNode;
		private Vector2 scrollPos;
		private Vector2 scrollChildPos;

		public enum ViewState
		{
				Root,
				Child
		}

		public static ViewState viewState = ViewState.Root;
		public static float width = 400f;
		private float heightButton = 25f;
		private string newChildName;
		public static List<ChildData> ListToRemove = new List<ChildData> ();
		public static ChildData[] childData;
		public static int editNodeNumber = 0;
		private string gameDataPath = "/Core/Data/";
		private GameObject gameProgressData;
		private GameObject gameLocalData;

		enum XmlState
		{
				Localisation,
				Config
		}

		private XmlState xmlState;
		#endregion
		private static GameObject[] selectedData;
		//  private static tk2dSpriteCollectionData finalData;
		private int spriteHeight = 768;
		private bool press = false;
		private Object font = null;
		#region Correct Sprite Position
		public float widthSourcePSD;
		public float heightSourcePSD;
		private GameObject[] selectedForCheck;
		#endregion
		enum ContentType
		{
				BackGround,
				ZoomZone
		}

		private ContentType contentType;
		private BaseCoreObject baseCoreObjec;
		string dataText;

		void CheckUniverce (Transform tr)
		{
				foreach (Transform item in tr) {
						if (item.gameObject.GetComponent<SphereCollider> ())
								DestroyImmediate (item.gameObject.GetComponent<SphereCollider> ());
						CheckUniverce (item);
				}
		}

		private void AddIcon (Transform tr, GameObject _obj)
		{
				foreach (Transform item in tr) {
						var oby = (GameObject)Instantiate (_obj);
						oby.transform.name = "StarSystem";
						oby.transform.position = item.position;
						//  oby.transform.parent = tr;
						DestroyImmediate (item.gameObject);
				}
		}

		void OnGUI ()
		{
				if (Event.current.type == EventType.mouseUp) {
						press = false;
				}
				EditorGUILayout.BeginHorizontal ();
				EditorGUILayout.BeginVertical (GUILayout.MaxWidth (100));
				BaseObject (6);
				EditorGUILayout.EndVertical ();

				EditorGUILayout.BeginVertical ();
				#region Check
				if (_editorState == EditorState.Check) {
						childData = null;
						EditorGUILayout.BeginHorizontal ();
						EditorGUILayout.BeginVertical ();
						if (GUILayout.Button ("Check All Textures")) {
								CustomTextureImporter importer = EditorWindow.GetWindow (typeof(CustomTextureImporter), false, "TexturImporter") as CustomTextureImporter;
						}
						EditorGUILayout.EndVertical ();         
						EditorGUILayout.EndHorizontal ();
           
				}

				#endregion

				if (_editorState == EditorState.BaseObject) {
						if (baseCoreObjec == null)
								baseCoreObjec = new BaseCoreObject ();
						baseCoreObjec.ViewButton ();
				}

				#region collection
				if (_editorState == EditorState.Collectons) {
						/*	childData = null;
            EditorGUILayout.BeginHorizontal ();

            data = EditorGUILayout.ObjectField ("", data, typeof(Object), GUILayout.MaxWidth (150));
            EditorGUILayout.LabelField ("Content type:", GUILayout.MaxWidth (90));

            contentType = (ContentType)EditorGUILayout.EnumPopup (contentType, GUILayout.MaxWidth (120));

            if (GUILayout.Button ("Create Json Collection Objects", GUILayout.MaxWidth (190))) {
                    if (data)
                            dataText = data.ToString ();
                    JSObject[] jsObject = JsonReader.Deserialize<JSObject[]> (dataText);
                    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    selectedData = Selection.gameObjects;

                    if (Selection.gameObjects.Length > 0) {
                            for (int i = 0; i < Selection.gameObjects.Length; i++) {
                                    if (selectedData [i].GetComponent<tk2dSpriteCollection> () != null) {
                                            int number = 0;
                                            tk2dSpriteCollectionIndex[] spriteCollections = null;
                                            var component = selectedData [i].GetComponent<tk2dSpriteCollection> ();
                                            tk2dSpriteCollectionData sprColl = null;
                                            if (sprColl == null) {
                                                    spriteCollections = tk2dEditorUtility.GetOrCreateIndex ().GetSpriteCollectionIndex ();

                                                    for (int j = 0; j < spriteCollections.Length; j++) {

                                                            if (selectedData [i].name == spriteCollections [j].name) {
                                                                    if (component.spriteCollection.Count == spriteCollections [j].spriteNames.Length) {
                                                                            GameObject scgo = AssetDatabase.LoadAssetAtPath (AssetDatabase.GUIDToAssetPath (spriteCollections [j].spriteCollectionDataGUID), typeof(GameObject)) as GameObject;
                                                                            var sc = scgo.GetComponent<tk2dSpriteCollectionData> ();
                                                                            sprColl = sc;
                                                                            number = j;
                                                                    }
                                                            }
                                                    }

                                                    if (sprColl == null) {
                                                            EditorUtility.DisplayDialog ("Create Sprite", "Unable to create sprite as no SpriteCollections have been found.", "Ok");
                                                            return;
                                                    }
                                            }


                                            for (int k = 0; k < component.spriteCollection.Count; k++) {
                                                    GameObject go = tk2dEditorUtility.CreateGameObjectInScene ("Sprite");
                                                    tk2dSprite sprite = go.AddComponent<tk2dSprite> ();
                                                    sprite.Collection = sprColl;
                                                    sprite.renderer.material = sprColl.FirstValidDefinition.material;
                                                    sprite.Build ();

                                                    go.name = spriteCollections [number].spriteNames [k];
                                                    if (jsObject != null) {
                                                            for (int f = 0; f < jsObject.Length; f++) {
                                                                    if (jsObject [f].name == "Resolution") {
                                                                            widthSourcePSD = jsObject [f].width;
                                                                            heightSourcePSD = jsObject [f].height;
                                                                    }
                                                                    if (go.name == jsObject [f].name) {
                                                                            float xPos = jsObject [f].x + (jsObject [f].width - jsObject [f].x) / 2 - widthSourcePSD / 2f;
                                                                            float yPos = heightSourcePSD / 2 - jsObject [f].y - (jsObject [f].height - jsObject [f].y) / 2f;
                                                                            float zPos = -1f;
                                                                            if (jsObject [f].layer != 0) {
                                                                                    float startZ = 0f;
                                                                                    switch (contentType) {
                                                                                    case ContentType.BackGround:
                                                                                            startZ = 0f;
                                                                                            break;
                                                                                    case ContentType.ZoomZone:
                                                                                            startZ = -40f;
                                                                                            break;
                                                                                    }
                                                                                    zPos = startZ + jsObject [f].layer;
                                                                            }

                                                                            go.transform.position = new Vector3 (xPos, yPos + 2f, zPos);
                                                                            break;
                                                                    }
                                                            }
                                                    }
                                                    sprite.spriteId = k;
                                                    sprite.MakePixelPerfect ();
                                            }
                                    }
                            }
                    } else if (Selection.gameObjects.Length == 0)
                            Debug.Log ("Object is not selected");

                    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            }
            EditorGUILayout.EndHorizontal ();
            EditorGUILayout.BeginHorizontal (GUILayout.MaxWidth (370));
            EditorGUILayout.BeginVertical (GUILayout.MaxWidth (85));
            EditorGUILayout.LabelField ("Sprite Height:", GUILayout.MaxWidth (85));
            EditorGUILayout.EndVertical ();
            EditorGUILayout.BeginVertical (GUILayout.MaxWidth (40));
            spriteHeight = EditorGUILayout.IntField ("", spriteHeight, GUILayout.MaxWidth (40));
            EditorGUILayout.EndVertical ();
            EditorGUILayout.BeginVertical (GUILayout.MaxWidth (150));
            if (GUILayout.Button ("Create Sprite Collection")) {
                    string prefabName = "SpriteCollection";
                    Object[] selectedObj = Selection.objects;
                    if (selectedObj.Length == 1) {
                            prefabName = selectedObj [0].name;
                    }
                    string path = tk2dEditorUtility.CreateNewPrefab (prefabName);
                    if (path != null) {
                            GameObject go = new GameObject ();
                            go.AddComponent<tk2dSpriteCollection> ();
                            go.GetComponent<tk2dSpriteCollection> ().targetHeight = spriteHeight;
                            go.SetActive (false);
                            Object p = EditorUtility.CreateEmptyPrefab (path);
                            EditorUtility.ReplacePrefab (go, p, ReplacePrefabOptions.ConnectToPrefab);
                            GameObject.DestroyImmediate (go);
                    }
            }
            EditorGUILayout.EndVertical ();
            EditorGUILayout.EndHorizontal ();
            EditorGUILayout.BeginHorizontal (GUILayout.MaxWidth (370));
            if (GUILayout.Button ("Copy prefabs to project")) {
                    Object[] selectedObj = Selection.objects;
                    if (selectedObj.Length > 0) {
                            var path = EditorUtility.SaveFolderPanel ("Save prefabs to directory", "", "");
                            if (path != "") {
                                    var globalPathLenght = Application.dataPath.Length;
                                    path = "Assets" + path.Substring (globalPathLenght, path.Length - globalPathLenght);
                                    for (int i = 0; i < selectedObj.Length; i++) {
                                            PrefabUtility.CreatePrefab (path + "/" + selectedObj [i].name + ".prefab", (GameObject)selectedObj [i]);
                                    }
                            }
                    }
            }
            EditorGUILayout.EndHorizontal ();*/
				}
				#endregion
				#region text
				if (_editorState == EditorState.Text) {

						EditorGUILayout.BeginHorizontal ();
						EditorGUILayout.LabelField ("Default text", GUILayout.MaxWidth (50));
						defaultText = EditorGUILayout.Toggle (defaultText, GUILayout.MaxWidth (20));
						EditorGUILayout.LabelField ("Select Font:", GUILayout.MaxWidth (75));
						font = EditorGUILayout.ObjectField (font, typeof(Object), GUILayout.MaxWidth (120));
						if (GUILayout.Button ("Create Text Label", GUILayout.MaxWidth (150))) {
								selectedRoot = Selection.transforms;
								if (selectedRoot.Length > 0) {
										for (int i = 0; i < selectedRoot.Length; i++) {
												//if (selectedRoot [i].GetComponent<UILabel> () == null) {
												//	if (selectedRoot [i].GetComponent<UIPanel> () == null) {
												//			selectedRoot [i].gameObject.AddComponent<UIPanel> ();
												//	}
												//	GameObject go = new GameObject ("Label");
												//	go.transform.position = selectedRoot [i].position;
												//	go.AddComponent<UILabel> ();
												//		if (!defaultText)
												//				go.AddComponent<ButtonTextSection> ();
												//		go.transform.parent = selectedRoot [i];
												//	} else {
												//			Debug.LogError ("At this Object can not be created Labele, choose another");
												//}
										}
								} else
										Debug.LogError ("Select Root Object");
						}
						EditorGUILayout.EndHorizontal ();
				}
				#endregion
				#region XML
				if (_editorState == EditorState.XML) {
						XMLMode ();
				}
				#endregion

				EditorGUILayout.EndVertical ();
				EditorGUILayout.EndHorizontal ();
		}

		static void ChangeTexturePreferences ()
		{
				var textures = Resources.FindObjectsOfTypeAll (typeof(Texture)) as Texture[];
				if (textures.Length > 0) {

						int count = 0;
						for (int i = 0; i < textures.Length; i++) {
								if (textures [i].name == "atlas0") {
										count++;
								}
								string path = AssetDatabase.GetAssetPath (textures [i]);
								TextureImporter textureImporter = AssetImporter.GetAtPath (path) as TextureImporter;

								if (textureImporter) {
										if (textureImporter.textureType != TextureImporterType.Advanced)
												textureImporter.textureType = TextureImporterType.Advanced;
										if (textureImporter.mipmapEnabled)
												textureImporter.mipmapEnabled = false;
										// count++;
										if (!textureImporter.isReadable)
												textureImporter.isReadable = true;
										AssetDatabase.ImportAsset (path);
								}
						}
				}
		}

		public static string newXmlData = "";

		public static void SaveNewXML ()
		{
				localXML.Save (Application.streamingAssetsPath + "/" + newXmlData + ".xml");
				Debug.Log ("Save " + newXmlData);
		}

		private static string[] ReadNames ()
		{
				List<string> temp = new List<string> ();
				foreach (UnityEditor.EditorBuildSettingsScene S in UnityEditor.EditorBuildSettings.scenes) {
						if (S.enabled) {
								string name = S.path.Substring (S.path.LastIndexOf ('/') + 1);
								name = name.Substring (0, name.Length - 6);
								temp.Add (name);
						}
				}
				return temp.ToArray ();
		}
		#region Base Button void
		void BaseObject (int count)
		{
				for (int i = 0; i < count; i++) {
						EditorGUILayout.BeginHorizontal ();
						string buttonName = "";
						int enumData = 0;
						switch (i) {
						case 0:
								buttonName = "Base Object";
								enumData = i;
								break;
						case 1:
								buttonName = "Collections";
								enumData = i;
								break;
						case 2:
								buttonName = "Text";
								enumData = i;
								break;
						case 3:
								buttonName = "Check";
								enumData = i;
								break;
						case 4:
								buttonName = "XML Tools";
								enumData = i;
								break;
						case 5:
								buttonName = "Consol";
								enumData = i;
								break;
						}
						if (GUILayout.Button (buttonName, GUILayout.MinHeight (heightButton))) {
								_editorState = (EditorState)enumData;
								if (_editorState != EditorState.XML)
										ListToRemove.Clear ();
						}
						EditorGUILayout.EndHorizontal ();
				}
		}
		#endregion
		#region XML Mode Logic
		private GameObject prefabData;

		void CreateCoreDorectory ()
		{
				if (!Directory.Exists (Application.dataPath + gameDataPath)) {
						Directory.CreateDirectory (Application.dataPath + gameDataPath);
						AssetDatabase.Refresh ();
				}
		}

		void CreateProgressData ()
		{

		}

		void CreateLocalData ()
		{
				if (!press) {
						press = true;
						if (gameProgressData == null) {
								prefabData = new GameObject ();
								prefabData.AddComponent<XMLLocalData> ();
								prefabData.name = "XMLLocalData";
						}
						PrefabUtility.CreatePrefab ("Assets" + gameDataPath + prefabData.name + ".prefab", prefabData);
						GameObject.DestroyImmediate (prefabData);
						gameProgressData = (GameObject)Resources.LoadAssetAtPath ("Assets" + gameDataPath + "XMLLocalData.prefab", typeof(GameObject));
						AssetDatabase.Refresh ();
				}
		}

		void ListXML ()
		{
				if (xmlData) {
						localXML = new XmlDocument ();
						localXML.Load (Application.streamingAssetsPath + "/" + xmlData.name + ".xml");
						element = localXML.DocumentElement;
						root = (XmlNode)element;
						newXmlData = xmlData.name;
				}
		}

		void XMLMode ()
		{
				if (_editorState == EditorState.XML) {
						EditorGUILayout.BeginHorizontal (GUILayout.Width (500), GUILayout.Height (30));
						EditorGUILayout.BeginVertical (GUILayout.Width (300));
						xmlData = EditorGUILayout.ObjectField ("Please select XML file:", xmlData, typeof(TextAsset));
						ListXML ();
						EditorGUILayout.EndVertical ();
						EditorGUILayout.BeginVertical ();
						if (xmlData) {
								if (GUILayout.Button ("Check XML Data", GUILayout.Width (150))) {
										if (xmlState == XmlState.Localisation) {
												CreateCoreDorectory ();
												gameLocalData =
(GameObject)
AssetDatabase.LoadAssetAtPath ("Assets" + gameDataPath + "XMLLocalData.prefab",
												                   typeof(GameObject));
												if (gameLocalData == null)
														CreateLocalData ();
												else {
														if (xmlData) {
																if (root == null) {
																		ListXML ();
																}
																if (root.HasChildNodes) {
																		gameLocalData.GetComponent<XMLLocalData> ().LocalNodes.Clear ();
																		for (int i = 0; i < root.ChildNodes.Count; i++) {
																				if (root.ChildNodes [i].Name != "#comment" &&
																						root.ChildNodes [i].Name != "#text" && root.ChildNodes [i].HasChildNodes) {
																						LocalNode localNode = new LocalNode ();
																						localNode.NodeName = root.ChildNodes [i].Name;
																						XmlNode childNode = root.ChildNodes [i];
																						for (int j = 0; j < childNode.ChildNodes.Count; j++) {
																								if (childNode.ChildNodes [j].Name != "#comment" &&
																										childNode.ChildNodes [j].Name != "#text") {
																										if (childNode.ChildNodes [j].Attributes.Count > 0) {
																												localNode.LocalItemId.Add (
																														childNode.ChildNodes [j].Attributes [0].Value);
																										}
																								}
																						}
																						gameLocalData.GetComponent<XMLLocalData> ().LocalNodes.Add (localNode);
																				}
																		}
																		EditorUtility.SetDirty (gameLocalData);
																		AssetDatabase.Refresh ();
																		Debug.Log ("XML Checked");
																}
														} else {
																Debug.LogError ("In XML Mode choose Local XML");
														}

												}


										}
								}
						}
						EditorGUILayout.EndVertical ();
						EditorGUILayout.BeginVertical ();
						if (viewState != ViewState.Root) {
								if (GUILayout.Button ("Back")) {
										viewState = ViewState.Root;
										width = 400f;
								}
						}
						EditorGUILayout.EndVertical ();
						EditorGUILayout.EndHorizontal ();

						EditorGUILayout.BeginHorizontal ();
						if (viewState == ViewState.Root)
								xmlState = (XmlState)EditorGUILayout.EnumPopup ("Select Editor mode:", xmlState, GUILayout.Width (250));
						EditorGUILayout.EndHorizontal ();

						EditorGUILayout.BeginHorizontal ();

						#region Scroll list
						if (xmlData != null) {
								scrollPos = EditorGUILayout.BeginScrollView (scrollPos, GUILayout.Width (width), GUILayout.Height (310));
								if (viewState == ViewState.Root) {
										RootList ();
										openwindos = false;
								} else if (viewState == ViewState.Child) {
										ChildList ();
								}
								EditorGUILayout.EndScrollView ();
						}
						#endregion
						EditorGUILayout.EndHorizontal ();

						if (viewState == ViewState.Root) {
								CreateFileNode ();
						}
						if (viewState == ViewState.Child) {
								CreateChildNode ();

								if (openwindos) {
										EditorGUILayout.BeginHorizontal ();
										sometext = EditorGUILayout.TextField (sometext, GUILayout.Width (500), GUILayout.Height (200));
										EditorGUILayout.EndHorizontal ();
										EditorGUILayout.BeginHorizontal ();
										if (GUILayout.Button ("Add Text", GUILayout.Width (250))) {
												childListNode.ChildNodes [numberNodeText].Attributes [0].InnerText = sometext;
										}
										if (GUILayout.Button ("Close Windows", GUILayout.Width (250))) {
												openwindos = false;
												sometext = "";
										}
										EditorGUILayout.EndHorizontal ();
								}
						}
				}
		}

		private string sometext = "";
		private int numberNodeText = 0;
		private string buttonName = "";

		void RootList ()
		{
				if (root.HasChildNodes) {
						if (childData == null)
								childData = new ChildData[root.ChildNodes.Count];
						if (root.ChildNodes.Count > 0) {
								for (int i = 0; i < root.ChildNodes.Count; i++) {
										if (root.ChildNodes [i].Name != "#comment" && root.ChildNodes [i].Name != "config" && root.ChildNodes [i].Name != "local") {

												{
														if (childData [i] == null) {
																childData [i] = new ChildData ();
														}
														if (xmlState == XmlState.Config) {
																if (root.ChildNodes [i].Attributes.Count > 0) {
																		buttonName = root.ChildNodes [i].Attributes [0].Value;
																} else {
																		buttonName = root.ChildNodes [i].Name;
																}
														} else {
																buttonName = root.ChildNodes [i].Name;
														}
														if (childData [i] != null)
																childData [i].ViewObject (buttonName, i);
												}
										}
								}
						}
				}
		}

		private bool openwindos = false;

		enum StartState
		{
				Activ,
				Passiv,
				Disable
		}

		private StartState startState;

		void ChildList ()
		{
				if (childListNode.HasChildNodes) {
						for (int i = 0; i < childListNode.ChildNodes.Count; i++) {
								if (childListNode.ChildNodes [i].Name != "#comment") {

										EditorGUILayout.BeginHorizontal ();
										EditorGUILayout.BeginVertical ();
										if (GUILayout.Button (childListNode.ChildNodes [i].Name, GUILayout.ExpandWidth (false))) {
										}
										EditorGUILayout.EndVertical ();
										EditorGUILayout.BeginVertical ();
										EditorGUILayout.LabelField ("id:", GUILayout.MaxWidth (20));
										EditorGUILayout.EndVertical ();
										EditorGUILayout.BeginVertical ();
										if (childListNode.ChildNodes [i].Attributes.Count > 0) {
												GUI.SetNextControlName (childListNode.ChildNodes [i].Attributes [0].InnerText);
												childListNode.ChildNodes [i].Attributes [0].InnerText =
EditorGUILayout.TextField (childListNode.ChildNodes [i].Attributes [0].InnerText);
										}
										if (Event.current.isMouse) {
												//   if (!openwindos) openwindos = true;
												//    sometext = GUI.GetNameOfFocusedControl();
												//   numberNodeText = i;
										}
										EditorGUILayout.EndVertical ();
										EditorGUILayout.BeginVertical ();
										// if (xmlState == XmlState.Localisation)
										// {

										EditorGUILayout.LabelField ("Text:", GUILayout.MaxWidth (70));
										EditorGUILayout.EndVertical ();
										EditorGUILayout.BeginVertical ();

										childListNode.ChildNodes [i].InnerText = EditorGUILayout.TextField (
												childListNode.ChildNodes [i].InnerText, GUILayout.MinWidth (160));

										EditorGUILayout.EndVertical ();
										//  }
										//  else
										//   {
										//  if (childListNode.ChildNodes[i].InnerText != "")
										//  {
										//    switch (childListNode.ChildNodes[i].InnerText)
										//   {
										//    case "Activ":
										// startState=StartState.Activ;
										//       break;
										//   case "Passiv":
										//  startState = StartState.Passiv;
										//       break;
										//    case "Disable":
										//  startState = StartState.Disable;
										//         break;
										//  } 
										//    }
										//   startState = (StartState)EditorGUILayout.EnumPopup(startState);
										//  EditorGUILayout.EndVertical();
										//  }

										EditorGUILayout.BeginVertical ();
										if (GUILayout.Button ("Save")) {
												for (int j = 0; j < root.ChildNodes.Count; j++) {
														if (j == editNodeNumber) {
																root.ChildNodes [j].ChildNodes [i].Attributes [0].InnerText = childListNode.ChildNodes [i].Attributes [0].InnerText;
																root.ChildNodes [j].ChildNodes [i].InnerText = childListNode.ChildNodes [i].InnerText;
																break;
														}
												}
												SaveNewXML ();
												AssetDatabase.Refresh ();
												ListXML ();
										}
										EditorGUILayout.EndVertical ();
										EditorGUILayout.BeginVertical ();
										if (GUILayout.Button ("Delete")) {
												for (int j = 0; j < root.ChildNodes.Count; j++) {
														if (j == editNodeNumber) {
																root.ChildNodes [j].RemoveChild (root.ChildNodes [j].ChildNodes [i]);
																childListNode = root.ChildNodes [j];
																break;
														}
												}


												SaveNewXML ();
												AssetDatabase.Refresh ();
												ListXML ();
										}
										EditorGUILayout.EndVertical ();
										EditorGUILayout.EndHorizontal ();
								}
						}
				}
		}

		void CreateFileNode ()
		{
				EditorGUILayout.BeginHorizontal (GUILayout.MaxWidth (450), GUILayout.Height (30));

				EditorGUILayout.BeginVertical ();
				string nameLabe = "";
				EditorGUILayout.LabelField (nameLabe = (xmlData) ? "New Node Name:" : "Create XML File:", GUILayout.MaxWidth (110));
				EditorGUILayout.EndVertical ();

				EditorGUILayout.BeginVertical ();
				newChildName = EditorGUILayout.TextField (newChildName, GUILayout.MaxWidth (200));
				EditorGUILayout.EndVertical ();

				EditorGUILayout.BeginVertical ();
				string nameTwoLabel = "";
				if (GUILayout.Button (nameTwoLabel = (xmlData) ? "Add Node" : "Create File", GUILayout.Height (20))) {
						if (xmlData) {
								bool add = true;
								for (int i = 0; i < root.ChildNodes.Count; i++) {
										if (newChildName == root.ChildNodes [i].Name) {
												if (xmlState == XmlState.Localisation) {
														Debug.LogError ("Node exists");
														add = false;
														break;
												} else {
														add = true;
												}

										} else {
												add = true;
										}
								}
								if (add) {
										if (xmlState == XmlState.Localisation) {
												XmlNode subElement = localXML.CreateElement (newChildName);
												XmlNode itemChld = root;
												itemChld.AppendChild (subElement);
										} else {
												XmlNode subElement = localXML.CreateElement ("scene");
												XmlNode itemChld = root;
												itemChld.AppendChild (subElement);
												XmlAttribute attribute = localXML.CreateAttribute ("id");
												attribute.Value = newChildName;
												subElement.Attributes.Append (attribute);

										}
										localXML.Save (Application.streamingAssetsPath + "/" + xmlData.name + ".xml");
										childData = null;
										AssetDatabase.Refresh ();
										ListXML ();
								}
						} else {
								if (newChildName != "") {
										var xmlDocument = new XmlDocument ();
										try {
												xmlDocument.Load (Application.streamingAssetsPath + "/" + newChildName + ".xml");
												Debug.LogError ("File exists");
										} catch (FileNotFoundException) {
												XmlTextWriter textWritter = new XmlTextWriter (Application.streamingAssetsPath + "/" + newChildName + ".xml", Encoding.UTF8);
												textWritter.WriteStartDocument ();
												textWritter.WriteStartElement ("data");
												textWritter.WriteEndElement ();
												textWritter.Close ();
												AssetDatabase.Refresh ();
										}
								}
						}
				}
				EditorGUILayout.EndVertical ();
				EditorGUILayout.EndHorizontal ();
		}

		void CreateChildNode ()
		{
				EditorGUILayout.BeginHorizontal (GUILayout.MaxWidth (450), GUILayout.Height (30));

				EditorGUILayout.BeginVertical ();
				if (xmlState == XmlState.Config)
						newChildName = EditorGUILayout.TextField (newChildName, GUILayout.MaxWidth (200));
				EditorGUILayout.EndVertical ();

				EditorGUILayout.BeginVertical ();
				string nameTwoLabel = "";
				if (GUILayout.Button ("Create Child Node", GUILayout.Height (20))) {
						bool add = true;
						if (xmlState == XmlState.Localisation) {
								for (int i = 0; i < twoRootNode.ChildNodes.Count; i++) {
										if (newChildName == twoRootNode.ChildNodes [i].Name) {
												Debug.LogError ("Node exists");
												add = false;
												break;
										} else {
												add = true;
										}
								}
						} else {
								add = true;
						}
						if (add) {
								if (xmlState == XmlState.Localisation) {
										XmlNode subElement2 = localXML.CreateElement ("localeitem");

										if (root.HasChildNodes) {
												for (int i = 0; i < root.ChildNodes.Count; i++) {
														if (root.ChildNodes [i].Name == twoRootNode.Name) {
																root.ChildNodes [i].AppendChild (subElement2);
																XmlAttribute attribute = localXML.CreateAttribute ("id");
																attribute.Value = "Enter ID";
																subElement2.Attributes.Append (attribute);
																subElement2.InnerText = "Enter Text";
																childListNode = root.ChildNodes [i];
														}
												}
										}
								} else {
										XmlNode subElement2 = localXML.CreateElement ("item");

										if (root.HasChildNodes) {
												for (int i = 0; i < root.ChildNodes.Count; i++) {
														if (root.ChildNodes [i].Attributes.Count > 0) {
																if (root.ChildNodes [i].Attributes [0].Value == twoRootNode.Attributes [0].Value) {
																		root.ChildNodes [i].AppendChild (subElement2);
																		XmlAttribute attribute = localXML.CreateAttribute ("id");
																		attribute.Value = "";
																		subElement2.Attributes.Append (attribute);
																		Debug.Log ("Node added");
																}
														}
												}
										}
								}

								localXML.Save (Application.streamingAssetsPath + "/" + xmlData.name + ".xml");
								AssetDatabase.Refresh ();
								childListNode = root.ChildNodes [editNodeNumber];
								ListXML ();
						}

				}
				EditorGUILayout.EndVertical ();
				EditorGUILayout.EndHorizontal ();
		}
		#endregion
}

[Serializable]
public class JSObject
{
		public string name;
		public string blendMode;
		public float x;
		public float y;
		public float width;
		public float height;
		public float layer;
}

public class ChildData
{
		public bool delete = false;

		public void ViewObject (string childNodeName, int number)
		{
				EditorGUILayout.BeginHorizontal ();
				EditorGUILayout.BeginVertical (GUILayout.Width (270));
				if (GUILayout.Button (childNodeName, GUILayout.ExpandWidth (false), GUILayout.Height (25))) {
						GameTools.viewState = GameTools.ViewState.Child;
						GameTools.twoRootNode = GameTools.root.ChildNodes [number];
						GameTools.childListNode = GameTools.root.ChildNodes [number];
						GameTools.editNodeNumber = number;
						GameTools.width = 600f;
				}
				EditorGUILayout.EndVertical ();

				EditorGUILayout.BeginVertical (GUILayout.Width (10));
				delete = EditorGUILayout.Toggle (delete);
				EditorGUILayout.EndVertical ();

				EditorGUILayout.BeginVertical (GUILayout.Width (35));
				if (delete) {
						if (GUILayout.Button ("Delete")) {
								for (int i = 0; i < GameTools.root.ChildNodes.Count; i++) {
										if (childNodeName == GameTools.root.ChildNodes [i].Name) {
												GameTools.root.RemoveChild (GameTools.root.ChildNodes [i]);
												GameTools.SaveNewXML ();
												AssetDatabase.Refresh ();
												for (int j = 0; j < GameTools.childData.Length; j++) {
														GameTools.childData [j].delete = false;
												}
										}
								}
						}
				}
				EditorGUILayout.EndVertical ();
				EditorGUILayout.EndHorizontal ();
		}
}




