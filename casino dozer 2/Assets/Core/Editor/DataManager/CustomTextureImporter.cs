using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

public class CustomTextureImporter : EditorWindow
{
    enum ImporterState
    {
        Singl,
        Individual
    }

    ImporterState importerState;
    Texture2D[] textures;
    List<Texture2D> FilterList = new List<Texture2D>();
    Texture2D selectedTexture;
    Vector2 WinSize;
    ImportSetting impostSetting;
    List<ImportSetting> importSettings = new List<ImportSetting>();
    public TextureImporter textureImporter;
    bool check = false;

    void OnGUI()
    {
        if (GUILayout.Button("Find Textures"))
        {
            textures = null;
            textures = Resources.FindObjectsOfTypeAll(typeof(Texture2D)) as Texture2D[];
            for (int i = 1; i < textures.Length; i++)
            {
                if (textures[i].name != "")
                {
                    if (textures[i].name.Length >= 5)
                    {
                        var rootName = textures[i].name.Substring(0, 5);
                        if (rootName == "atlas")
                        {
                            FilterList.Add(textures[i]);
                        }
                    }
                }
            }

            countTextures = FilterList.Count;
        }
        if (textures != null)
        {
            if (FilterList.Count > 0)
            {
                EditorGUILayout.BeginHorizontal();
                if (GUILayout.Button("Check Textures"))
                {
                    check = true;
                    CheckTextures(progressCount);
                }
                EditorGUILayout.EndHorizontal();
                EditorGUILayout.BeginHorizontal(GUILayout.Height(25));
                importerState = (ImporterState)EditorGUILayout.EnumPopup(importerState, GUILayout.Width(150));
                EditorGUILayout.EndHorizontal();
                EditorGUILayout.BeginHorizontal(GUILayout.Height(25));
                EditorGUILayout.BeginVertical(GUILayout.Width(180));
                WinSize = EditorGUILayout.BeginScrollView(WinSize, GUILayout.Width(150), GUILayout.Height(400));
                for (int i = 0; i < FilterList.Count; i++)
                {
                    ViewTextures(i);
                }
                EditorGUILayout.EndScrollView();
                EditorGUILayout.EndVertical();

                EditorGUILayout.BeginVertical(GUILayout.Width(300));
                if (importerState == ImporterState.Singl)
                {
                    if (impostSetting == null)
                    {
                        impostSetting = new ImportSetting();
                        impostSetting.IsRedable = false;
                        impostSetting.GenerateMipMaps = false;
                        importSettings.Add(impostSetting);
                    }
                    ViewImporterSetting(0);
                }
                EditorGUILayout.EndVertical();
                EditorGUILayout.EndHorizontal();
            }

        }
        if (check)
        {
            EditorGUI.ProgressBar(new Rect(100, 45, 300, 20), progressCount / countTextures, "Progress");
        }
    }

    int countTextures;
    int progressCount;

    void CheckTextures(int index)
    {
        string path = AssetDatabase.GetAssetPath(FilterList[index]);
        textureImporter = AssetImporter.GetAtPath(path) as TextureImporter;
        if (importerState == ImporterState.Singl)
        {
            textureImporter.textureType = SetTextureType(0);
            textureImporter.isReadable = importSettings[0].IsRedable;
            textureImporter.mipmapEnabled = importSettings[0].GenerateMipMaps;
            textureImporter.filterMode = SetFilterMode(0);
            //textureImporter.SetPlatformTextureSettings(0);
            textureImporter.textureFormat = SetTextureFormat(0);
        }
        AssetDatabase.ImportAsset(path);
        progressCount++;
        if (progressCount < FilterList.Count)
        {
            CheckTextures(progressCount);
        }
        else
        {
            check = false;
            progressCount = 0;
        }
    }

    void ViewTextures(int index)
    {
        EditorGUILayout.BeginHorizontal();

        var color = GUI.color;
        if (importerState == ImporterState.Individual)
        {
            if (selectedTexture != null)
            {
                if (selectedTexture == FilterList[index])
                {
                    GUI.color = Color.green;
                }
            }
        }
        if (GUILayout.Button(FilterList[index].name, GUILayout.Height(20)))
        {
            if (importerState == ImporterState.Individual)
            {
                EditorGUIUtility.PingObject(FilterList[index]);
                selectedTexture = FilterList[index];
            }
        }
        GUI.color = color;
        EditorGUILayout.EndHorizontal();
    }

    void ViewImporterSetting(int index)
    {
        EditorGUILayout.BeginVertical();
        importSettings[index].texturType = (ImportSetting.TexturType)EditorGUILayout.EnumPopup("Texture Type ", importSettings[index].texturType);
        importSettings[index].IsRedable = EditorGUILayout.Toggle("Texture is Redable ", importSettings[index].IsRedable);
        importSettings[index].GenerateMipMaps = EditorGUILayout.Toggle("Generate Mip Maps ", importSettings[index].GenerateMipMaps);
        importSettings[index].filterMode = (ImportSetting.Filtermode)EditorGUILayout.EnumPopup("Filter Mode ", importSettings[index].filterMode);
        importSettings[index].platform = (ImportSetting.Platform)EditorGUILayout.EnumPopup("Platform ", importSettings[index].platform);
        importSettings[index].textureSize = (ImportSetting.TextureSize)EditorGUILayout.EnumPopup("Texture Size ", importSettings[index].textureSize);
        importSettings[index].textureFormat = (ImportSetting.Textureformat)EditorGUILayout.EnumPopup("Format ", importSettings[index].textureFormat);
        EditorGUILayout.EndVertical();
    }

    TextureImporterType SetTextureType(int index)
    {
        TextureImporterType importer = TextureImporterType.Advanced;
        switch (importSettings[index].texturType)
        {
            case ImportSetting.TexturType.Texture:
                importer = TextureImporterType.Image;
                break;
            case ImportSetting.TexturType.NormalMap:
                importer = TextureImporterType.Bump;
                break;
            case ImportSetting.TexturType.GUI:
                importer = TextureImporterType.GUI;
                break;
            case ImportSetting.TexturType.Cursor:
                importer = TextureImporterType.Cursor;
                break;
            case ImportSetting.TexturType.Reflection:
                importer = TextureImporterType.Reflection;
                break;
            case ImportSetting.TexturType.Cookie:
                importer = TextureImporterType.Cookie;
                break;
            case ImportSetting.TexturType.Lightmap:
                importer = TextureImporterType.Lightmap;
                break;
            case ImportSetting.TexturType.Advansed:
                importer = TextureImporterType.Advanced;
                break;
        }
        return importer;
    }

    FilterMode SetFilterMode(int index)
    {
        FilterMode mode = FilterMode.Point;

        switch (importSettings[index].filterMode)
        {
            case ImportSetting.Filtermode.Point:
                mode = FilterMode.Point;
                break;
            case ImportSetting.Filtermode.Bilenear:
                mode = FilterMode.Bilinear;
                break;
            case ImportSetting.Filtermode.Trileniar:
                mode = FilterMode.Trilinear;
                break;
        }
        return mode;
    }

    TextureImporterFormat SetTextureFormat(int index)
    {
        TextureImporterFormat format = TextureImporterFormat.RGBA32;
        switch (importSettings[index].textureFormat)
        {

            case ImportSetting.Textureformat.Alpha8:
                format = TextureImporterFormat.Alpha8;
                break;
            case ImportSetting.Textureformat.ARGB16:
                format = TextureImporterFormat.ARGB16;
                break;
            case ImportSetting.Textureformat.ARGB32:
                format = TextureImporterFormat.ARGB32;
                break;
            case ImportSetting.Textureformat.ATC_RGB4:
                format = TextureImporterFormat.ATC_RGB4;
                break;
            case ImportSetting.Textureformat.ATC_RGBA8:
                format = TextureImporterFormat.ATC_RGBA8;
                break;
            case ImportSetting.Textureformat.ATF_RGB_DXT1:
                format = TextureImporterFormat.ATF_RGB_DXT1;
                break;
            case ImportSetting.Textureformat.ATF_RGB_JPG:
                format = TextureImporterFormat.ATF_RGB_JPG;
                break;
            case ImportSetting.Textureformat.ATF_RGBA_JPG:
                format = TextureImporterFormat.ATF_RGBA_JPG;
                break;
            case ImportSetting.Textureformat.Automatic16bit:
                format = TextureImporterFormat.Automatic16bit;
                break;
            case ImportSetting.Textureformat.AutomaticCompressed:
                format = TextureImporterFormat.AutomaticCompressed;
                break;
            case ImportSetting.Textureformat.AutomaticTruecolor:
                format = TextureImporterFormat.AutomaticTruecolor;
                break;
            case ImportSetting.Textureformat.DXT1:
                format = TextureImporterFormat.DXT1;
                break;
            case ImportSetting.Textureformat.DXT5:
                format = TextureImporterFormat.DXT5;
                break;
            case ImportSetting.Textureformat.ETC_RGB4:
                format = TextureImporterFormat.ETC_RGB4;
                break;
            case ImportSetting.Textureformat.PVRTC_RGB2:
                format = TextureImporterFormat.PVRTC_RGB2;
                break;
            case ImportSetting.Textureformat.PVRTC_RGB4:
                format = TextureImporterFormat.PVRTC_RGB4;
                break;
            case ImportSetting.Textureformat.PVRTC_RGBA2:
                format = TextureImporterFormat.PVRTC_RGBA2;
                break;
            case ImportSetting.Textureformat.PVRTC_RGBA4:
                format = TextureImporterFormat.PVRTC_RGBA4;
                break;
            case ImportSetting.Textureformat.RGB16:
                format = TextureImporterFormat.RGB16;
                break;
            case ImportSetting.Textureformat.RGB24:
                format = TextureImporterFormat.RGB24;
                break;
            case ImportSetting.Textureformat.RGBA16:
                format = TextureImporterFormat.RGBA16;
                break;
            case ImportSetting.Textureformat.RGBA32:
                format = TextureImporterFormat.RGBA32;
                break;
        }
        return format;
    }
}

[System.Serializable]
public class ImportSetting
{
    public enum TexturType
    {
        Texture,
        NormalMap,
        GUI,
        Cursor,
        Reflection,
        Cookie,
        Lightmap,
        Advansed
    }

    public TexturType texturType;
    public bool IsRedable = true;
    public bool GenerateMipMaps;

    public enum Filtermode
    {
        Point,
        Bilenear,
        Trileniar
    }

    public Filtermode filterMode = Filtermode.Bilenear;

    public enum Textureformat
    {
        AutomaticCompressed,
        Automatic16bit,
        AutomaticTruecolor,
        DXT1,
        DXT5,
        RGB16,
        RGB24,
        Alpha8,
        ARGB16,
        RGBA32,
        ARGB32,
        RGBA16,
        PVRTC_RGB2,
        PVRTC_RGBA2,
        PVRTC_RGB4,
        PVRTC_RGBA4,
        ETC_RGB4,
        ATC_RGB4,
        ATC_RGBA8,
        ATF_RGB_DXT1,
        ATF_RGBA_JPG,
        ATF_RGB_JPG
    }

    public Textureformat textureFormat;

    public enum TextureSize
    {
        Size_32,
        Size_64,
        Size_128,
        Size_256,
        Size_512,
        Size_1024,
        Size_2048,
        Size_4096
    }

    public TextureSize textureSize = TextureSize.Size_1024;

    public enum Platform
    {
        Standalone,
        Iphone,
        Android
    }

    public Platform platform = Platform.Standalone;
}

