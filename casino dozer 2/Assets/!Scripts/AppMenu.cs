﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System;

public class AppMenu : MonoBehaviour
{	
	public static AppMenu instance;
//	public static int UserVirtualcurrencyBalance=0;
	//The current user name
	public static string UserName = "Anonymous";
	void Awake()
	{
		instance = this;
		DontDestroyOnLoad(this);
	}
	
    void Start()
    {
#if UNITY_IPHONE
#if _APPLICASA
		Applicasa.PromotionManager.SetLiKitPromotionDelegateAndCheckPromotions(PromotionsAvailable,true);
		
		// To request supersonic to display demo campaings.
		Applicasa.PromotionManager.ShowDemoCampaigns();
		
		//Update User virtual currency balace
		UpdateVirtualCurrencyBalance ();
		
		//Update User name
		UpdateUserDisplay();
#endif
#endif
		Debug.Log("Unity : AppMenu.Start()");
    }
	
	public void ShowPromotion()
	{
//		#if UNITY_IPHONE
//		Applicasa.PromotionManager.RaiseCustomEvent("SuperSonic");
//		Applicasa.PromotionManager.GetAvailablePromos(PromotionCallback);
//		TapjoyPlugin.ShowOffers();
		//AdSocial.Instance.ShowVideoad ();

//		#endif
//#if UNITY_ANDROID
//		TapjoyPlugin.ShowOffers();
//#endif
	}

	#if UNITY_IPHONE
	#if _APPLICASA
	#region Promotions
	[MonoPInvokeCallback (typeof (Applicasa.Promotion.GetPromotionArrayFinished))]
		public static void PromotionCallback (bool success, Applicasa.Error error, Applicasa.Promotion.PromotionArray promotionArrayPtr)
		{
			if (success) {
				UpdateVirtualCurrencyBalance ();
				Applicasa.Promotion[] promotions = Applicasa.Promotion.GetPromotionArray (promotionArrayPtr);
				Debug.Log ("LiLog_Unity " + System.DateTime.Now.ToShortTimeString() + ": Got " + promotions.Length + " promotions");
				if (promotions.Length > 0) 
					promotions [0].Show (PromoResult);
			} else {
				Debug.Log ("LiLog_Unity " + System.DateTime.Now.ToShortTimeString() + ": Couldn't get promotions");
			}
		}
	
	[MonoPInvokeCallback (typeof (Applicasa.Promotion.PromotionsAvailable))]
		public static void PromotionsAvailable (Applicasa.Promotion.PromotionArray promotionArrayPtr)
		{
				UpdateVirtualCurrencyBalance ();
				Applicasa.Promotion[] promotions = Applicasa.Promotion.GetPromotionArray (promotionArrayPtr);
				Debug.Log ("LiLog_Unity " + System.DateTime.Now.ToShortTimeString() + ": " + promotions.Length + " Available promotions");
				if (promotions.Length > 0) 
					promotions [0].Show (PromoResult);
		}
	
	
		
	    [MonoPInvokeCallback (typeof (Applicasa.ThirdPartyAction.GetThirdPartyActionArrayFinished))]
		public static void GetThirdPartyActionArray(bool success, Applicasa.Error error, Applicasa.ThirdPartyAction[] result)
		{
			int nOldBalance = UserVirtualcurrencyBalance;
			UpdateVirtualCurrencyBalance ();
			nOldBalance = UserVirtualcurrencyBalance-nOldBalance;
			ScoreManager.instance.SetPartyCoinsCount(nOldBalance);
			Debug.Log("===== GetThirdPartyActionArray : Total = " + UserVirtualcurrencyBalance + ", Reward = " + nOldBalance);
			
		    Debug.Log ("LiLog_Unity " + System.DateTime.Now.ToShortTimeString() + ": GetTrialPayActionArray number of actions = "+(result==null?"0":result.Length.ToString()));
		}
	    // We must provide a callback for the promotion result.
		[MonoPInvokeCallback (typeof (Applicasa.Promotion.PromotionResultDelegate))]
		public static void PromoResult (Applicasa.PromotionAction promoAction, Applicasa.PromotionResult result,  Applicasa.PromotionResultInfo info)
		{
			//Update User virtual currency balace
			Applicasa.ThirdPartyAction.GetThirdPartyActions(GetThirdPartyActionArray);
			int nOldBalance = UserVirtualcurrencyBalance;
			UpdateVirtualCurrencyBalance ();
			nOldBalance = UserVirtualcurrencyBalance-nOldBalance;
			ScoreManager.instance.SetPartyCoinsCount(nOldBalance);
			Debug.Log("===== PromoResult : Total = " + UserVirtualcurrencyBalance + ", Reward = " + nOldBalance);
			Debug.Log ("LiLog_Unity " + System.DateTime.Now.ToShortTimeString() + ": Got Promotion Result");
			Debug.Log ("LiLog_Unity " + System.DateTime.Now.ToShortTimeString() + ": info is "+info.stringResult);
		}
		
	#endregion

	#region StatusBar
		//Update User virtual currency balance 
		static IEnumerator UpdateVirtualCurrencyBalance ()
		{
			Debug.Log ("LiLog_Unity " + System.DateTime.Now.ToShortTimeString() + ": Get Current User Main Balance");
			UserVirtualcurrencyBalance = Applicasa.IAP.GetCurrentUserMainBalance ();
			Debug.Log ("LiLog_Unity " + System.DateTime.Now.ToShortTimeString() + ": Balance = " + UserVirtualcurrencyBalance);	
			return null;
		}
	    //Update User name 
		static IEnumerator UpdateUserDisplay ()
		{
#if !UNITY_EDITOR
			Debug.Log ("LiLog_Unity " + System.DateTime.Now.ToShortTimeString() + ": Get Current User name");
			UserName = Applicasa.User.GetCurrentUser().UserFirstName + " " + Applicasa.User.GetCurrentUser().UserLastName;
			if (UserName.Equals( "" ))
				UserName = "unknown";
			Debug.Log ("LiLog_Unity " + System.DateTime.Now.ToShortTimeString() + ": Name = " + UserName);	
#endif
			return null;
		}
	
	#endregion	
#endif
#endif
}
