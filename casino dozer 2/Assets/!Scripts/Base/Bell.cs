﻿using UnityEngine;
using System.Collections;

public class Bell : MonoBehaviour
{
		GameManager gameManager;
		public Transform Indicator;
		public float SpeedDecrement = 5;
		public GameObject Effect;
		Vector3 scale;
		float startScale;
		bool initScale = false;

		void Awake ()
		{
				gameManager = FindObjectOfType (typeof(GameManager))as GameManager;
				if (gameManager == null) {
						Debug.LogError ("GameManager is not found!");
				}
				if (Indicator == null) {
						Debug.LogError ("Indicator is not assigned!");
				}

		}

		public void SetPress ()
		{
				if (Indicator.localScale.y < startScale * 0.66) {
						gameManager.VibroCollisionCount = 20; 
						gameManager.VibrateMainBet ();
						Increment ();
						if (SoundManager.instance) {
								var type = SoundManager.SoundType.Sound_Shake;
								SoundManager.instance.PlayPointSound (type, this.transform.position);
						}
				}
		}

		public void CheckBell ()
		{
				var sc = Indicator.localScale;
				if (sc.y > 0) {
						Decrement ();						
				}
		}

		void Decrement ()
		{
				if (!initScale) {
						initScale = true;
						if (Indicator)
								startScale = Indicator.localScale.y;
				}
				scale = Indicator.localScale;
				scale.y -= Time.deltaTime * SpeedDecrement / 100;
				if (scale.y < 0.66 * startScale) {
						if (!Effect.activeSelf)
								Effect.SetActive (true);
				}
				Indicator.localScale = scale;
		}

		void Increment ()
		{
				scale = Indicator.localScale;
				scale.y += 0.33f * startScale;
				if (scale.y > 0.66f * startScale) {
						if (Effect.activeSelf)
								Effect.SetActive (false);
				}
				Indicator.localScale = scale;
		}
}
