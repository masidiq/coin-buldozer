﻿using UnityEngine;
using System.Collections;

public class PrizeCollectView : MonoBehaviour
{
		public ViewPrizeTexture PrizeTexture;
		public UIDrawTexture TargerTexture;
		GameObject prize;

		void OnEnable ()
		{
				if (TargerTexture == null)
						Debug.LogError ("TargerTexture is not assigned!");
		}

		public void VievPrizeTexture (int number, GameObject _prize)
		{
				switch (number) {
				case 1:
						TargerTexture.UI_Data.Image = PrizeTexture.TexturePrize1;
			Debug.Log("hahahhahahha");
						break;
				case 2:
						TargerTexture.UI_Data.Image = PrizeTexture.TexturePrize2;
			Debug.Log("hahahhahahha");
						break;
				case 3:
						TargerTexture.UI_Data.Image = PrizeTexture.TexturePrize3;
			Debug.Log("hahahhahahha");
						break;
				case 4:
						TargerTexture.UI_Data.Image = PrizeTexture.TexturePrize4;
			Debug.Log("hahahhahahha");
						break;
				case 5:
						TargerTexture.UI_Data.Image = PrizeTexture.TexturePrize5;
			Debug.Log("hahahhahahha");
						break;
				case 6:
						TargerTexture.UI_Data.Image = PrizeTexture.TexturePrize6;
			Debug.Log("hahahhahahha");
						break;
				case 7:
						TargerTexture.UI_Data.Image = PrizeTexture.TexturePrize7;
			Debug.Log("hahahhahahha");
						break;
				case 8:
						TargerTexture.UI_Data.Image = PrizeTexture.TexturePrize8;
			Debug.Log("hahahhahahha");
						break;
				case 9:
						TargerTexture.UI_Data.Image = PrizeTexture.TexturePrize9;
			Debug.Log("hahahhahahha");

						break;
				case 10:
						TargerTexture.UI_Data.Image = PrizeTexture.TexturePrize10;
			Debug.Log("hahahhahahha");
						break;	

				}
				if (_prize) {
						prize = (GameObject)Instantiate (_prize);
						prize.transform.position = new Vector3 (0f, 12f, 13f);
						prize.transform.rotation = Quaternion.identity;
						if (prize.GetComponent<CoinBase> () != null) {
								Destroy (prize.GetComponent<CoinBase> ());
						}
						if (prize.GetComponent<Rigidbody> () != null) {
								Destroy (prize.GetComponent<Rigidbody> ());
						}
						if (prize.GetComponent<MeshCollider> () != null) {
								Destroy (prize.GetComponent<MeshCollider> ());
						}
						foreach (Transform tr in prize.transform) {
								Destroy (tr.gameObject);
						}
				}
				float time = 0;
				time = (SoundManager.instance != null) ? SoundManager.instance.GetPrizeSoundLenght () : 3f;
				Invoke ("CloseView", time);
		}

	void Update() {
		if (GameManager.instance.gameMode == GameManager.GameMode.ViewMenu)
		{
			gameObject.SetActive(false);
		}
		else
		{
			gameObject.SetActive(true);
		}
	}

		void CloseView ()
		{
				if (prize)
						Destroy (prize);
				gameObject.SetActive (false);
		}
}

[System.Serializable]
public class ViewPrizeTexture
{
		public Texture2D TexturePrize1;
		public Texture2D TexturePrize2;
		public Texture2D TexturePrize3;
		public Texture2D TexturePrize4;
		public Texture2D TexturePrize5;
		public Texture2D TexturePrize6;
		public Texture2D TexturePrize7;
		public Texture2D TexturePrize8;
		public Texture2D TexturePrize9;
		public Texture2D TexturePrize10;
}
