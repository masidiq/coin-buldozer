﻿using UnityEngine;
using System.Collections;

public class CloseThroughTime : MonoBehaviour
{
		public float time = 3f;

		void OnEnable ()
		{
				Invoke ("Close", time);
		}

		void Close ()
		{
				gameObject.SetActive (false);
		}
}
