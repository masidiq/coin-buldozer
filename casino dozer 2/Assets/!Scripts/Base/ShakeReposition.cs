﻿using UnityEngine;
using System.Collections;

public class ShakeReposition : MonoBehaviour
{
		public GameObject Target;

		void Start ()
		{
				if (UIManager.instance) {
						Camera cam = UIManager.instance.transform.gameObject.GetComponent<Camera> ();
						if (cam != null) {
								if (Target) {
										UIBase element = Target.GetComponent<UIBase> ();
										if (element != null) {
												Vector2 elementPos = new Vector2 (element.RectElement.x, element.RectElement.y);
												Vector3 sharkPos = cam.ScreenToWorldPoint (new Vector3 (elementPos.x, Screen.height - elementPos.y, cam.nearClipPlane));
												transform.position = new Vector3 (sharkPos.x, sharkPos.y, transform.position.z);
										}
								}
						}					
				}
		}
}
