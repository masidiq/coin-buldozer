﻿using UnityEngine;
using System.Collections;

public class MenuPanelView : MonoBehaviour
{
	public static MenuPanelView instance;
		public GameObject NotifyOn;
		public GameObject NotifyOff;

		public GameObject Gamecenter;

		void OnEnable ()
		{
		instance = this;
#if UNITY_IOS
				Gamecenter.SetActive(true);
				if (GameManager.instance) {
					if (NotifyOn)
						NotifyOn.SetActive(GameManager.instance.Notification);
					if (NotifyOff)
						NotifyOff.SetActive(!GameManager.instance.Notification);
				}

#endif
		}
}
