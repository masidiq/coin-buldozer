﻿using UnityEngine;
using System.Collections;

public class InitPrizePanelData : MonoBehaviour
{
		public static	InitPrizePanelData instance;
		public		ItemPanelCollection itemPanelCollection;
		public 		ItemPrize[] itemPrizes;

		PrizeRootTypeList prizeTypeList;
		PrizeManager prizeManager;
		public Camera PrizeViewCamera;
		public Vector3 cameraStartPosition;
		public Camera ViewCamera;
		public Vector3 startViewCameraPos;
		public UIDrawTexture DrawText;
		float maxCameraZpos = 124f;
		float maxControllerYpos = 400f;

		#region iniSelectedPrize

		public IdentPrizeInPanel SelectedPrize;
		GameObject selectedPrize;
		GameObject viewTextBackground = null;
		public GameObject ViewText;
		public GameObject SellButton;

		#endregion
	void OnEnable ()
	{
		IdentSelectedPrize (0, 0, itemPrizes[0].data[0].transform);
		float targetWidth = UIManager.instance.TargetResolution.x;
		float targetHeight = UIManager.instance.TargetResolution.y;
		Vector2 RealmResolutio = new Vector2 (Screen.width, Screen.height);
		deltaX = (RealmResolutio.x - targetWidth) * 0.01f;
		deltaZ = (RealmResolutio.y - targetHeight) * 0.03f;

		if (Screen.height > 1024)
			deltaZ = 3;
		deltaZ1 = deltaZ;
		if (deltaZ1 < 0)	deltaZ1 = 0;

		if (PrizeViewCamera) {
			if (PrizeViewCamera.transform.localPosition != cameraStartPosition)
				PrizeViewCamera.transform.localPosition = cameraStartPosition;
		}
//		Debug.Log("@@@@@@@@@@@ OnEnable() = " + deltaX + " , " + cameraStartPosition);
		transform.localPosition = Vector3.zero;

		Init ();

//		AdSocial.Instance.showBanner(false);
	}

		
	void Awake ()
	{
		instance = this;
		prizeManager = FindObjectOfType (typeof(PrizeManager))as PrizeManager;
		if (prizeManager == null)
			Debug.LogError ("PrizeManager is not found!");
	}

	float deltaX;
	float deltaZ, deltaZ1;

		void Start ()
		{
				float rScaleX = (Screen.width * UIManager.instance.TargetResolution.y)/(Screen.height * UIManager.instance.TargetResolution.x);
				cameraStartPosition = new Vector3 (cameraStartPosition.x * rScaleX + deltaX, cameraStartPosition.y, cameraStartPosition.z/* - deltaZ*/);
				PrizeViewCamera.transform.localPosition = cameraStartPosition;
				if (GameManager.instance) {
						GameManager.instance.gameMode = GameManager.GameMode.ViewMenu;
				}
//				Debug.Log("@@@@@@@@@@@ Start() = " + cameraStartPosition + " : " + rScaleX);

				float targetHeight = UIManager.instance.TargetResolution.y;
				float rY = (Screen.height - targetHeight) * 0.03f;
				if (Screen.height > 1024)
					rY = 3;

				if (rY < 0) rY = 0;
				for( int i = 0 ; i < prize_row_count ; i ++ )
				{
					for( int j  = 0; j < prize_col_count; j ++ )
					{	
						IdentPrizeInPanel item  = itemPrizes[i].data[j];
				item.transform.localPosition = new Vector3(item.transform.localPosition.x * rScaleX + deltaX,// + (Screen.width-UIManager.instance.TargetResolution.x)/4/27.3f,// * rScaleX,
				                                           item.transform.localPosition.y,
				                                           item.transform.localPosition.z - rY);
					}				
				}
		}

		void OnDisable ()
		{
//			AdSocial.Instance.showBanner(true);
				if (GameManager.instance) {
						GameManager.instance.gameMode = GameManager.GameMode.Playing;
				}
				if (ViewCamera) {
						ViewCamera.transform.localPosition = startViewCameraPos;
				}	
				DisableBonus ();
		}

		void DisableBonus ()
		{
				for (int i = 0; i < 10; i++) {
						for (int k = 0; k < 6; k++) {
								switch (k) {
								case 0:
										itemPanelCollection.itemPanel [i].Bonus1.transform.gameObject.SetActive (false);
										break;
								case 1:
										itemPanelCollection.itemPanel [i].Bonus2.transform.gameObject.SetActive (false);
										break;
								case 2:
										itemPanelCollection.itemPanel [i].Bonus3.transform.gameObject.SetActive (false);
										break;
								case 3:
										itemPanelCollection.itemPanel [i].Bonus4.transform.gameObject.SetActive (false);
										break;
								case 4:
										itemPanelCollection.itemPanel [i].Bonus5.transform.gameObject.SetActive (false);
										break;
								case 5:
										itemPanelCollection.itemPanel [i].Bonus6.transform.gameObject.SetActive (false);
										break;
								}
						}
				}
		}

	public void Init ()
	{
				DisableBonus ();
				prizeTypeList = prizeManager.GetPrizeCollection ();
				int enableCount;
				int count;
				//bool noCount = false;
				for (int i = 0; i < 10; i++) {
						enableCount = 6;
						for (int j = 0; j < 4; j++) {
								switch (j) {
								case 0:
										itemPanelCollection.itemPanel [i].Subtype1.UI_Data.Text = "x" + prizeTypeList.SubTypeList [i].SubType1List.ToString ();	
										count = prizeTypeList.SubTypeList [i].SubType1List;	
										if (count <= enableCount)
												enableCount = count;									
								
										break;
								case 1:
										itemPanelCollection.itemPanel [i].Subtype2.UI_Data.Text = "x" + prizeTypeList.SubTypeList [i].SubType2List.ToString ();
										count = prizeTypeList.SubTypeList [i].SubType2List;	
										if (count <= enableCount)
												enableCount = count;							
									
										break;
								case 2:
										itemPanelCollection.itemPanel [i].Subtype3.UI_Data.Text = "x" + prizeTypeList.SubTypeList [i].SubType3List.ToString ();
										count = prizeTypeList.SubTypeList [i].SubType3List;	
										if (count <= enableCount)
												enableCount = count;										
								
										break;
								case 3:
										itemPanelCollection.itemPanel [i].Subtype4.UI_Data.Text = "x" + prizeTypeList.SubTypeList [i].SubType4List.ToString ();
										count = prizeTypeList.SubTypeList [i].SubType4List;	
										if (count <= enableCount)
												enableCount = count;								
									
										break;
								}						
						}
						if (enableCount > 0) {							
								for (int k = 0; k < enableCount; k++) {
										switch (k) {
										case 0:
												itemPanelCollection.itemPanel [i].Bonus1.transform.gameObject.SetActive (true);
												break;
										case 1:
												itemPanelCollection.itemPanel [i].Bonus2.transform.gameObject.SetActive (true);
												break;
										case 2:
												itemPanelCollection.itemPanel [i].Bonus3.transform.gameObject.SetActive (true);
												break;
										case 3:
												itemPanelCollection.itemPanel [i].Bonus4.transform.gameObject.SetActive (true);
												break;
										case 4:
												itemPanelCollection.itemPanel [i].Bonus5.transform.gameObject.SetActive (true);
												break;
										case 5:
												itemPanelCollection.itemPanel [i].Bonus6.transform.gameObject.SetActive (true);
												break;
										}
								}
						}
				}
		}

	private int sizeFilter = 15;
	private Vector3[] filters;
	private Vector3 filterSum = Vector3.zero;
	private int posFilter = 0;
	private int qSamples = 0;
//	private Vector3 previous_accel = Vector3.zero;
	private int update_counter = 0;
//	private Quaternion from_rot = Quaternion.identity;
	private Quaternion to_rot  = Quaternion.identity;

	private int prize_row_count = 10;
	private int prize_col_count = 4;
	private int tick = 0;

	Vector3 MovAverage(Vector3 sample)
	{
		sample.Set(-sample.x, sample.y, sample.z);
		if (qSamples==0) filters = new Vector3[sizeFilter];
		filterSum += sample - filters[posFilter];
		filters[posFilter++] = sample;
		if (posFilter > qSamples) qSamples = posFilter;
		posFilter = posFilter % sizeFilter;
		return filterSum / qSamples;
	}

	void FixedUpdate()
	{
		update_counter ++;
		if( update_counter >= 1 ){
			update_counter = 0;
			
			Vector3 temp = -MovAverage(Input.acceleration.normalized); 
			temp = Quaternion.Euler(-90, 0, 0) * temp;
			Quaternion rot = Quaternion.FromToRotation( Vector3.up, temp);
			to_rot = new Quaternion(rot.x-0.8f, rot.y, rot.z, 1.0f);
		} 
		
		for( int i = 0 ; i < prize_row_count ; i ++ )
		{
			for( int j  = 0 ; j < prize_col_count; j ++ )
			{
				IdentPrizeInPanel item  = itemPrizes[i].data[j];
				if( item != null )
				{
					itemPrizes[i].data[j].transform.rotation = Quaternion.Slerp(itemPrizes[i].data[j].transform.rotation, to_rot, 200 * Time.deltaTime );
			
				}
			}
		
//			previous_accel = Input.acceleration.normalized;
		}
	}

	static System.String[] prizeMessages = {
		"Sell 1 Bat Kid for 2 Coins",
		"Sell 1 Watch for 3 Coins",
		"Sell 1 Tiger Toy for 4 Coins",
		"Sell 1 CupCake for 5 Coins",
		"Sell 1 Dice 6 Coins",
		"Sell 1 Diamond for 7 Coins",
		"Sell 1 Cash Stack for 8 Coins",
		"Sell 1 Super Car for 9 Coins",
		"Sell 1 Spidy Kid for 6 Coins",
		"Sell 1 Gold Brick for 6 Coins"
	};
		public void IdentSelectedPrize (int type, int subType, Transform trPrize)
		{
				int prizesCount = 0;
				var collection = PrizeManager.instance.GetPrizeCollection ();
		Debug.Log ("@@@@@@ IdentSelectedPrize : " + type + " | " + subType);

				if (viewTextBackground) {
						viewTextBackground.SetActive (false);
				}
				switch (subType) {
				case 0:
						viewTextBackground = itemPanelCollection.itemPanel [type].BackgrountSubtypeText1.transform.gameObject;
						prizesCount = collection.SubTypeList [type].SubType1List;
						break;
				case 1:
						viewTextBackground = itemPanelCollection.itemPanel [type].BackgrountSubtypeText2.transform.gameObject;
						prizesCount = collection.SubTypeList [type].SubType2List;
						break;
				case 2:
						viewTextBackground = itemPanelCollection.itemPanel [type].BackgrountSubtypeText3.transform.gameObject;
						prizesCount = collection.SubTypeList [type].SubType3List;
						break;
				case 3:
						viewTextBackground = itemPanelCollection.itemPanel [type].BackgrountSubtypeText4.transform.gameObject;
						prizesCount = collection.SubTypeList [type].SubType4List;
						break;
				}
				viewTextBackground.SetActive (true);
		viewTextBackground.transform.localRotation = new Quaternion(0,0,0,0);
				if (ViewText) {
					if (ViewText.GetComponent<UILabel>())
						ViewText.GetComponent<UILabel>().UI_Data.Text = prizeMessages[type];
//						if (ViewText.GetComponent<IdentTextLabel> ()) {
//								ViewText.GetComponent<IdentTextLabel> ().SetText (type);
//						}
				}

				if (SellButton) {
					if (prizesCount > 0) {
					Debug.Log("== item ["+type+"]["+subType+"] = 1");
						SellButton.SetActive (true);
						if (SellButton.GetComponent<SellPrize> ()) {
							SellButton.GetComponent<SellPrize> ().Type = type;
							SellButton.GetComponent<SellPrize> ().SubType = subType;
						}
					} else
						SellButton.SetActive (false);
				}

				if (ViewCamera) {
							
					if (trPrize != null) {
						ViewCamera.transform.localPosition = new Vector3 (trPrize.localPosition.x, ViewCamera.transform.localPosition.y, trPrize.localPosition.z);
						Debug.Log("$$$$$$$$$$$ viewcamera.location = " + ViewCamera.transform.localPosition);
					}
					SetPrizeScreen ();
				} 
		}

		void SetPrizeScreen ()
		{
			if (DrawText != null) {	
				ViewCamera.enabled = true;	
				Texture tex = GetTexture ();
				if (tex == null)
				{
					DrawText.UI_Data.Text = "  ";
					return;
				}
				DrawText.UI_Data.Image = tex;// GetTexture ();
//				DrawText.UI_Data.ManualSize = new Vector2 (Screen.width/8, Screen.height/8);
			}
		}

		Texture GetTexture ()
		{				
				Texture texture = null;
				float rScale = 1f;
				if (Screen.height > 1024) 
					rScale = 1024f/Screen.height;
				RenderTexture renderTexture = new RenderTexture ((int)(Screen.width*rScale), (int)(Screen.height*rScale), 24);
				ViewCamera.targetTexture = renderTexture;
				ViewCamera.Render ();
				texture = (Texture)renderTexture;	
				ViewCamera.targetTexture = null;
				ViewCamera.enabled = false;					
				return texture;	
		}

		Vector3 startClick;
		Vector3 tempCameraPos;
		Vector3 tempControllerPos;
		Vector3 tempForSpeedPos;
		float speedMove;
		float tempSpeedMove;
		bool press = false;

		void Update ()
		{
				if (Input.GetMouseButtonDown (0)) {
						startClick = Input.mousePosition;
						tempCameraPos = PrizeViewCamera.transform.localPosition;
						tempControllerPos = transform.localPosition;
				}
				if (Input.GetMouseButtonUp (0)) {
						press = false;
				}
				if (Input.GetMouseButton (0)) {
						if (PrizeViewCamera) {
								if (!press)
										press = true;
								var pos = tempCameraPos;
								var conPos = tempControllerPos;
								Vector3 input = Input.mousePosition;
								var delta = input.y - startClick.y;
								var camPos = PrizeViewCamera.transform.localPosition;
								tempSpeedMove = camPos.z - tempForSpeedPos.z;

								if (delta > 0) {
										if (camPos.z < maxCameraZpos/* deltaZ1*/) {
												pos.z += delta * 0.05f;
												conPos.y += delta * 0.05f * 400 / 112;
										} else {
												pos.z = maxCameraZpos/* - deltaZ*/;
												conPos.y = maxControllerYpos;
										}
								} else {
										if (camPos.z > cameraStartPosition.z) {
												pos.z += delta * 0.05f;
												conPos.y += delta * 0.05f * 400 / 112;
										} else {
												pos.z = cameraStartPosition.z;
												conPos.y = 0;
										}
								}	
				Debug.Log("^^^^^^ (camPos) = " + camPos.z + ", " + cameraStartPosition.z );
				Debug.Log("^^^^^^ pos = " + pos.z + " ,  " + conPos.y );
								transform.localPosition = conPos;
								PrizeViewCamera.transform.localPosition = pos;
								tick++;
								if (tick % 5 == 0) {
										tick = 0;
										tempForSpeedPos = PrizeViewCamera.transform.localPosition;
										speedMove = Mathf.Abs (tempSpeedMove) * 10f;
								}
						}
				}
				if (!press) {
						if (speedMove > 0) {
								speedMove -= Time.deltaTime;

						}
				}
		}
}

[System.Serializable]
public class ItemPanelCollection
{
		public ItemPanel[] itemPanel;
}

[System.Serializable]
public class ItemPanel
{
		public UILabel Subtype1;
		public UILabel Subtype2;
		public UILabel Subtype3;
		public UILabel Subtype4;
		public UIDrawTexture BackgrountSubtypeText1;
		public UIDrawTexture BackgrountSubtypeText2;
		public UIDrawTexture BackgrountSubtypeText3;
		public UIDrawTexture BackgrountSubtypeText4;
		public UIDrawTexture Bonus1;
		public UIDrawTexture Bonus2;
		public UIDrawTexture Bonus3;
		public UIDrawTexture Bonus4;
		public UIDrawTexture Bonus5;
		public UIDrawTexture Bonus6;
}

[System.Serializable]
public class ItemPrize
{
	public IdentPrizeInPanel[] data;
}

