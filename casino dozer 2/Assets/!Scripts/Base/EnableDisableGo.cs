﻿using UnityEngine;
using System.Collections;

public class EnableDisableGo : MonoBehaviour
{
		public GameObject[] EnableObjects;

		void OnEnable ()
		{
				if (EnableObjects.Length > 0) {
						foreach (var item in EnableObjects) {
								item.SetActive (true);
						}
				}
		}

		void OnDisable ()
		{
				if (EnableObjects.Length > 0) {
						foreach (var item in EnableObjects) {
								if (item)
										item.SetActive (false);
						}
				}
		}
}
