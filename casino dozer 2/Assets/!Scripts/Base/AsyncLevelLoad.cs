﻿using UnityEngine;
using System.Collections;

public class AsyncLevelLoad : MonoBehaviour
{	
		void Start ()
		{			
			StartCoroutine (LoadLevel ());
		}

		IEnumerator LoadLevel ()
		{
				yield return new WaitForSeconds (1f);
				AsyncOperation async = Application.LoadLevelAsync ("MainGame");
				yield return async;
		}
	
}
