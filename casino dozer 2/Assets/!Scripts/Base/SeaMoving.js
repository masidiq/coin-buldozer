#pragma strict

var first_pos : Vector3;
var offset : float = 1.0f;

function Start () {
	first_pos = gameObject.transform.position;
}

function Update () {
	
}

function FixedUpdate() {
	if( transform.position.y <= first_pos.y - 2.0f ) {
		offset = 0.015f;
	}
	
	if( transform.position.y >= first_pos.y ) {
		offset = -0.015f;
	}
	
	transform.position.y += offset;
}