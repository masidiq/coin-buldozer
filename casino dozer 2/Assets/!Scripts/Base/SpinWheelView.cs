using UnityEngine;
using System.Collections;

public class SpinWheelView : MonoBehaviour
{
	private GameManager.GameMode mOldMode; 
	void OnEnable ()
	{
		mOldMode = GameManager.instance.gameMode;
		GameManager.instance.gameMode = GameManager.GameMode.ViewMenu;
	}

	void OnDisable ()
	{
		GameManager.instance.gameMode = mOldMode;
	}
}
