﻿using UnityEngine;
using System.Collections;

public class CoinBase : MonoBehaviour
{
		public enum CoinType
		{
				Normal,
				BonusCoin1,
				CoinWall,
				CoinShower,
				CoinGift,
				BonusCoinGiant,
				XP15,
				XP30,
				XP50,
				GiantCoin,
				CoinPrize
		}

		public CoinType cointType;

		public enum PrizeType
		{
				Prize1,
				Prize2,
				Prize3,
				Prize4,
				Prize5,
				Prize6,
				Prize7,
				Prize8,
				Prize9,
				Prize10
		}

		public PrizeType prizeType;

		public enum PrizeSubType
		{
				PrizeSub_1,
				PrizeSub_2,
				PrizeSub_3,
				PrizeSub_4
		}

		public PrizeSubType prizeSubType;
		bool isCollision = false;
		[HideInInspector]
		public bool soundIsPlay = false;
		bool collisionProcessed = false;
		GameManager gameManager;
		SoundManager soundManager;
		CoinManager coinManager;

		void Start ()
		{
				gameManager = FindObjectOfType (typeof(GameManager)) as GameManager;
				if (gameManager == null)
						Debug.LogError ("GameManager is not found!");
				soundManager = FindObjectOfType (typeof(SoundManager)) as SoundManager;
				if (soundManager == null)
						Debug.LogError ("SoundManager is not found!");
				coinManager = FindObjectOfType (typeof(CoinManager)) as CoinManager;
				if (coinManager == null)
						Debug.LogError ("CoinManager is not found!");

				if (this.cointType == CoinType.GiantCoin) {

				} else if (this.cointType == CoinType.CoinPrize) {

				}

		}

		void OnCollisionEnter (Collision collision)
		{
				if (this.cointType == CoinType.Normal) {
						if (soundManager != null && !soundIsPlay) {
								var type = SoundManager.SoundType.Sound_CoinOnTable;
								soundIsPlay = true;
								soundManager.PlayPointSound (type, this.transform.position);
						} 
				}
				if (collision.gameObject.name.Contains ("Receivecoin_Bet")) {
           
						if (coinManager) {
								coinManager.OnCoinCollected (this);
								var type = soundManager.GetSoundTypeOfCoin (this);
								soundManager.PlayPointSound (type, this.transform.position);
								gameManager.RemoveCoin (this);
								Destroy (gameObject);
						}
				} else if (collision.gameObject.name.Contains ("ReceiveFailedcoin_Bet")) {
						gameManager.RemoveCoin (this);
						Destroy (gameObject);
				} 
				if (this.cointType == CoinType.GiantCoin) {
						if (!collisionProcessed) {
								collisionProcessed = true;
								gameManager.VibroCollisionCount = 10;
								gameManager.VibrateMainBet ();
						}
				}
		}

		public void EnableParticle (bool enable)
		{
				Transform t_child = transform.Find ("BonusParticle");

				if (t_child != null) {
						var temp = t_child.gameObject;
						var ps = temp.particleSystem;

						ps.enableEmission = enable;
				}

				t_child = transform.Find ("PrizeParticle");
				if (t_child != null) {
						var temp = t_child.gameObject;
						var ps = temp.particleSystem;

						ps.enableEmission = enable;
				}
		}

		public void EnableGiantProcessed (bool enable)
		{
				collisionProcessed = enable;
		}
}
