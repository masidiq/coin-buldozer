﻿using UnityEngine;
using System.Collections;

public class IdentPrizeInPanel : MonoBehaviour
{
		public enum PrizeType
		{
				Prize1,
				Prize2,
				Prize3,
				Prize4,
				Prize5,
				Prize6,
				Prize7,
				Prize8,
				Prize9,
				Prize10
		}

		public PrizeType prizeType;

		public enum PrizeSubType
		{
				PrizeSub_1,
				PrizeSub_2,
				PrizeSub_3,
				PrizeSub_4
		}

		public PrizeSubType prizeSubType;
		private bool fSet = false;
		void OnEnable()
		{
			if (fSet == false)
			{
//			Vector3 vec = new Vector3(transform.position.x+(Screen.width-UIManager.instance.TargetResolution.x)/4/27.3f,
//			                          transform.position.y,
//			                          transform.position.z);//,//-(Screen.height-UIManager.instance.TargetResolution.y)/2/27.3f);
//				Vector3 vec = new Vector3(transform.position.x+(Screen.width-UIManager.instance.TargetResolution.x)/4/27.3f,
//		        		                  transform.position.y,
//		                		          transform.position.z-(Screen.height-UIManager.instance.TargetResolution.y)/2/27.3f);
//				transform.position = vec;

//			Vector3 vec1 = new Vector3(UIManager.instance.GetScale(), UIManager.instance.GetScale(), UIManager.instance.GetScale());
				//transform.localScale = vec1;

//			transform.localRotation = new Quaternion(0, 0, 0, 0);
				fSet = true;
			}
		}

		void	OnMouseDown ()
		{
				if (InitPrizePanelData.instance) {
						InitPrizePanelData.instance.IdentSelectedPrize ((int)prizeType, (int)prizeSubType, transform);
				}
		}
}
