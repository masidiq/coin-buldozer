﻿using UnityEngine;
using System.Collections;

public class SetGameMode : MonoBehaviour
{
		public enum SetMode
		{
				Welcome,
				Playing,
				ViewMenu
		}

		public SetMode setMode;

		void OnEnable ()
		{
				if (GameManager.instance) {
						GameManager.instance.gameMode = (GameManager.GameMode)setMode;
				}
		}

		void OnDisable ()
		{
				if (GameManager.instance) {
						GameManager.instance.gameMode = GameManager.GameMode.Playing;
				}
		}
}
