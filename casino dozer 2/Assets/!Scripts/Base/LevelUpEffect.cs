﻿using UnityEngine;
using System.Collections;

public class LevelUpEffect : MonoBehaviour
{
		UIDrawTexture sourceTexture;
		Vector3 scale;
		Vector3 pos;
		bool init = false;

		void OnEnable ()
		{								
				sourceTexture = transform.gameObject.GetComponent<UIDrawTexture> ();
				pos = transform.position;
				init = false;
		}


	void Update ()
	{
		if (!init) {
			transform.localScale = Vector3.zero;
			init = true;
		}

		scale = transform.localScale;
		if (scale.x < 1) {
			var newScale = new Vector3 (scale.x + Time.deltaTime * 0.2f, scale.y + Time.deltaTime * 0.2f, scale.z);
//			transform.position = new Vector3 (pos.x - sourceTexture.UI_Data.Image.width * newScale.x / 10, pos.y, pos.z);
			transform.localScale = newScale;
			Debug.Log("!!!!! effect position = " + transform.position);
		}
	}
}
