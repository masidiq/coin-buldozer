﻿using UnityEngine;
using System.Collections;

public class InitPrizeText : MonoBehaviour
{
    private GUIText component;

    void Start()
    {
        component = GetComponent<GUIText>();
    }
	public void SetText()
	{
	    if (component)
            component.text = "You won XXXX prize! Collect them all";
	}
}
