﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PrizeManager : MonoBehaviour
{
		public Transform PrizeGeneratePoint;
		public static PrizeManager instance;

		#region Prize Prefabs

		public GameObject Prize1_1;
		public GameObject Prize1_2;
		public GameObject Prize1_3;
		public GameObject Prize1_4;
		public GameObject Prize2_1;
		public GameObject Prize2_2;
		public GameObject Prize2_3;
		public GameObject Prize2_4;
		public GameObject Prize3_1;
		public GameObject Prize3_2;
		public GameObject Prize3_3;
		public GameObject Prize3_4;
		public GameObject Prize4_1;
		public GameObject Prize4_2;
		public GameObject Prize4_3;
		public GameObject Prize4_4;
		public GameObject Prize5_1;
		public GameObject Prize5_2;
		public GameObject Prize5_3;
		public GameObject Prize5_4;
		public GameObject Prize6_1;
		public GameObject Prize6_2;
		public GameObject Prize6_3;
		public GameObject Prize6_4;
		public GameObject Prize7_1;
		public GameObject Prize7_2;
		public GameObject Prize7_3;
		public GameObject Prize7_4;
		public GameObject Prize8_1;
		public GameObject Prize8_2;
		public GameObject Prize8_3;
		public GameObject Prize8_4;
		public GameObject Prize9_1;
		public GameObject Prize9_2;
		public GameObject Prize9_3;
		public GameObject Prize9_4;
		public GameObject Prize10_1;
		public GameObject Prize10_2;
		public GameObject Prize10_3;
		public GameObject Prize10_4;

		#endregion

		GameManager gameManager;
		CoinManager coinManager;
		float lastTimeGenerate = 0;
		static PrizeRootTypeList prizeList;
		public PrizeCollectView CollectView;

		void Awake ()
		{

				instance = this;
				gameManager = FindObjectOfType (typeof(GameManager))as GameManager;
				if (gameManager == null)
						Debug.LogError ("GameManager is not found!");
				coinManager = FindObjectOfType (typeof(CoinManager))as CoinManager;
				if (coinManager == null)
						Debug.LogError ("CoinManager  is not found!");
				if (PrizeGeneratePoint == null) {
						Debug.LogError ("PrizeGeneratePoint is not assigned!");
				}
				if (CollectView == null) {
						Debug.LogError ("PrizeCollectView is not assigned!");
				}
				if (prizeList == null)
						prizeList = new PrizeRootTypeList ();

				DontDestroyOnLoad (this);
	
		}

		int PrizeCount ()
		{
				int count = 0;
				foreach (var item in gameManager.CoinsList) {
						if (item.cointType == CoinBase.CoinType.CoinPrize)
								count++;
				}
				return count;
		}

		public void GenerateTypePrize (int i_prizetype, int i_subtype, Vector3 pos, Quaternion rot)
		{			
				string name = "Prize" + i_prizetype + "_" + i_subtype; 				
				GameObject	prefab = (GameObject)GetType ().GetField (name).GetValue (this);
				if (prefab != null) {
						GameObject coinObject = (GameObject)Instantiate (prefab, pos, rot);					
						coinObject.transform.parent = transform;
						var newCoin = coinObject.GetComponent<CoinBase> ();
						gameManager.AddCon (newCoin);

						if (SoundManager.instance) {
								var type = SoundManager.instance.GetSoundTypeOfCoin (newCoin);
								SoundManager.instance.PlayPointSound (SoundManager.SoundType.Sound_PrizeGenerated, newCoin.transform.position);
						}
				}			
		}

		public void GenerateRandomPrize ()
		{
				if (PrizeCount () < gameManager.settings.MaxPrizeCount) {

						if (Time.time > lastTimeGenerate + gameManager.settings.GenerateTimeInteral) {
								lastTimeGenerate = Time.time;
								int i_prizetype = Random.Range (1, 10);
								int i_subtype = Random.Range (1, 4);
								float temp = Random.Range (0.0f, 1.0f);
							
								if (temp > gameManager.randomizer.BorderCreatePrize) {
										string name = "Prize" + i_prizetype + "_" + i_subtype;             
										GenerateTypePrize (i_prizetype, i_subtype, PrizeGeneratePoint.position, PrizeGeneratePoint.rotation);
								}
						}
				}				
		}
		//function generateRandomPrize( check_condition : boolean ) : boolean
		//{
		// modified 20130509 for ver3
		//var r : int = Random.Range(1.0f, 10.9f);
		//var sub : int = Random.Range(1.0f, 4.9f);
		// check ungenerated prizes and color.
		//var temp : float = Random.Range(0.0f, 1.0f);
		//if( temp > 0.3 ) {
		//	var pm : GameObject = GameObject.Find("5.2. PrizeManager");
		//	if( pm != null ) {
		//	var s_pm : PrizeManager = pm.GetComponent(PrizeManager);
		//	if( s_pm != null ) {
		//		var count : int = s_pm.getPrizeCount( r - 1, sub - 1 );
		//	if( count > 0 ) {
		//	var temp_color = s_pm.getZeroPrizeSubtype( r - 1, sub - 1 );
		//	if( temp_color >= 0 ) {
		//		sub = temp_color + 1;
		//	}
		//	else {
		//		var temp_type = s_pm.getZeroPrizeType( r - 1, sub - 1 );
		//		if( temp_type >= 0 ) {
		//			r = temp_type + 1;
		//			}
		//		}
		//		}
		//		}
		//	}
		//	}
		//	return this.generatePrize( r - 1, sub - 1 );
		//}
		int GetSubTypeCount (int type, int subType)
		{
				int count = 0;

				return count;
		}

		public void AddPrizeToList (CoinBase coin)
		{
				if (!CollectView.transform.gameObject.activeSelf) {
						CollectView.transform.gameObject.SetActive (true);
						CollectView.VievPrizeTexture ((int)coin.prizeType + 1, coin.transform.gameObject);
			Debug.Log("prize.............2222");
				}
				AddPrize (coin);
		}

		void AddPrize (CoinBase coin)
		{
				var type = (int)coin.prizeType;
				var subType = (int)coin.prizeSubType;
				switch (subType) {
				case 0:
						if (prizeList.SubTypeList [type].SubType1List < 4) {
								prizeList.SubTypeList [type].SubType1List++;
								if (GetListPrizeCount () % 4 == 0)
										RoulettePlay ();
						}
						break;
				case 1:

						if (prizeList.SubTypeList [type].SubType2List < 4) {
								prizeList.SubTypeList [type].SubType2List++;
								if (GetListPrizeCount () % 4 == 0)
										RoulettePlay ();
						}
						break;
				case 2:
						if (prizeList.SubTypeList [type].SubType3List < 4) {
								prizeList.SubTypeList [type].SubType3List++;
								if (GetListPrizeCount () % 4 == 0)
										RoulettePlay ();
						}
						break;
				case 3:
						if (prizeList.SubTypeList [type].SubType4List < 4) {
								prizeList.SubTypeList [type].SubType4List++;
								if (GetListPrizeCount () % 4 == 0)
										RoulettePlay ();
						}
						break;
				}
		}

		void RemovePrize (int type, int subtype)
		{
				switch (subtype) {
				case 0:
						if (prizeList.SubTypeList [type].SubType1List > 0) {
								prizeList.SubTypeList [type].SubType1List--;
								AddCoinsCount (type);
						}
						break;
				case 1:
						if (prizeList.SubTypeList [type].SubType2List > 0) {
								prizeList.SubTypeList [type].SubType2List--;
								AddCoinsCount (type);
						}
						break;
				case 2:
						if (prizeList.SubTypeList [type].SubType3List > 0) {
								prizeList.SubTypeList [type].SubType3List--;
								AddCoinsCount (type);
						}
						break;
				case 3:
						if (prizeList.SubTypeList [type].SubType4List > 0) {
								prizeList.SubTypeList [type].SubType4List--;
								AddCoinsCount (type);
						}
						break;
				}
		}

		void AddCoinsCount (int type)
		{
				if (ScoreManager.instance) {
						switch (type) {
						case 0:
								ScoreManager.instance.SetPartyCoinsCount (2);
								break;
						
						case 1:
								ScoreManager.instance.SetPartyCoinsCount (3);
								break;
				
						case 2:
								ScoreManager.instance.SetPartyCoinsCount (4);
								break;
		
						case 3:
								ScoreManager.instance.SetPartyCoinsCount (5);
								break;

						case 4:
								ScoreManager.instance.SetPartyCoinsCount (6);
								break;

						case 5:
								ScoreManager.instance.SetPartyCoinsCount (7);
								break;

						case 6:
								ScoreManager.instance.SetPartyCoinsCount (8);
								break;

						case 7:
								ScoreManager.instance.SetPartyCoinsCount (9);
								break;

						case 8:
								ScoreManager.instance.SetPartyCoinsCount (6);
								break;

						case 9:
								ScoreManager.instance.SetPartyCoinsCount (6);
								break;
						}
				}
		}

		public UIPanel SpinWeel;
		
	public void showBonus()
	{
		RoulettePlay();
	}

		void RoulettePlay ()
		{
				Invoke ("ViewWeel", 1.5f);
		}

		void ViewWeel ()
		{
				if (SpinWeel)
						SpinWeel.transform.gameObject.SetActive (true);
				if (SpinWeelManager.instance)
						SpinWeelManager.instance.InitWeelMode ();
		}

		int GetListPrizeCount ()
		{
				int count = 0;
				if (prizeList != null) {
						if (prizeList.SubTypeList.Count > 0) {
								foreach (var item in prizeList.SubTypeList) {
										count += item.SubType1List;
										count += item.SubType2List;
										count += item.SubType3List;
										count += item.SubType4List;
								}
						}
				}
				return count;
		}

		public void SetPrizeCollection (PrizeRootTypeList sourceList)
		{			
				prizeList = sourceList;
		}

		public PrizeRootTypeList GetPrizeCollection ()
		{						
				return prizeList;								
		}

		public void SellPrize (int type, int subtype)
		{
				RemovePrize (type, subtype);

				if (InitPrizePanelData.instance) {
						InitPrizePanelData.instance.Init ();
						InitPrizePanelData.instance.IdentSelectedPrize (type, subtype, null);
				}
		}

		void RefrashPrizeParameter ()
		{
				int i = 0; 
				int prize_count = 0;
				int first = 0;				
				for (i = 0; i < 10; i++) { 
						//		prize_count = g_scr_prize_manager.getSuccessPrizeCount (i);

						switch (i) {
						case 0: 
						//		param_regeneration_max_counter = 30 - prize_count * 2;
								break;
						case 1:
						//		param_newcoin_to_silver_rate = 0.1 * prize_count;
								break;
						case 2:
							//	param_giant_to_prize_rate = 0.1 * prize_count;
								break;
						case 3:
							//	param_special_coins_prize_to_xp_rate = 0.1 * prize_count;
								break;
						case 4:
							//	param_coin_shower_count = 6 + prize_count / 2;
							//	if (param_coin_shower_count > 10) {
							//			param_coin_shower_count = 10;
							//	}
								break;
						case 5:
						//		param_giant_to_more_coins_count = prize_count;
								break;
						case 6:
							//	param_coinwall_stay_time = 13 + prize_count * 1;
								break;
						case 7:
							//	param_special_to_extra_coins_rate = prize_count * 0.1;
								break;
						case 8:
								//param_special_prize_appear_rate = prize_count * 0.1;
								break;
						case 9:
								first = 40;
							//	if (Global.IsPaidVersion ()) {
								first = 100;
							//	}
							//	param_coin_regeneration_limit = first + prize_count;
								break;
						}
				}
		}
}

public class PrizeRootTypeList
{
		public  List<PrizeSubTypelist> SubTypeList = new List<PrizeSubTypelist> ();

		public	PrizeRootTypeList ()
		{				
				if (SubTypeList.Count < 10) {
						for (int i = 0; i < 10; i++) {
								PrizeSubTypelist subType = new PrizeSubTypelist ();
								SubTypeList.Add (subType);
						}
				}
		}
}

public class PrizeSubTypelist
{
		public int SubType1List;
		public int SubType2List;
		public int SubType3List;
		public int SubType4List;
}

