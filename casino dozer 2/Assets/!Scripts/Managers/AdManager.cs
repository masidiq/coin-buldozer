﻿//#define Amazon_Platform

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ChartboostSDK;

public class AdManager : MonoBehaviour
{
		private static AdManager instance;
		AdSocial adSocialController;

		public enum IsPaid
		{
				No,
				Yes
		}

		public IsPaid isPaid;

		public enum AppIs
		{
				iPhone,
				iPad,
				Android,
				Amazon,
				AmazonHD
		}

		[HideInInspector]
//	XMLLocalData
	public AppIs appIs = AppIs.Amazon;
//		SaveManager saveManager;
//		public StringData _StringData;
//	bool callStart = false;

		void Awake ()
		{
		//	if (adSocialController == null)
				//		adSocialController = new AdSocial ();
				instance = this;
			//	adSocialController.InitAllAd ();
				DontDestroyOnLoad (this);
				//if (appIs != AppIs.AmazonHD)
				//		InitApp ();
//				saveManager = new SaveManager ();
		}

	void Start ()
	{
		try {
	//		Debug.Log("@@@@@@ AdManager : InitChartBoost()");
	//		InitChartBoost ();
		} catch (System.Exception e) {
	//		Debug.Log("@@@@@@ AdManagerError : InitChartBoost");
		}

		try {
	//		Debug.Log("@@@@@@ AdManager : InitializeInAppPurchase()");
	//		InitializeInAppPurchase ();
		} catch (System.Exception e) {
		}
		try {
	//		adSocialController.InitGameCenter ();	
//			adSocialController.InitFacebook ();
		} catch (System.Exception e) {
		}
				//ViewStartAd ();
//		Debug.Log("@@@@@@ AdManager : Start()");

//		#if UNITY_ANDROID
//		Upsight.init( Playhaven_API.playhaven_appid_android, Playhaven_API.playhaven_signature_android );
//		#else
//		Upsight.init( Playhaven_API.playhaven_appid_iphone, Playhaven_API.playhaven_signature_iphone );
//		#endif
//		
//		UpsightManager.badgeCountRequestSucceededEvent += badgeCount =>
//		{
//		};
//		
//		// we always want to register for push notifications at launch so that if there is a pending push notification to
//		// display it gets shown
//		Upsight.registerForPushNotifications();
//		// perform an open request at every app luanch
//		Upsight.requestAppOpen();
	}
	
	
//	void OnApplicationPause( bool pauseStatus )
//	{
//		// session tracking requires that you call requestAppOpen every time your app is launched
//		if( !pauseStatus )
//			Upsight.requestAppOpen();
//	}


		#region purchased

		bool isPurchased = false;

		public bool GetPurchased ()
		{
				return isPurchased;
		}

		void InitializeInAppPurchase ()
		{		
				List<string> productIdentifiers = new List<string> ();
				productIdentifiers.Add (PurchCoins (PurchCoin.One, false));
				productIdentifiers.Add (PurchCoins (PurchCoin.Two, false));
				productIdentifiers.Add (PurchCoins (PurchCoin.Three, false));
				productIdentifiers.Add (PurchCoins (PurchCoin.Four, false));
				if (adSocialController != null) {
			Debug.Log("@@@@@@ initIAP");
						adSocialController.InitPurch (productIdentifiers.ToArray ());
				}
		}

		public void SetPurchased (bool _isPurchased)
		{
				isPurchased = _isPurchased;
		}
//		#if UNITY_ANDROID
//#if Amazon_Platform
//#else
//		static public List<GooglePurchase> google_purchases = null;
//		static public List<GoogleSkuInfo> google_skus = null;
//#endif
//		#endif
		[HideInInspector]
		public bool googleIAB_enabled = false;

		public string PurchCoins (PurchCoin purchCoin, bool purch)
		{
				string id = "";
				switch (purchCoin) {
				case PurchCoin.One:
						switch (appIs) {
						case AppIs.iPhone:
								if (isPaid == IsPaid.No) {
										id = Inapp_API.inapp_100coins_iphone;
								} else {
										id = Inapp_API.inapp_100coins_iphone_paid;
								}
								break;
						case AppIs.iPad:
								if (isPaid == IsPaid.No) {
										id = Inapp_API.inapp_100coins_ipad;
								} else {
										id = Inapp_API.inapp_100coins_ipad_paid;
								}
								break;
						case AppIs.Android:
								id = Inapp_API.inapp_100coins_google;
								break;
						case AppIs.Amazon:
								if (isPaid == IsPaid.No) {
										id = Inapp_API.inapp_100coins_amazon;
								} else {
										id = Inapp_API.inapp_100coins_amazon_paid;
								}
								break;
//						case AppIs.AmazonHD:
//								if (isPaid == IsPaid.No) {
//										id = Inapp_API.inapp_100coins_amazonhd;
//								} else {
//										id = Inapp_API.inapp_100coins_amazonhd_paid;
//								}
//								break;
						}
						break;
				case PurchCoin.Two:
						switch (appIs) {
						case AppIs.iPhone:
								if (isPaid == IsPaid.No) {
										id = Inapp_API.inapp_350coins_iphone;
								} else {
										id = Inapp_API.inapp_350coins_iphone_paid;
								}
								break;
						case AppIs.iPad:
								if (isPaid == IsPaid.No) {
										id = Inapp_API.inapp_350coins_ipad;
								} else {
										id = Inapp_API.inapp_350coins_ipad_paid;
								}
								break;
						case AppIs.Android:
								id = Inapp_API.inapp_350coins_google;
								break;
						case AppIs.Amazon:
								if (isPaid == IsPaid.No) {
										id = Inapp_API.inapp_350coins_amazon;
								} else {
										id = Inapp_API.inapp_350coins_amazon_paid;
								}
								break;
//						case AppIs.AmazonHD:
//								if (isPaid == IsPaid.No) {
//										id = Inapp_API.inapp_350coins_amazonhd;
//								} else {
//										id = Inapp_API.inapp_350coins_amazonhd_paid;
//								}
//								break;
						}
						break;
				case PurchCoin.Three:
						switch (appIs) {
						case AppIs.iPhone:
								if (isPaid == IsPaid.No) {
										id = Inapp_API.inapp_750coins_iphone;
								} else {
										id = Inapp_API.inapp_750coins_iphone_paid;
								}
								break;
						case AppIs.iPad:
								if (isPaid == IsPaid.No) {
										id = Inapp_API.inapp_750coins_ipad;
								} else {
										id = Inapp_API.inapp_750coins_ipad_paid;
								}
								break;
						case AppIs.Android:
								id = Inapp_API.inapp_750coins_google;
								break;
						case AppIs.Amazon:
								if (isPaid == IsPaid.No) {
										id = Inapp_API.inapp_750coins_amazon;
								} else {
										id = Inapp_API.inapp_750coins_amazon_paid;
								}
								break;
//						case AppIs.AmazonHD:
//								if (isPaid == IsPaid.No) {
//										id = Inapp_API.inapp_750coins_amazonhd;
//								} else {
//										id = Inapp_API.inapp_750coins_amazonhd_paid;
//								}
//								break;
						}
						break;
				case PurchCoin.Four:
						switch (appIs) {
						case AppIs.iPhone:
								if (isPaid == IsPaid.No) {
										id = Inapp_API.inapp_1200coins_iphone;
								} else {
										id = Inapp_API.inapp_1200coins_iphone_paid;
								}
								break;
						case AppIs.iPad:
								if (isPaid == IsPaid.No) {
										id = Inapp_API.inapp_1200coins_ipad;
								} else {
										id = Inapp_API.inapp_1200coins_ipad_paid;
								}
								break;
						case AppIs.Android:
								id = Inapp_API.inapp_1200coins_google;
								break;
						case AppIs.Amazon:
								if (isPaid == IsPaid.No) {
										id = Inapp_API.inapp_1200coins_amazon;
								} else {
										id = Inapp_API.inapp_1200coins_amazon_paid;
								}
								break;
//						case AppIs.AmazonHD:
//								if (isPaid == IsPaid.No) {
//										id = Inapp_API.inapp_1200coins_amazonhd;
//								} else {
//										id = Inapp_API.inapp_1200coins_amazonhd_paid;
//								}
//								break;
						}
						break;
				}
				if (purch)
						Purch (id);
				return id;
		}

		void Purch (string id)
		{
				if (adSocialController != null) {
						adSocialController.Purch (id);
				}
		}

		public void ConsumeGoogleIABProduct (string _id)
		{
				#if UNITY_ANDROID && !Amazon_Platform
				if (CSGlobal.google_purchases == null) {
						GoogleIAB.purchaseProduct (_id);
						Debug.Log ("@@@@ purchaseProduct : " + _id);
				} else {
						bool purchased = false;
						// find the item.
						for (int i = 0; i < CSGlobal.google_purchases.Count; i++) {
						GooglePurchase p = CSGlobal.google_purchases [i];
								if (p.productId.CompareTo (_id) == 0 && p.purchaseState == GooglePurchase.GooglePurchaseState.Purchased) {
										purchased = true;
										Debug.Log ("@@@@ cosumeIAP : " + _id);
										GoogleIAB.consumeProduct (_id);
								}
						}

						if (!purchased) {
								GoogleIAB.purchaseProduct (_id);
						Debug.Log ("@@@@ purchaseProduct : " + _id);

						}
				}
				#endif 
		}

		public void PurchaseSuccessed (string id)
		{
				int increasing_count = 0; 

				Debug.Log ("purchase Successed called with id = " + id);

				string id_100 = PurchCoins (PurchCoin.One, false);
				string id_350 = PurchCoins (PurchCoin.Two, false);
				string id_750 = PurchCoins (PurchCoin.Three, false);
				string id_1200 = PurchCoins (PurchCoin.Four, false);


				if (id.CompareTo (id_100) == 0) {
						increasing_count = 120;
				} else if (id.CompareTo (id_350) == 0) {
						increasing_count = 455; 
				} else if (id.CompareTo (id_750) == 0) {
						increasing_count = 1050; 
				} else if (id.CompareTo (id_1200) == 0) { 
						increasing_count = 1800; 
				}
				if (ScoreManager.instance) {
						ScoreManager.instance.SetPartyCoinsCount (increasing_count);
				}	
		}

		public void IAB_billingSupportedEvent ()
		{

		Debug.Log("Billing supported");
				#if UNITY_ANDROID
				googleIAB_enabled = true;
				List<string> productIdentifiers = new List<string> ();
				productIdentifiers.Add (PurchCoins (PurchCoin.One, false));
				productIdentifiers.Add (PurchCoins (PurchCoin.Two, false));
				productIdentifiers.Add (PurchCoins (PurchCoin.Three, false));
				productIdentifiers.Add (PurchCoins (PurchCoin.Four, false));
				GoogleIAB.queryInventory (productIdentifiers.ToArray ());
				#endif
		}

	public void PurchaseCancelled (string error)
		{
				Debug.Log ("purchase cancelled called with error = " + error);
		}

		public void PurchaseFailed (string error)
		{
				Debug.Log ("purchase failed called with error = " + error);
		}

		#endregion

		void InitApp ()
		{
				if (Application.platform == RuntimePlatform.Android) {
					#if Amazon_Platform
					appIs = AppIs.Amazon;
					#else
					appIs = AppIs.Android;
					#endif
				} else if (Application.platform == RuntimePlatform.IPhonePlayer) {
						if (Screen.width == 640 || Screen.width == 320 || Screen.height == 568 || Screen.height == 568 * 2) {
								appIs = AppIs.iPhone;
						} else {
								appIs = AppIs.iPad;
						}
				}

		}

		public void AppReview () //Выводим просьбу оставить отзыв
		{
				if (ScoreManager.instance) {
						ScoreData data = ScoreManager.instance.GetScoreData ();
						string str_review = "";
						str_review = "Congratulations! You made it to Level " + data.Level + "! Please rate or review " + StringData.gameName + " to give us valuable feedback!";
			
						string app_id = GetReviewValByPlatform ();		
						if (adSocialController != null) {	
								adSocialController.AppReview (app_id, str_review);
						}
				}
		}

		public bool isAmazon ()
		{
				if (appIs == AppIs.Amazon || appIs == AppIs.AmazonHD)
						return true;
				else
						return false;
				
		}

		#region Ad


		void TimeSetSmalBanner ()
		{
				if (adSocialController != null) {
						if (!adSocialController.ViewSmalBanner ()) {
								Invoke ("TimeSetSmalBanner ", 0.5f);
						}
				}
		}

		public void ViewBanner ()
		{
				if (isPaid == IsPaid.No) {
						TimeSetSmalBanner ();
				}
		}

		#endregion

		string GetReviewValByPlatform ()
		{
				string pfix = "";
				switch (appIs) {
				case AppIs.iPhone:
						pfix = Inapp_API.appid_iphone;
						if (isPaid == IsPaid.Yes) {
								pfix = Inapp_API.appid_iphone_paid;
						}
						break;
				case AppIs.iPad:
						pfix = Inapp_API.appid_ipad;
						if (isPaid == IsPaid.Yes) {
								pfix = Inapp_API.appid_ipad_paid;
						}
						break;				
				}	
				return pfix;
		}

		string GetLeaderboardValByPlatform ()
		{
				string id = "";
				switch (appIs) {
				case AppIs.iPhone:
						if (isPaid == IsPaid.No) {
								id = Leaderboard_API.leaderboard_iphone;
						} else {
								id = Leaderboard_API.leaderboard_iphone_paid;
						}
						break;
				case AppIs.iPad:
						if (isPaid == IsPaid.No) {
								id = Leaderboard_API.leaderboard_ipad;
						} else {
								id = Leaderboard_API.leaderboard_ipad_paid;
						}
						break;
				case AppIs.Android:

						id = Leaderboard_API.leaderboard_android;

						break;
				case AppIs.Amazon:
						if (isPaid == IsPaid.No) {
								id = Leaderboard_API.leaderboard_amazon;
						} else {
								id = Leaderboard_API.leaderboard_amazon_paid;
						}
						break;
//				case AppIs.AmazonHD:
//						if (isPaid == IsPaid.No) {
//								id = Leaderboard_API.leaderboard_amazonhd;
//						} else {
//								id = Leaderboard_API.leaderboard_amazonhd_paid;
//						}
//						break;
				}		
				return id;
		}

		public string GetMoPubByPlatform ()
		{			
				if (appIs == AppIs.iPhone) {
						return Mopub_API.mopub_appid_iphone;
				} else if (appIs == AppIs.iPad) {
						return Mopub_API.mopub_appid_ipad;
				} else {
						return Mopub_API.mopub_appid_android;
				}
		}

		public void PostFacebook ()
		{		

				if (ScoreManager.instance) {
						ScoreData data = ScoreManager.instance.GetScoreData ();
						string current_score = data.Score.Substring (0, data.Score.IndexOf ("/"));
						string current_level = data.Level;		
						string message = "Hello my Game";				
						message = "My current score on " + StringData.gameName + " is " + current_score + " in level " + current_level;

						#if UNITY_IPHONE || UNITY_ANDROID	
						if (adSocialController != null) {				
								Debug.Log ("~~~~ FacebookPostMessage : {" + message + "}");
//								adSocialController.FacebookPostMessage (message);
								Application.OpenURL (Facebook_API.osx_facebook_url);
						}
						#endif
				}

		}

		public void GetData ()
		{			
			#if UNITY_IPHONE	
				adSocialController.ViedUserData (GetLeaderboardValByPlatform ());		
			#endif

			#if UNITY_ANDROID
				#if Amazon_Platform
				// show amazone leaderboard.
				AGSLeaderboardsClient.ShowLeaderboardsOverlay();		  
				#else
				PlayGameServices.showLeaderboards();
				#endif
			#endif
		}

		public void ReportData (int score)
		{
			#if UNITY_IPHONE
				adSocialController.AddUserData (score, GetLeaderboardValByPlatform ());					
			#endif				 
			#if UNITY_ANDROID 
			#if Amazon_Platform 
			AGSLeaderboardsClient.SubmitScore(GetLeaderboardValByPlatform (), score);
			#else
			PlayGameServices.submitScore(GetLeaderboardValByPlatform (), score);
			#endif
			#endif
		}

		public void SetMoreGames ()
		{
				if (adSocialController != null) {
			adSocialController.ShowInterstitial();
			Debug.Log("chartboost");
				}
		}

		public void SetFreeGames ()
		{
				if (adSocialController != null) {
			adSocialController.ShowInterstitial();
			Debug.Log("chartboost");
						//	Upsight.sendContentRequest( "more_games", true, true );

						//PlayHavenManager.Instance.ContentRequest("more_games");
				}
		}

		void InitChartBoost ()
		{
				if (adSocialController != null) {
						switch (appIs) {
						case AppIs.iPhone:
						#if UNITY_IPHONE
								adSocialController.ChartBoostInit (Chartboost_API.chartboost_appid_iphone, Chartboost_API.chartboost_signature_iphone);
						#endif
								break;
						case AppIs.iPad:
						#if UNITY_IPHONE
								adSocialController.ChartBoostInit (Chartboost_API.chartboost_appid_ipad, Chartboost_API.chartboost_signature_ipad);
						#endif
								break;
						case AppIs.Android:
						#if UNITY_ANDROID
								adSocialController.ChartBoostInit (Chartboost_API.chartboost_appid_android, Chartboost_API.chartboost_signature_android);
						#endif
								break;
						case AppIs.Amazon:
						#if UNITY_ANDROID
								adSocialController.ChartBoostInit (Chartboost_API.chartboost_appid_amazon, Chartboost_API.chartboost_signature_amazon);
						#endif
								break;
						case AppIs.AmazonHD:
						#if Amazon_Platform
								adSocialController.ChartBoostInit (Chartboost_API.chartboost_appid_amazonhd, Chartboost_API.chartboost_signature_amazonhd);
						#endif
								break;
						}
						/*if (isPaid == IsPaid.No) {
								for (int i = 0; i < Chartboost_API.chartboost_names.Length; i++) {
										adSocialController.CacheInterval (Chartboost_API.chartboost_names [i]);									
								}
						}*/
				}
		}
	
	// add 2014-02-17
	public static void showAd()
	{
		if (instance != null)
			if (instance.isPaid == IsPaid.No)
				instance.adSocialController.ShowInterstitial();
	}

	void OnEnable() {
		// Remove event handlers
		
		Chartboost.didFailToLoadInterstitial += didFailToLoadInterstitial;
		Chartboost.didDismissInterstitial += didDismissInterstitial;
		Chartboost.didCloseInterstitial += didCloseInterstitial;
		Chartboost.didClickInterstitial += didClickInterstitial;
		Chartboost.didCacheInterstitial += didCacheInterstitial;
		Chartboost.shouldDisplayInterstitial += shouldDisplayInterstitial;
		Chartboost.didDisplayInterstitial += didDisplayInterstitial;
		Chartboost.didFailToLoadMoreApps += didFailToLoadMoreApps;
		Chartboost.didDismissMoreApps += didDismissMoreApps;
		Chartboost.didCloseMoreApps += didCloseMoreApps;
		Chartboost.didClickMoreApps += didClickMoreApps;
		Chartboost.didCacheMoreApps += didCacheMoreApps;
		Chartboost.shouldDisplayMoreApps += shouldDisplayMoreApps;
		Chartboost.didDisplayMoreApps += didDisplayMoreApps;
		Chartboost.didFailToRecordClick += didFailToRecordClick;
		Chartboost.didFailToLoadRewardedVideo += didFailToLoadRewardedVideo;
		Chartboost.didDismissRewardedVideo += didDismissRewardedVideo;
		Chartboost.didCloseRewardedVideo += didCloseRewardedVideo;
		Chartboost.didClickRewardedVideo += didClickRewardedVideo;
		Chartboost.didCacheRewardedVideo += didCacheRewardedVideo;
		Chartboost.shouldDisplayRewardedVideo += shouldDisplayRewardedVideo;
		
		Chartboost.didCompleteRewardedVideo += didCompleteRewardedVideo;
	}
	
	void OnDisable() {
		// Remove event handlers
		
		Chartboost.didFailToLoadInterstitial -= didFailToLoadInterstitial;
		Chartboost.didDismissInterstitial -= didDismissInterstitial;
		Chartboost.didCloseInterstitial -= didCloseInterstitial;
		Chartboost.didClickInterstitial -= didClickInterstitial;
		Chartboost.didCacheInterstitial -= didCacheInterstitial;
		Chartboost.shouldDisplayInterstitial -= shouldDisplayInterstitial;
		Chartboost.didDisplayInterstitial -= didDisplayInterstitial;
		Chartboost.didFailToLoadMoreApps -= didFailToLoadMoreApps;
		Chartboost.didDismissMoreApps -= didDismissMoreApps;
		Chartboost.didCloseMoreApps -= didCloseMoreApps;
		Chartboost.didClickMoreApps -= didClickMoreApps;
		Chartboost.didCacheMoreApps -= didCacheMoreApps;
		Chartboost.shouldDisplayMoreApps -= shouldDisplayMoreApps;
		Chartboost.didDisplayMoreApps -= didDisplayMoreApps;
		Chartboost.didFailToRecordClick -= didFailToRecordClick;
		Chartboost.didFailToLoadRewardedVideo -= didFailToLoadRewardedVideo;
		Chartboost.didDismissRewardedVideo -= didDismissRewardedVideo;
		Chartboost.didCloseRewardedVideo -= didCloseRewardedVideo;
		Chartboost.didClickRewardedVideo -= didClickRewardedVideo;
		Chartboost.didCacheRewardedVideo -= didCacheRewardedVideo;
		Chartboost.shouldDisplayRewardedVideo -= shouldDisplayRewardedVideo;
		
		
		Chartboost.didCompleteRewardedVideo -= didCompleteRewardedVideo;
	}
	
	void didCompleteRewardedVideo(CBLocation location, int reward) {
		ScoreManager.instance.SetRewardedCoinsCount (reward);
		Debug.Log("chartboostbvjhbk");
		Chartboost.cacheRewardedVideo(CBLocation.Default);
		
	}
	
	void didFailToLoadInterstitial(CBLocation location, CBImpressionError error) {
		Debug.Log(string.Format("didFailToLoadInterstitial: {0} at location {1}", error, location));
	}
	
	void didDismissInterstitial(CBLocation location) {
		Debug.Log("didDismissInterstitial: " + location);
	}
	
	void didCloseInterstitial(CBLocation location) {
		Debug.Log("didCloseInterstitial: " + location);
	}
	
	void didClickInterstitial(CBLocation location) {
		Debug.Log("didClickInterstitial: " + location);
	}
	
	void didCacheInterstitial(CBLocation location) {
		Debug.Log("didCacheInterstitial: " + location);
	}
	
	bool shouldDisplayInterstitial(CBLocation location) {
		Debug.Log("shouldDisplayInterstitial: " + location);
		return true;
	}
	
	void didDisplayInterstitial(CBLocation location){
		Debug.Log("didDisplayInterstitial: " + location);
	}
	
	void didFailToLoadMoreApps(CBLocation location, CBImpressionError error) {
		Debug.Log(string.Format("didFailToLoadMoreApps: {0} at location: {1}", error, location));
	}
	
	void didDismissMoreApps(CBLocation location) {
		Debug.Log(string.Format("didDismissMoreApps at location: {0}", location));
	}
	
	void didCloseMoreApps(CBLocation location) {
		Debug.Log(string.Format("didCloseMoreApps at location: {0}", location));
	}
	
	void didClickMoreApps(CBLocation location) {
		Debug.Log(string.Format("didClickMoreApps at location: {0}", location));
	}
	
	void didCacheMoreApps(CBLocation location) {
		Debug.Log(string.Format("didCacheMoreApps at location: {0}", location));
	}
	
	bool shouldDisplayMoreApps(CBLocation location) {
		Debug.Log(string.Format("shouldDisplayMoreApps at location: {0}", location));
		return true;
	}
	
	void didDisplayMoreApps(CBLocation location){
		Debug.Log("didDisplayMoreApps: " + location);
	}
	
	void didFailToRecordClick(CBLocation location, CBImpressionError error) {
		Debug.Log(string.Format("didFailToRecordClick: {0} at location: {1}", error, location));
	}
	
	void didFailToLoadRewardedVideo(CBLocation location, CBImpressionError error) {
		Debug.Log(string.Format("didFailToLoadRewardedVideo: {0} at location {1}", error, location));
	}
	
	void didDismissRewardedVideo(CBLocation location) {
		Debug.Log("didDismissRewardedVideo: " + location);
	}
	
	void didCloseRewardedVideo(CBLocation location) {
		Debug.Log("didCloseRewardedVideo: " + location);
	}
	
	void didClickRewardedVideo(CBLocation location) {
		Debug.Log("didClickRewardedVideo: " + location);
	}
	
	void didCacheRewardedVideo(CBLocation location) {
		Debug.Log("didCacheRewardedVideo: " + location);
	}
	
	bool shouldDisplayRewardedVideo(CBLocation location) {
		Debug.Log("shouldDisplayRewardedVideo: " + location);
		return true;
	}

}

