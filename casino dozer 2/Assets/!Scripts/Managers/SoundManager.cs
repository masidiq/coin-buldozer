﻿using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour
{
		public static SoundManager instance;
		GameManager gameManager;
		[HideInInspector]		
		AudioSource source;
		public AudioClip BonusCollected;
		public AudioClip CoinTable;
		public AudioClip CoinCollected;
		public AudioClip GiantActivated;
		public AudioClip LevelUp;
		public AudioClip PrizeGenerated;
		public AudioClip Regeneration;
		public AudioClip PrizeCollected;
		public AudioClip Shake;
		public AudioClip OpenWall;
		public AudioClip CloseWall;

		public enum SoundType
		{
				Sound_None,
				Sound_BonusXPCollected,
				Sound_CoinOnTable,
				Sound_CoinCollected,
				Sound_GiantActivated,
				Sound_Levelup,
				Sound_PrizeGenerated,
				Sound_Regeneration,
				Sound_PrizeCollected,
				Sound_Shake,
				Sound_OpenWall,
				Sound_CloseWall,
		}

		[HideInInspector]
		public SoundType soundType;

		void Awake ()
		{
				instance = this;
				gameManager = FindObjectOfType (typeof(GameManager))as GameManager;
		}

		public void AudioPlay (SoundType type)
		{
				if (gameManager.Mute) {
						var _clip = GetClip (type);
						source = gameObject.AddComponent<AudioSource> ();
						source.clip = _clip;
						source.Play ();
						Destroy (source, _clip.length);
				}
		}

		AudioClip GetClip (SoundType type)
		{
				AudioClip _clip = null;
				switch (type) {		
				case SoundType.Sound_None:
						_clip = null;
						break;
				case SoundType.Sound_BonusXPCollected:
						_clip = BonusCollected;
						break;
				case SoundType.Sound_CoinOnTable:
						_clip = CoinTable;
						break;
				case SoundType.Sound_CoinCollected:
						_clip = CoinCollected;
						break;
				case SoundType.Sound_GiantActivated:
						_clip = GiantActivated;
						break;
				case SoundType.Sound_Levelup:
						_clip = LevelUp;
						break;
				case SoundType.Sound_PrizeGenerated:
						_clip = PrizeGenerated;
						break;
				case SoundType.Sound_Regeneration:
						_clip = Regeneration;
						break;
				case SoundType.Sound_PrizeCollected:
						_clip = PrizeCollected;
						break;
				case SoundType.Sound_Shake:
						_clip = Shake;
						break;
				case SoundType.Sound_OpenWall:
						_clip = OpenWall;
						break;
				case SoundType.Sound_CloseWall:
						_clip = CloseWall;
						break;			
				}	
				return _clip;
		}

		public SoundType GetSoundTypeOfCoin (CoinBase _coinBase)
		{
				if (_coinBase.cointType == CoinBase.CoinType.Normal) {
						return SoundType.Sound_CoinCollected;
				}
	
				if (_coinBase.cointType == CoinBase.CoinType.CoinPrize) {
					
						return SoundType.Sound_PrizeCollected;
				}
	
				if (_coinBase.cointType == CoinBase.CoinType.GiantCoin) {
						return SoundType.Sound_None;
				}
				if (_coinBase.cointType == CoinBase.CoinType.XP15) {
						return SoundType.Sound_BonusXPCollected;
				}
				if (_coinBase.cointType == CoinBase.CoinType.CoinWall) {
						return SoundType.Sound_OpenWall;
				}
				return SoundType.Sound_CoinOnTable;
		}

		public void PlayPointSound (SoundType type, Vector3 pos)
		{
				if (gameManager.Mute) {
						var _clip = GetClip (type);
						if (_clip != null)
								AudioSource.PlayClipAtPoint (_clip, pos);
				}
		}

		public float GetPrizeSoundLenght ()
		{
				float _lenght = 0;
				if (PrizeCollected != null)
						_lenght = PrizeCollected.length;
				return _lenght;
		}

		public float GetLevelUpSoundLenght ()
		{
				float _lenght = 0;
				if (PrizeCollected != null)
						_lenght = LevelUp.length;
				return _lenght;
		}
}
