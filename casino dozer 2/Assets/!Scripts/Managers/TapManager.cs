﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TapManager : MonoBehaviour
{
		GameManager gameManager;
		CoinManager coinManager;
		Vector3 firstScreenPosition;
		Vector3 coinsPosition;
		Quaternion coinsRotation;
		private ScoreManager scoreManager;
		public Transform TargetFirstScreenTransform;
		public Transform TouchObjectTransform;
		public int TapCount;
		public List<UIDrawTexture> TapCoinsBar = new List<UIDrawTexture> ();
		public Texture2D EnableBarImage;
		public Texture2D DisableBarImage;

		void Start ()
		{
				gameManager = FindObjectOfType (typeof(GameManager)) as GameManager;
				if (gameManager == null)
						Debug.LogError ("GameManager not found!");
				coinManager = FindObjectOfType (typeof(CoinManager)) as CoinManager; 
				if (coinManager == null)
						Debug.LogError ("CoinManager not found!");
				scoreManager = FindObjectOfType (typeof(ScoreManager)) as ScoreManager;
				if (scoreManager == null)
						Debug.LogError ("GameManager not found!");
				if (TargetFirstScreenTransform) {
						firstScreenPosition = Camera.main.WorldToScreenPoint (TargetFirstScreenTransform.position);
						coinsPosition = TargetFirstScreenTransform.position;
						coinsRotation = TargetFirstScreenTransform.rotation;
				} else
						Debug.LogError ("TargetFirstScreenPosition is not assigned!");
				TapCount = 0;

			GameObject.Find("TargetObjects").SetActive(false);

		}

		void Update ()
		{
				if (gameManager.gameMode == GameManager.GameMode.Playing) {
						if (Input.GetMouseButtonDown (0)) {		
								ConvertTouchPoint (Input.mousePosition);		
						} else if (Input.touches.Length > 0) {
								Touch touch = Input.touches [0];
								switch (touch.phase) {
								case TouchPhase.Began:
										ConvertTouchPoint (touch.position);
										break;
								case TouchPhase.Moved:
										break;
								case TouchPhase.Ended:
										break;	
								default:
										break;
								}
						}
				}
		}

		Bounds bounds;

		void ConvertTouchPoint (Vector3 touchPosition)
		{
				if (gameManager.gameMode != GameManager.GameMode.Playing) {
						return;
				}
				Ray ray;
				RaycastHit hit;
				Vector3 pos = Camera.main.ScreenToWorldPoint (new Vector3 (touchPosition.x, touchPosition.y, firstScreenPosition.z));
				ray = Camera.main.ScreenPointToRay (touchPosition);
				if (TouchObjectTransform) {
						if (TouchObjectTransform.GetComponent<BoxCollider> () != null) {
								bounds = TouchObjectTransform.collider.bounds;
						} else
								Debug.LogError ("TouchObjectTransform collaidet is null!");				
				} else
						Debug.LogError ("TouchObjectTransform is not assigned!");
				if (bounds != null) {
						if (bounds.IntersectRay (ray)) {
								if (TapCount < TapCoinsBar.Count) {
										if (Physics.Raycast (ray, out hit)) {
												gameManager.SetTapZoneState (false);
												gameManager.SetZeroWait ();
												float zPosition = hit.point.z;

												if (zPosition < coinsPosition.z) {
														zPosition = coinsPosition.z;
												}												
												int count = Convert.ToInt32 (scoreManager.CoinsCount.UI_Data.Text);
												if (count > 0) {
														count--;
														TapBarViev ();
														scoreManager.CoinsCount.UI_Data.Text = count.ToString ();
														CoinBase coin = coinManager.CreateCoin ((int)(CoinBase.CoinType.Normal), 0, 0,
																                new Vector3 (pos.x, coinsPosition.y, zPosition), coinsRotation);
														gameManager.AddCon (coin);
												} else
														gameManager.ShopAlertView ();
										}
								}
						}
				}
		}

		void TapBarViev ()
		{				
				TapCoinsBar [TapCount].UI_Data.Image = DisableBarImage;
				TapCount++;
		}

		public IEnumerator EnableBar ()
		{
				if (TapCount > 0) {
						EnableTapBarImage (TapCount);
						yield return new WaitForSeconds (0.06f);
						if (TapCount > 0)
								StartCoroutine (EnableBar ());
				}
		}

		void EnableTapBarImage (int index)
		{			
				TapCount--;	
				TapCoinsBar [index - 1].UI_Data.Image = EnableBarImage;			
				
				if (TapCount < 0)
						TapCount = 0;
		}
}