﻿using System;
using System.Net.NetworkInformation;
using System.Reflection.Emit;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TapjoyUnity;
using System.Net;

public class GameManager : MonoBehaviour
{
		public static GameManager instance;

		public enum GameMode
		{
				Welcome,
				Playing,
				ViewMenu
		}

		[HideInInspector]
		public GameMode gameMode;

		#region Settings

		public Settings settings;

		#endregion

		#region Target Objects

		public TargetObjects targetObjects;

		#endregion

		#region Wall

		public Wall wall;
		private float timeWallView;
		private bool wallView = false;
		private bool wallBackMove = false;
		private float startTimeWallView;

		public void WallAction ()
		{
				if (!wallBackMove) {
						timeWallView += wall.WallTimeView;
						if (!wallView) {
								wallView = true;
								SetWallPosition (true);
								startTimeWallView = Time.time;
						}
				}
		}

		void DefaultWallMove ()
		{
				wallBackMove = false;
		}

		void SetWallPosition (bool up)
		{
				if (up) {
						if (!wall.isUp) {
								wall.isUp = true;
								var pos = new Vector3 (wall.RootWall.transform.position.x, wall.RootWall.transform.position.y + wall.DeltaMove, wall.RootWall.transform.position.z);
								iTween.MoveTo (wall.RootWall, pos, wall.MoveTime);
						}
				} else {
						if (wall.isUp) {
								wallBackMove = true;
								wall.isUp = false;
								var pos = new Vector3 (wall.RootWall.transform.position.x, wall.StartYPosition, wall.RootWall.transform.position.z);
								iTween.MoveTo (wall.RootWall, pos, wall.MoveTime);
								Invoke ("DefaultWallMove", wall.MoveTime + 0.1f);
						}
				}
		}

		void InitWall ()
		{
				if (wall.RootWall) {
						wall.StartYPosition = wall.RootWall.transform.position.y;
				} else {
						Debug.LogError ("The Wall is not assigned!");
				}
		}

		#endregion

		#region Plate

		private Vector3 startPlatePosition;

		enum MoveDirection
		{
				Forward,
				Back
		}

		private MoveDirection moveDirection;
		private float offset = 1f;
		bool pausePlate = false;

		void PlateMove ()
		{
				if (!pausePlate) {
						if (targetObjects.Plate) {
								if (moveDirection == MoveDirection.Forward) {
										if (offset != 1f)
												offset = 1f;
										if (targetObjects.Plate.position.z <= startPlatePosition.z - 10) {
												moveDirection = MoveDirection.Back;
										}
								} else {
										if (offset != -1f)
												offset = -1f;
										if (targetObjects.Plate.position.z >= startPlatePosition.z) {
												moveDirection = MoveDirection.Forward;
												// Включаем черепушки
												StartCoroutine (tapManager.EnableBar ());
										}
								}
								targetObjects.Plate.position += Vector3.back * offset * Time.deltaTime * settings.SpeedPlateMove;
						}
				}
		}

		#endregion

		#region Coins

		public List<CoinBase> CoinsList = new List<CoinBase> ();
		CoinManager coinManager;

		void CreateCoin (Transform targetTransform, int i, int j)
		{
				Vector3 pos = new Vector3 (targetTransform.position.x + settings.CoinSize.x * j,
						              targetTransform.position.y,
						              targetTransform.position.z - settings.CoinSize.x * i);
				CoinBase newCoin = coinManager.CreateCoin ((int)(CoinBase.CoinType.Normal), 0, 0, pos, new Quaternion (0.7f, 0f, 0f, -0.7f));
				if (newCoin != null) {
						newCoin.soundIsPlay = true;
						AddCon (newCoin);
				}
		}

		void CreateDefaultCoins ()
		{
				for (int i = 0; i < 2; i++) {
						for (int j = 0; j < 4; j++) {
								this.CreateCoin (targetObjects.FirstLineCoinPosition, i, j);
						}
				}
				for (int i = 0; i < 4; i++) {
						for (int j = 0; j < 7; j++) {
								this.CreateCoin (targetObjects.SecondLineCoinPosition, i, j);
						}
				}

				var prize = coinManager.CreateCoin ((int)(CoinBase.CoinType.CoinPrize), (int)(CoinBase.PrizeType.Prize3), (int)(CoinBase.PrizeSubType.PrizeSub_3), targetObjects.FirstPrizePosition.position, targetObjects.FirstPrizePosition.rotation);
				if (prize != null) {
						prize.soundIsPlay = true;
						AddCon (prize);
				}
				var bonus = coinManager.CreateCoin ((int)(CoinBase.CoinType.CoinShower), 0, 0, targetObjects.FirstBonusPosition.position, targetObjects.FirstBonusPosition.rotation);
				if (bonus != null) {
						bonus.soundIsPlay = true;
						AddCon (bonus);
				}
				scoreManager.SetScoreLoadData (null);
		}

		public int GetCoins ()
		{
				return CoinsList.Count;
		}

		public int GetBonusCount ()
		{
				int count = 0;
				if (CoinsList.Count > 0) {
						foreach (var item in CoinsList) {
								if (item.cointType != CoinBase.CoinType.Normal && item.cointType != CoinBase.CoinType.CoinPrize) {
										count++;
								}
						}
				}
				return count;
		}

		public void ReplaceRandonCoinWithBonus (int data)
		{
				var count = CoinsList.Count;
				if (count == 0) {
						return;
				}

				int index = UnityEngine.Random.Range (0, count);

				CoinBase coin = CoinsList [index];
				ReplaceCoinWithBonus (coin, data);
		}

		void ReplaceCoinWithBonus (CoinBase coin, int type)
		{
				var pos = coin.transform.position;
				var rot = coin.transform.rotation;

				if (coin.cointType == CoinBase.CoinType.Normal) {
						CoinsList.Remove (coin);
						Destroy (coin.transform.gameObject);
						coinManager.CreateBonusItem (pos, rot, type);
				}
		}

		public void AddCon (CoinBase coin)
		{
				CoinsList.Add (coin);
		}

		public void RemoveCoin (CoinBase coin)
		{
				CoinsList.Remove (coin);
		}

		#endregion

		#region Score

		private ScoreManager scoreManager;

		#endregion

		#region Tap zone

		TapManager tapManager;
		TapZoneViewer tapZoneViewer;
		float startWaitTime;

		public void SetTapZoneState (bool enabled)
		{
				if (enabled == tapZoneViewer.IsTouch) {
						tapZoneViewer.SetTouch (enabled);
				}
		}

		public void SetZeroWait ()
		{
				startWaitTime = Time.time;
		}

		public float GetPauseTime ()
		{
				return Time.time - startWaitTime;
		}

		public void SetPauseTime (float data)
		{
				startWaitTime = Time.time - data;
		}

		#endregion

		#region ReadWriteData

		ReadWriteDataManager readWriteDataManager;

		#endregion

		#region InfoViewManager

		private InfoViewManager infoViewManager;
		public int CollisionCount;

		public void SetViewText (InfoViewManager.ViewText _viewText)
		{
			if (InfoViewPanel.g_fShow)
				infoViewManager.SetViewerText (_viewText, CollisionCount);
		}

		#endregion

		#region Vibro

		bool vibroIsEnable = false;
		int vibroCount = 0;
		public Transform Bet;
		Vector3 startBetPosition;
		Quaternion startRotation;
		VibroSettings vibroSettings;
		[HideInInspector]
		public int VibroCollisionCount;
		public Bell BellObject;

		public void VibrateMainBet ()
		{
				if (!vibroIsEnable) {
						if (vibroSettings == null)
								vibroSettings = new VibroSettings ();						
						startBetPosition = Bet.position;
						startRotation = Bet.rotation;
						pausePlate = true;
						vibroIsEnable = true;				
				}
		}

		void Vibrating (int count)
		{
				if (vibroCount < count) {
						var pos = Bet.position;
						if (count == 10) {
								pos.x += vibroSettings.vibroOffsetGiant [vibroCount] [0] / 12.0f;
								pos.y += vibroSettings.vibroOffsetGiant [vibroCount] [1] / 8.0f;
								pos.z += vibroSettings.vibroOffsetGiant [vibroCount] [2] / 12.0f;
						} else {
								pos.x += vibroSettings.vibroOffset [vibroCount] [0] / 15.0f;
								pos.y += vibroSettings.vibroOffset [vibroCount] [1] / 15.0f;
								pos.z += vibroSettings.vibroOffset [vibroCount] [2] / 15.0f;
						}
						Bet.position = pos;
						if (vibroCount <= 5) {
								Bet.RotateAround (new  Vector3 (0f, 0f, -15f), Vector3.left, 0.4f);
								Bet.Rotate (-1, 0, 0);
						}				
						vibroCount += 1;
				} else {
						vibroCount = 0;
						vibroIsEnable = false;
						Bet.position = startBetPosition;
						Bet.rotation = startRotation;			
						pausePlate = false;	
				}
				foreach (var item in CoinsList) {
						if (count == 20) {
								item.transform.gameObject.rigidbody.AddForce (Vector3.back * 8, ForceMode.Acceleration); 
						} else {
								item.transform.gameObject.rigidbody.AddForce (Vector3.back * 3, ForceMode.Acceleration); 
						}
				}
		}

		#endregion

		#region Sound

		public bool Mute = true;

		#endregion
	#if UNITY_IOS 
		#region Notification

		public bool Notification = true;

		public void setNotification(bool isNotify)
		{
			Notification = isNotify;
			if (!isNotify)
			{
#if UNITY_IPHONE
			NotificationServices.ClearLocalNotifications();
			NotificationServices.CancelAllLocalNotifications();
#endif
			}
		}

		#endregion
	#endif
		#region Text View

		public GameObject ShopAlert;

		public void ShopAlertView ()
		{				
				if (ShopAlert)
		{
			//ShopAlert.renderer.material.renderQueue = 2000;
						//ShopAlert.SetActive (true);
			Debug.Log(" Ganti sama Ads");
		}
		}

		#endregion

		#region Timer

		public UILabel TimerLabel;
		const float gen_max_time = 30f;
		const float gen_max_time_offline = 300f;
		const int gen_max_coins	= 40;
		float time = gen_max_time;
		float deltaCounter;
		float currentTime;

		void Timer ()
		{
				if (Time.time - currentTime > 1) {
						currentTime = Time.time;
						time--;
						if (time == 0) {
						if (scoreManager.GetCoinsCount () < gen_max_coins) {
										scoreManager.SetCoinsCount ();
										if (SoundManager.instance) {
												var type = SoundManager.SoundType.Sound_Regeneration;
												SoundManager.instance.PlayPointSound (type, this.transform.position);
										}
								}
						}
						if (time < 0)
							time = gen_max_time;
						TimerLabel.UI_Data.Text = time.ToString ();
				}
		}

		#endregion

		#region Randomizer

		public Randomizer randomizer;

		#endregion

		TJPlacement tapjoy;
		bool contentIsReadyForPlacement = false;
		void Awake ()
		{
				instance = this;
				Physics.gravity = new Vector3 (0, -30.0f, 0);
				currentTime = Time.time;
				gameMode = GameMode.Playing; 
		}



		public static bool CheckForInternetConnection()
		{
			try
			{
				using (var client = new WebClient())
				{
					using (var stream = client.OpenRead("http://www.google.com"))
					{
						return true;
					}
				}
			}
			catch
			{
				return false;
			}
		}

		void Start ()
		{
				//if (AdManager.instance != null)
					//AdManager.instance.ViewBanner();

				if (CheckForInternetConnection()) {
					Tapjoy.Connect("f8mTWFc9SVmkYkuf55j6ZgECoMcigERtNUlBxPPvLA9NMN8DJ5Tqu4m0snUC");
				}
				
				InitWall ();
				readWriteDataManager = FindObjectOfType (typeof(ReadWriteDataManager)) as ReadWriteDataManager;
				if (readWriteDataManager == null)
						Debug.LogError ("ReadWriteDataManager not found!");
				infoViewManager = FindObjectOfType (typeof(InfoViewManager)) as InfoViewManager;
				if (infoViewManager == null)
						Debug.LogError ("InfoViewManager not found!");
				tapZoneViewer = FindObjectOfType (typeof(TapZoneViewer)) as TapZoneViewer;
				if (tapZoneViewer == null)
						Debug.LogError ("TapZoneViewer not found!");
				tapManager = FindObjectOfType (typeof(TapManager)) as TapManager;
				if (tapManager == null)
						Debug.LogError ("TapManager not found!");
				scoreManager = FindObjectOfType (typeof(ScoreManager)) as ScoreManager;
				if (scoreManager == null)
						Debug.LogError ("ScoreManager not found!");
				coinManager = FindObjectOfType (typeof(CoinManager)) as CoinManager;
				if (coinManager) {
						if (!readWriteDataManager.LoadFromUserData ())
								CreateDefaultCoins ();
				} else
						Debug.LogError ("CoinManager not found!");


				startWaitTime = Time.time;
				if (targetObjects.Plate) {
						startPlatePosition = targetObjects.Plate.position;
				} else {
						Debug.LogError ("Plate is not assigned!");
				}
				if (BellObject == null)
						Debug.LogError ("BellObject is not assigned!");
		}

	public void HandlePlacementContentDismiss(TJPlacement placement) {
		Debug.Log("C#: HandlePlacementContentDismiss");
		Application.Quit ();
	}

	public void HandlePlacementRequestSuccess(TJPlacement placement) {
		if (placement.IsContentAvailable()) {
			contentIsReadyForPlacement = true;
		} else {
			Debug.Log("C#: No content available for " + placement.GetName());
		}
	}

	#region Connect Delegate Handlers
	bool isConnected = false;
	public void HandleConnectSuccess() {
		Debug.Log("C#: Handle Connect Success");
		isConnected = true;
		tapjoy = TJPlacement.CreatePlacement("AppLaunch");
		tapjoy.RequestContent();
	}
	
	public void HandleConnectFailure() {
		Debug.Log("C#: Handle Connect Failure");
	}
	#endregion

	public void ShowAdOnExit(){
		if (contentIsReadyForPlacement) {
			tapjoy.ShowContent ();
		} else {
			AdBuddizClient.instance.Show (true);
		}
	}

	void OnEnable(){
		Tapjoy.OnConnectSuccess += HandleConnectSuccess;
		Tapjoy.OnConnectFailure += HandleConnectFailure;
		TJPlacement.OnContentDismiss += HandlePlacementContentDismiss;
		TJPlacement.OnRequestSuccess += HandlePlacementRequestSuccess;
	}


		void Update ()
		{
#if UNITY_ANDROID
//		if (Application.platform == RuntimePlatform.Android) {
//			if (Input.GetKeyUp(KeyCode.Escape)) {
//				if (Chartboost.CBBinding.onBackPressed())
//					return;
//			}
//		}
#endif

	


		if (tapZoneViewer != null && tapZoneViewer.IsTouch) {
						var delta = Time.time - startWaitTime;
						if (delta >= settings.WaitTouchTime) {
								SetTapZoneState (true);
						}
				}
				if (Input.GetKeyDown (KeyCode.Escape)) {

						this.OnApplicationPause (true);
						AlertManager.instance.Show();
				}
				if (wallView) {
						var deltaTime = Time.time - startTimeWallView;
						if (deltaTime >= timeWallView) {
								timeWallView = 0;
								wallView = false;
								SetWallPosition (false);
						}
				}
				PlateMove ();
				if (ShopAlert.activeSelf) {
						if (Input.GetMouseButton (0)) {
								if (ShopAlert) {
										var component = ShopAlert.GetComponent<UIPanel> ();
										if (component != null) {
												if (component.ClickIsContains (Input.mousePosition)) {
														ShopAlert.SetActive (false);
														
												}
										}
								}
						}
				} 
				Timer ();		
				if (vibroIsEnable) {
						Vibrating (VibroCollisionCount);
				}
				if (BellObject != null)
						BellObject.CheckBell ();
		}

	bool once_pause = false;
	float paused_time = 0;
		void OnApplicationPause (bool isPause)
		{
		if( isPause )
		{
			if (readWriteDataManager != null)
				readWriteDataManager.SaveToUserData ();
			once_pause = true;
			paused_time = Time.realtimeSinceStartup;
			// cancel all notifications first.
			#if UNITY_IPHONE
			NotificationServices.ClearLocalNotifications();
			NotificationServices.CancelAllLocalNotifications();
			#endif
			
			#if UNITY_IPHONE			
				if( this.Notification ) {


				// push local notification.
				int nAvailCoin = ScoreManager.instance.GetCoinsCount();
				Debug.Log("== pause : Avaiable=" + nAvailCoin + "   MaxGen=" + gen_max_coins);
				if( nAvailCoin < gen_max_coins ) {
					float offset = (gen_max_coins - nAvailCoin) * gen_max_time_offline;
					offset += 60; // for test // add 10 seconds.
					
					// schedule notification to be delivered for max regeneration limit.
					LocalNotification localNotify = new LocalNotification();
					localNotify.fireDate = System.DateTime.Now.AddSeconds(offset);
					localNotify.alertBody = "You've generated more coins! Come back and play!";
					localNotify.applicationIconBadgeNumber = -1;
					NotificationServices.ScheduleLocalNotification(localNotify);
					
					// add notification for one coin
					if( nAvailCoin < gen_max_coins - 1 ) { 
						offset = gen_max_time_offline;
						offset += 60; // for test // add 10 seconds.
						
						localNotify = new LocalNotification();
						localNotify.fireDate = System.DateTime.Now.AddSeconds(offset);
						localNotify.alertBody = "You've generated more coins! Come back and play!";
						NotificationServices.ScheduleLocalNotification(localNotify);              
					}
				}
			}
			#endif 

			Debug.Log("Enter to background");
		}
		else {
			Debug.Log("Enter to forground");
			
			#if UNITY_IPHONE
			// clear all notifications.
				if (NotificationServices.localNotificationCount > 0) { 
					Debug.Log("MYLOG : Local notification count = " + NotificationServices.localNotificationCount); 				
					Debug.Log(NotificationServices.localNotifications[0].alertBody);
				}
			
				NotificationServices.ClearLocalNotifications();
				NotificationServices.CancelAllLocalNotifications();
			#endif
			if (once_pause) {
				once_pause = false;
				float diff = Time.realtimeSinceStartup - paused_time;
				
				int nAvailCoin = ScoreManager.instance.GetCoinsCount();
				Debug.Log("== resume : Avaiable=" + nAvailCoin + "   MaxGen=" + gen_max_coins);
				if( nAvailCoin < gen_max_coins ) {
					int nCnt = (int)(diff / gen_max_time_offline);

					if (nAvailCoin + nCnt > gen_max_coins)
						nCnt = gen_max_coins - nAvailCoin;

					Debug.Log("== Cnt =" + nCnt);
					ScoreManager.instance.SetPartyCoinsCount(nCnt);
				}
			}
			}
	}

		void OnDisable ()
		{
			if (readWriteDataManager != null)
				readWriteDataManager.SaveToUserData ();
		}
}

[Serializable]
public class Wall
{
		[HideInInspector]
		public bool isUp = false;
		public float DeltaMove = 3.5f;
		public float MoveTime = 1f;
		public float StartYPosition;
		public GameObject RootWall;
		public float WallTimeView = 5f;
}

[Serializable]
public class Settings
{
		///<summary>
		/// View Test message
		///	</summary>
		public int StartCoinCount = 10;
		public int MaxBonusCount = 5;
		public int MaxCoinCount = 55;
		public int MaxPrizeCount = 6;
		public Vector2 CoinSize = new Vector2 (2.0f, 2.0f);
		public float WaitTouchTime = 10f;
		public float SpeedPlateMove = 10f;
		public int DefaultCoinsCount = 100;
		public int CoinsShowerCount = 6;
		public float GenerateTimeInteral = 100f;
		public int VibroCount = 20;
}

[Serializable]
public class TargetObjects
{
		public Transform FirstLineCoinPosition;
		public Transform SecondLineCoinPosition;
		public Transform FirstPrizePosition;
		public Transform FirstBonusPosition;
		public List<GameObject> TapViewer = new List<GameObject> ();
		public Transform Plate;
}

[Serializable]
public class VibroSettings
{
		public float[][] vibroOffset = new float[20][];
		public float[][] vibroOffsetGiant = new float[10][];

		public	VibroSettings ()
		{
				vibroOffset [0] = new float[] { 1.0f, 1.0f, -1.8f };
				vibroOffset [1] = new float[] { -0.5f, 3.0f, -1.0f };
				vibroOffset [2] = new float[] { 0.0f, 1.2f, -0.5f };
				vibroOffset [3] = new float[] { 0.8f, 1.6f, -0.4f };
				vibroOffset [4] = new float[] { 0.9f, 2.6f, -1.0f };
				vibroOffset [5] = new float[] { 0.2f, 1.5f, -0.5f };
				vibroOffset [6] = new float[] { -1.1f, 1.0f, -1.8f };
				vibroOffset [7] = new float[] { 0.0f, 0.0f, 0.0f };
				vibroOffset [8] = new float[] { 0.6f, 1.2f, -1.5f };
				vibroOffset [9] = new float[] { 0.8f, 1.6f, -0.4f };
				vibroOffset [10] = new float[] { 0.9f, 2.6f, -1.0f };
				vibroOffset [11] = new float[] { 0.2f, 1.5f, -0.5f };
				vibroOffset [12] = new float[] { -1.1f, 1.0f, -1.8f };
				vibroOffset [13] = new float[] { 0.0f, 0.0f, 0.0f };
				vibroOffset [14] = new float[] { 0.6f, 1.2f, -1.5f };
				vibroOffset [15] = new float[] { 0.8f, 1.6f, -0.4f };
				vibroOffset [16] = new float[] { 0.9f, 2.6f, -1.0f };
				vibroOffset [17] = new float[] { 0.2f, 1.5f, -0.5f };
				vibroOffset [18] = new float[] { -1.1f, 1.0f, -1.8f };
				vibroOffset [19] = new float[] { 0.0f, 0.0f, 0.0f };

				vibroOffsetGiant [0] = new float[] { 0.0f, 1.0f, -1.8f };
				vibroOffsetGiant [1] = new float[] { 0.0f, 2.5f, -3.5f };
				vibroOffsetGiant [2] = new float[] { 0.0f, 5.5f, -5.0f };
				vibroOffsetGiant [3] = new float[] { 0.0f, 0.0f, 0.0f };
				vibroOffsetGiant [4] = new float[] { 0.0f, 0.0f, 0.0f };
				vibroOffsetGiant [5] = new float[] { 0.0f, 0.0f, 0.0f };
				vibroOffsetGiant [6] = new float[] { 0.0f, 0.0f, 0.0f };
				vibroOffsetGiant [7] = new float[] { 0.0f, 0.0f, 0.0f };
				vibroOffsetGiant [8] = new float[] { 0.0f, 0.0f, 0.0f };
				vibroOffsetGiant [9] = new float[] { 0.0f, 0.0f, 0.0f };
		}
}

[Serializable]
public class Randomizer
{
		public float BorderCreatePrize = 0.3f;
}
