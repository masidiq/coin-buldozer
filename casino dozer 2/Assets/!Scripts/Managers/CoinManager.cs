﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CoinManager : MonoBehaviour
{

		#region Coin Prefabs

		public GameObject Bonus1;
		public GameObject Coingiant;
		public GameObject Coingift;
		public GameObject Coinshower;
		public GameObject Coinwalls;
		public GameObject Coinxp15;
		public GameObject Coinxp30;
		public GameObject Coinxp50;
		public GameObject Giantcoin;
		public GameObject Giantcoin_pos;
		public GameObject CoinWall_left;
		public GameObject CoinWall_right;
		public GameObject Coinnormal;

		#endregion

		// Other Prefabs
		public Transform CoinZeroPosition;
		public Transform ExtraCoinPosition;
		//Other
		List<Vector3> StartCoinPositions = new List<Vector3> ();
		List<Vector3> ExtraCoinPositionsList = new List<Vector3> ();
		GameManager gameManager;
		private ScoreManager scoreManager;
		private PrizeManager prizeManager;

		void Awake ()
		{
				gameManager = FindObjectOfType (typeof(GameManager)) as GameManager;
				if (gameManager == null) {
						Debug.LogError ("GameManager not found!");
				}
				scoreManager = FindObjectOfType (typeof(ScoreManager)) as ScoreManager;
				if (scoreManager == null) {
						Debug.LogError ("GameManager not found!");
				}  
				prizeManager = FindObjectOfType (typeof(PrizeManager)) as PrizeManager;
				if (prizeManager == null) {
						Debug.LogError ("GameManager not found!");
				} 
		}

		void Start ()
		{
				InitStartCoinsPosition ();
		}

		void InitStartCoinsPosition ()
		{
				for (int i = 0; i < gameManager.settings.StartCoinCount; i++) {
						switch (i) {
						case 0:
								if (CoinZeroPosition)
										StartCoinPositions.Add (CoinZeroPosition.localPosition);
								else
										Debug.LogError ("CoinZeroPosition is not assigned!");
								break;
						case 1:
								StartCoinPositions.Add (new Vector3 (CoinZeroPosition.localPosition.x + 6f, CoinZeroPosition.localPosition.y, CoinZeroPosition.localPosition.z));
								break;
						case 2:
								StartCoinPositions.Add (new Vector3 (CoinZeroPosition.localPosition.x, CoinZeroPosition.localPosition.y, CoinZeroPosition.localPosition.z - 2f));
								break;
						case 3:
								StartCoinPositions.Add (new Vector3 (CoinZeroPosition.localPosition.x + 6f, CoinZeroPosition.localPosition.y, CoinZeroPosition.localPosition.z - 2f));
								break;
						case 4:
								StartCoinPositions.Add (new Vector3 (CoinZeroPosition.localPosition.x + 2f, CoinZeroPosition.localPosition.y + 1f, CoinZeroPosition.localPosition.z));
								break;
						case 5:
								StartCoinPositions.Add (new Vector3 (CoinZeroPosition.localPosition.x + 4f, CoinZeroPosition.localPosition.y + 1f, CoinZeroPosition.localPosition.z));
								break;
						case 6:
								StartCoinPositions.Add (new Vector3 (CoinZeroPosition.localPosition.x + 2f, CoinZeroPosition.localPosition.y + 1f, CoinZeroPosition.localPosition.z - 2f));
								break;
						case 7:
								StartCoinPositions.Add (new Vector3 (CoinZeroPosition.localPosition.x + 4f, CoinZeroPosition.localPosition.y + 1f, CoinZeroPosition.localPosition.z - 2f));
								break;
						case 8:
								StartCoinPositions.Add (new Vector3 (CoinZeroPosition.localPosition.x + 6f, CoinZeroPosition.localPosition.y + 1f, CoinZeroPosition.localPosition.z));
								break;
						case 9:
								StartCoinPositions.Add (new Vector3 (CoinZeroPosition.localPosition.x, CoinZeroPosition.localPosition.y + 1f, CoinZeroPosition.localPosition.z - 2f));
								break;

						}
				}
				for (int i = 0; i < gameManager.settings.StartCoinCount; i++) {
						switch (i) {
						case 0:
								if (ExtraCoinPosition)
										StartCoinPositions.Add (ExtraCoinPosition.localPosition);
								else
										Debug.LogError ("ExtraCoinPosition is not assigned!");
								break;
						case 1:
								ExtraCoinPositionsList.Add (new Vector3 (ExtraCoinPosition.localPosition.x + 6f, ExtraCoinPosition.localPosition.y, ExtraCoinPosition.localPosition.z));
								break;
						case 2:
								ExtraCoinPositionsList.Add (new Vector3 (ExtraCoinPosition.localPosition.x, ExtraCoinPosition.localPosition.y, ExtraCoinPosition.localPosition.z - 2f));
								break;
						case 3:
								ExtraCoinPositionsList.Add (new Vector3 (ExtraCoinPosition.localPosition.x + 6f, ExtraCoinPosition.localPosition.y, ExtraCoinPosition.localPosition.z - 2f));
								break;
						case 4:
								ExtraCoinPositionsList.Add (new Vector3 (ExtraCoinPosition.localPosition.x + 2f, ExtraCoinPosition.localPosition.y + 1f, ExtraCoinPosition.localPosition.z));
								break;
						case 5:
								ExtraCoinPositionsList.Add (new Vector3 (ExtraCoinPosition.localPosition.x + 4f, ExtraCoinPosition.localPosition.y + 1f, ExtraCoinPosition.localPosition.z));
								break;
						case 6:
								ExtraCoinPositionsList.Add (new Vector3 (ExtraCoinPosition.localPosition.x + 2f, ExtraCoinPosition.localPosition.y + 1f, ExtraCoinPosition.localPosition.z - 2f));
								break;
						case 7:
								ExtraCoinPositionsList.Add (new Vector3 (ExtraCoinPosition.localPosition.x + 4f, ExtraCoinPosition.localPosition.y + 1f, ExtraCoinPosition.localPosition.z - 2f));
								break;
						case 8:
								ExtraCoinPositionsList.Add (new Vector3 (ExtraCoinPosition.localPosition.x + 6f, ExtraCoinPosition.localPosition.y + 1f, ExtraCoinPosition.localPosition.z));
								break;
						case 9:
								ExtraCoinPositionsList.Add (new Vector3 (ExtraCoinPosition.localPosition.x, ExtraCoinPosition.localPosition.y + 1f, ExtraCoinPosition.localPosition.z - 2f));
								break;

						}
				}
		}

		public bool GenerateRandomBonus ()
		{
			if (scoreManager.isBasicLevel())
			{
				prizeManager.GenerateRandomPrize ();
				scoreManager.SetScoreCoinsUp (1);
				scoreManager.SetCoinsCount ();
				//ShowBonusGiant ();
				return false;
			}
				float data = Random.Range (0.0f, 10.9f);
				if (data < 3.0) {
						//		Debug.Log ("Random bonus is failed as random range is ignored.");
						return false;
				}

				int newData = (int)(data - 3f);
			
				// check setting.
				int coinCount = gameManager.GetCoins ();

				if (coinCount >= gameManager.settings.MaxCoinCount) {
						//		Debug.Log ("Random bonus is failed as coin count is already exceed.");
						return false;
				}
				int bonusCount = gameManager.GetBonusCount ();

				if (bonusCount >= gameManager.settings.MaxBonusCount) {
						//		Debug.Log ("Random bonus is failed as bonus count is already exceed.");
						return false;
				}
				int bonusType = newData++;

				if (bonusType == (int)CoinBase.CoinType.CoinShower) {
						if (coinCount + 10 >= gameManager.settings.MaxCoinCount) {
								//				Debug.Log ("Random bonus is failed as coin count is already exceed when coinshower bonus is collected.");
								return false;
						}
				} else if (bonusType == (int)CoinBase.CoinType.CoinGift) {
						if (coinCount + 10 >= gameManager.settings.MaxCoinCount) {
								//				Debug.Log ("Random bonus is failed as coin count is already exceed when coingift bonus is collected.");
								return false;
						}
				}
				gameManager.ReplaceRandonCoinWithBonus (newData++);
				return true;
		}

		public void CreateBonusItem (Vector3 pos, Quaternion rot, int type)
		{
				CoinBase newCoin = CreateCoin (type, 0, 0, pos, rot);
				gameManager.AddCon (newCoin);
		}

		public CoinBase CreateCoin (int coinType, int prizeType, int prizeSubType, Vector3 pos, Quaternion rot)
		{
				CoinBase newCoin = null;
				GameObject prefab = null;
				switch (coinType) {
				case 0:
						prefab = Coinnormal;
						break;
				case 1:
						prefab = Bonus1;
						break;
				case 2:
						prefab = Coinwalls;
						break;
				case 3:
						prefab = Coinshower;
						break;
				case 4:
						prefab = Coingift;
						break;
				case 5:
						prefab = Coingiant;
						break;
				case 6:
						prefab = Coinxp15;
						break;
				case 7:
						prefab = Coinxp30;
						break;
				case 8:
						prefab = Coinxp50;
						break;
				case 9:
						prefab = Giantcoin;
						break;
				case 10:
						int i_prizetype = prizeType + 1;
						int i_subtype = prizeSubType + 1;						
						prizeManager.GenerateTypePrize (i_prizetype, i_subtype, pos, rot);  						
						break;
				}
				if (prefab) {
						GameObject coinObject = (GameObject)Instantiate (prefab, pos, rot);
						coinObject.transform.parent = transform;
						newCoin = coinObject.GetComponent<CoinBase> ();
						newCoin.cointType = (CoinBase.CoinType)coinType;
						newCoin.prizeType = (CoinBase.PrizeType)prizeType;
						newCoin.prizeSubType = (CoinBase.PrizeSubType)prizeSubType;
				}
				return newCoin;
		}

		public void OnCoinCollected (CoinBase coin)
		{
				if (coin.cointType == CoinBase.CoinType.Normal) {
						gameManager.CollisionCount++;
						scoreManager.SetScoreCoinsUp (1);
						ShowNormalCoin ();
						scoreManager.SetCoinsCount ();
						return;
				}
				switch (coin.cointType) {
				case CoinBase.CoinType.BonusCoin1:
			Debug.Log ("OnCoinCollected: BonusCoin1");
						scoreManager.SetScoreCoinsUp (2);
						scoreManager.SetCoinsCount ();
						ShowBonusCoin ();
						break;
				case CoinBase.CoinType.CoinWall:
			Debug.Log ("OnCoinCollected: CoinWall");
						gameManager.WallAction ();
						scoreManager.SetScoreCoinsUp (1);
						scoreManager.SetCoinsCount ();
						ShowBonusWall ();
						break;
				case CoinBase.CoinType.CoinGift:
			Debug.Log ("OnCoinCollected: CoinGift");
						for (int j = 0; j < 4; j++) {
								CoinBase _coin = CreateCoin ((int)CoinBase.CoinType.CoinWall + j, 0, 0, ExtraCoinPositionsList [j], ExtraCoinPosition.rotation);
								gameManager.AddCon (_coin);
								scoreManager.SetScoreCoinsUp (1);
								scoreManager.SetCoinsCount ();
								ShowBonusGift ();
						}
						break;
				case CoinBase.CoinType.CoinShower:
			Debug.Log ("OnCoinCollected: CoinShower");
						var count = gameManager.settings.CoinsShowerCount;
						for (int i = 0; i < count; i++) {
								CoinBase _coinShower = CreateCoin ((int)(CoinBase.CoinType.Normal), 0, 0, new Vector3 (-3 + i, 10 + i * 1.5f, 18), Quaternion.identity);
								_coinShower.transform.localEulerAngles = new Vector3 (Random.Range (0, 90), Random.Range (0, 90), Random.Range (0, 90));
								gameManager.AddCon (_coinShower);
								scoreManager.SetScoreCoinsUp (1);
								scoreManager.SetCoinsCount ();
								ShowCoinShower ();
						}
						break;
				case CoinBase.CoinType.BonusCoinGiant:
			Debug.Log ("OnCoinCollected: BonusCoinGiant");
						CoinBase _coinGiant = CreateCoin ((int)(CoinBase.CoinType.GiantCoin), 1, 0, new Vector3 (0, 10, 18), Quaternion.identity);
						gameManager.AddCon (_coinGiant);
						scoreManager.SetScoreCoinsUp (1);
						scoreManager.SetCoinsCount ();
						ShowBonusGiant ();
                // check prize parameter 
                // for test Global.param_giant_to_more_coins_count = 8;
                //	if( Global.param_giant_to_more_coins_count > 0 ) {  
                // param_giant_to_more_coins_count is start from 1. more coins should start from 2.
                //	var count : int = 2 + Global.param_giant_to_more_coins_count - 1;
                //	for( j = 0 ; j < count; j ++ ) { 
                //		c = this.createCoin( CoinType.Normal, PrizeType.Prize1, 0, extra_coin_pos[j], extra_coin_first_pos.rotation);


						break;
				case CoinBase.CoinType.GiantCoin:					
			Debug.Log ("OnCoinCollected: GiantCoin");
						prizeManager.GenerateRandomPrize ();
						scoreManager.SetScoreCoinsUp (1);
						scoreManager.SetCoinsCount ();
						ShowBonusGiant ();
						break;
				case CoinBase.CoinType.XP15:
				case CoinBase.CoinType.XP30:
				case CoinBase.CoinType.XP50:
			Debug.Log ("OnCoinCollected xp15");
						scoreManager.SetScoreCoinsUp (15);
						scoreManager.SetCoinsCount ();
						ShowBonusXP ();
						break;
				case CoinBase.CoinType.CoinPrize:
			Debug.Log ("prize..............");
						prizeManager.AddPrizeToList (coin);    
						break;
				default:
			Debug.Log ("OnCoinCollected: default "+ (int)coin.cointType);
						break;
				}
		}

		private bool show = false;

		void ShowNormalCoin ()
		{
				gameManager.SetViewText (InfoViewManager.ViewText.Coin);
				if (!show) {
						show = true;
						Invoke ("DefaultShow", 2f);
				}
		}

		void ShowBonusCoin ()
		{
				gameManager.SetViewText (InfoViewManager.ViewText.BonusCoin);
		}

		void ShowBonusXP ()
		{
				gameManager.SetViewText (InfoViewManager.ViewText.BonusXP);
		}

		void ShowBonusWall ()
		{
				gameManager.SetViewText (InfoViewManager.ViewText.CoinsWall);
		}

		void ShowBonusGift ()
		{
				gameManager.SetViewText (InfoViewManager.ViewText.Gift);
		}

		void ShowBonusGiant ()
		{
				gameManager.SetViewText (InfoViewManager.ViewText.GiantCoins);
		}

		void ShowCoinShower ()
		{
				gameManager.SetViewText (InfoViewManager.ViewText.CoinsShower);
		}

		void DefaultShow ()
		{
				show = false;
		}
}
