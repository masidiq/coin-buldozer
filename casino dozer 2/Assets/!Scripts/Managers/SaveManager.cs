﻿using UnityEngine;
using System.IO;
using System;
using System.Collections;
using System.Collections.Generic;
using JsonFx;
using JsonFx.Json;

public class SaveManager
{
		private static SaveManager _saveManager;
		GameManager gameManager;
		CoinManager coinManager;

		public static SaveManager saveManager {
				get {
			if (_saveManager == null)
				_saveManager = new SaveManager();
			return _saveManager;
				}
		}

		public void SaveData (GameManager manager, ScoreData scoreData, PrizeRootTypeList prizeList)
		{
				CteateData (manager, scoreData, prizeList);
		}

		public UserData LoadData ()
		{
				UserData userData = null;

				if (File.Exists (savePath)) {
						StreamReader fileReader = new StreamReader (savePath);
						string serializeData = fileReader.ReadToEnd ();
						userData = JsonReader.Deserialize<UserData> (serializeData);
						fileReader.Close ();
				}
				return userData;
		}

		private static string savePath {
				get {
						string path = "";
						path = Application.persistentDataPath + "/PlayerData.txt";

#if UNITY_IPHONE
						//var _path = Application.dataPath.Substring (0, Application.dataPath.Length - 5);
						//_path = path.Substring(0, path.LastIndexOf('/'));
						//_path += "/Documents"+"/PlayerData.txt";;
						//path = _path;
#endif

						return path;
				}
		}

		public bool FirstPlay ()
		{
				return !File.Exists (savePath);
		}

		void CteateData (GameManager manager, ScoreData scoreData, PrizeRootTypeList prizeList)
		{
				UserData _userData = new UserData ();
				_userData.Mute = manager.Mute;
				_userData.PauseTime = manager.GetPauseTime ();
				_userData.Level = scoreData.Level;
				_userData.LastExitTime = SaveTime ();
				_userData.CoinsCount = scoreData.CoinsCount;
				_userData.Score = scoreData.Score;
				_userData.PrizeCollection = prizeList;
				if (manager.CoinsList.Count > 0) {
						for (int i = 0; i < manager.CoinsList.Count; i++) {
								CoinData coin = new CoinData ();
								coin.CoinName = "Coin " + i.ToString ();
								coin.CoinType = (int)manager.CoinsList [i].cointType;
								coin.PrizeType = (int)manager.CoinsList [i].prizeType;
								coin.PrizeSubType = (int)manager.CoinsList [i].prizeSubType;
								var pos = manager.CoinsList [i].transform.position;
								coin.PositionX = pos.x;
								coin.PositionY = pos.y;
								coin.PositionZ = pos.z;
								var rot = manager.CoinsList [i].transform.rotation;
								coin.RotationX = rot.x;
								coin.RotationY = rot.y;
								coin.RotationZ = rot.z;
								coin.RotationW = rot.w;
								if (coin != null)
										_userData.Coins.Add (coin);
						}
				}
				if (_userData != null) {
						JsonWriterSettings settings = new JsonWriterSettings ();
						settings.PrettyPrint = true;
						JsonFx.Json.JsonDataWriter writer = new JsonDataWriter (settings);
						StringWriter wr = new StringWriter ();
						writer.Serialize (wr, _userData);
						string _data = wr.ToString ();
						StreamWriter fileWriter = File.CreateText (savePath);
						fileWriter.Write (_data);
						fileWriter.Close ();
				}
		}

		#region Convert to json format

		private static string convetPath {
				get {
						string path = "";
						path = Application.persistentDataPath + "/PlayerPrefs.txt";

#if UNITY_IPHONE
						//var _path = Application.dataPath.Substring (0, Application.dataPath.Length - 5);
						//_path = path.Substring(0, path.LastIndexOf('/'));
						//_path += "/Documents"+"/PlayerData.txt";;
						//path = _path;
#endif

						return path;
				}
		}

		public bool Convert ()
		{
				bool convert = false;
				if (File.Exists (convetPath)) {
						convert = true;
				}
				return convert;
		}

		public void DeleteData ()
		{
				if (File.Exists (convetPath)) {
						File.Delete (convetPath);
				}
		}

		#endregion

		string SaveTime ()
		{
				string time = "";
				DateTime saveNow = DateTime.Now;	
				time =	saveNow.ToString ();	
				return time;
		}
}

[System.Serializable]
public class UserData
{
		public bool Mute;
		public bool IsFirstPlay = true;
		public float PauseTime;
		public List<CoinData> Coins = new List<CoinData> ();
		public string Level;
		public string CoinsCount;
		public string Score;
		public PrizeRootTypeList PrizeCollection;
		public string LastExitTime;
		public bool Notification; // add new
}

[System.Serializable]
public class CoinData
{
		public string CoinName;
		public int CoinType;
		public int PrizeType;
		public int PrizeSubType;
		public float PositionX;
		public float PositionY;
		public float PositionZ;
		public float RotationX;
		public float RotationY;
		public float RotationZ;
		public float RotationW;
}
