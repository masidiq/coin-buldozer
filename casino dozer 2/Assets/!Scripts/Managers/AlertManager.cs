﻿using UnityEngine;
using System.Collections;

public class AlertManager : MonoBehaviour {
	public static AlertManager instance;
	public GameObject alertFrame;

	void Start () {
		if (instance == null)
			instance = this;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Show(){
		alertFrame.gameObject.SetActive (true);
	}

	public void QuitGame(){
		GameManager.instance.ShowAdOnExit ();
	}

	public void Hide(){
		alertFrame.gameObject.SetActive (false);
	}
}
