﻿using System;
using System.Diagnostics;
using UnityEngine;
using System.Collections;
using Debug = UnityEngine.Debug;

public class ScoreManager : MonoBehaviour
{
		public static ScoreManager instance;
		public UILabel CoinsCount;
		public UILabel Level;
		public UILabel Score;
		public UIDrawTexture ScoreTexture;
		public float MaxScaleScoreTexture;
		int targetStartScore = 100;
		int currenctCoins = 0;
		int currentLevelMaxCoins;
		int oldLevelMaxCoins;
		InfoViewManager infoViewManager;

		void Awake ()
		{
				instance = this;
				infoViewManager = FindObjectOfType (typeof(InfoViewManager))as InfoViewManager;
				if (infoViewManager == null)
						Debug.LogError ("InfoViewManager is not found!");
				DontDestroyOnLoad (this);
		}

		void Start ()
		{
				Invoke ("InitLevelBar", 1f);
		}

		public void InitLevelBar ()
		{
				var text = Score.UI_Data.Text;
				var part1Tex = text.Substring (0, text.IndexOf ("/"));
				var part2Text = text.Substring (text.IndexOf ("/") + 1, text.Length - text.IndexOf ("/") - 1);
				SetScoreTexture (Convert.ToInt32 (Level.UI_Data.Text), Convert.ToInt32 (part1Tex), Convert.ToInt32 (part2Text));
		}

		int SetLevel ()
		{
				int level = 0;
				if (Level) {
						int count = Convert.ToInt32 (Level.UI_Data.Text);
						count++;
						level = count;
						Level.UI_Data.Text = count.ToString ();
				} else
						Debug.LogError ("Level Label is NotConvertedAttribute assigned!");
				return level;
		}

	public void SetScoreCoinsUp (int _count)
	{
		if (Score) {
			var text = Score.UI_Data.Text;
			string part1Tex = text.Substring (0, text.IndexOf ("/"));
			string part2Text = text.Substring (text.IndexOf ("/") + 1, text.Length - text.IndexOf ("/") - 1);

			int count = Convert.ToInt32 (part1Tex);
			count += _count;
			currenctCoins = count;
			SetScoreTexture (Convert.ToInt32 (Level.UI_Data.Text), count, currentLevelMaxCoins);						
			Score.UI_Data.Text = count.ToString () + "/" + currentLevelMaxCoins.ToString ();
			if (count >= currentLevelMaxCoins) {
				nGiantCount = 0;
				LevelUp (count, currentLevelMaxCoins);
				infoViewManager.ViewLevelUp ();
				//AdManager.instance.ReportData(count); 
			}
		} 
		else
			Debug.LogError ("Score Label is NotConvertedAttribute assigned!");
	}

	public void SetCoinsCount ()
	{
		if (CoinsCount) {
			int count = Convert.ToInt32 (CoinsCount.UI_Data.Text);
			count++;
			CoinsCount.UI_Data.Text = count.ToString ();
		} else
			Debug.LogError ("CoinsCount Label is NotConvertedAttribute assigned!");
	}

	public void SetRewardedCoinsCount (int coin)
	{
		Debug.LogError ("CoinsCount");

		if (CoinsCount) {
			int count = Convert.ToInt32 (CoinsCount.UI_Data.Text);
			count += coin;

			CoinsCount.UI_Data.Text = count.ToString ();
		} else
			Debug.LogError ("CoinsCount Label is NotConvertedAttribute assigned!");
	}

		public void SetPartyCoinsCount (int _count)
		{
				if (CoinsCount) {
						int count = Convert.ToInt32 (CoinsCount.UI_Data.Text);
						count += _count;
						CoinsCount.UI_Data.Text = count.ToString ();
				} else
						Debug.LogError ("CoinsCount Label is NotConvertedAttribute assigned!");
		}

		public int GetCoinsCount ()
		{
				return Convert.ToInt32 (CoinsCount.UI_Data.Text);
		}

	public int GetCurCount() {
		var text = Score.UI_Data.Text;
		string part1Tex = text.Substring (0, text.IndexOf ("/"));

		int count = Convert.ToInt32 (part1Tex);
		return count;
	}

	public int GetLevelMaxCoinsCount()
	{
		return currentLevelMaxCoins;
	}

		public void SetScoreLoadData (ScoreData scoreData)
		{
				if (scoreData != null) {
						Level.UI_Data.Text = scoreData.Level;
						CoinsCount.UI_Data.Text = scoreData.CoinsCount;
						Score.UI_Data.Text = scoreData.Score;								
						try {
								var tempString = Score.UI_Data.Text.Substring (0, Score.UI_Data.Text.IndexOf ("/"));
						} catch {
								int newLevelCoinsCount = CalculateLevelCoins (Convert.ToInt32 (Level.UI_Data.Text));
								Score.UI_Data.Text = Score.UI_Data.Text + "/" + newLevelCoinsCount.ToString ();
						}				
				} else {
						Level.UI_Data.Text = "1";
//						if (AdManager.instance.isPaid == AdManager.IsPaid.Yes)
//							CoinsCount.UI_Data.Text = "100";	
//						else
							CoinsCount.UI_Data.Text = "42";
						Score.UI_Data.Text = "0/100";
						currenctCoins = 0;
				}
				var text = Score.UI_Data.Text;			
				string part1Tex = text.Substring (0, text.IndexOf ("/"));
				string part2Text = text.Substring (text.IndexOf ("/") + 1, text.Length - text.IndexOf ("/") - 1);
				int count = Convert.ToInt32 (part1Tex);		
				currentLevelMaxCoins = Convert.ToInt32 (part2Text);				
				SetScoreTexture (Convert.ToInt32 (Level.UI_Data.Text), count, currentLevelMaxCoins);
		}

		public ScoreData GetScoreData ()
		{
				ScoreData scoreData = new ScoreData ();
				scoreData.Level = Level.UI_Data.Text;
				scoreData.CoinsCount = CoinsCount.UI_Data.Text;
				scoreData.Score = Score.UI_Data.Text;
				return scoreData;
		}

		void SetScoreTexture (int level, int coins, int levelCoins)
		{		
				var scale = ScoreTexture.transform.localScale;
				oldLevelMaxCoins = CalculateOldMaxCoinsCount (level);
				float delta = coins - oldLevelMaxCoins;
				float deltaGlobal = levelCoins - oldLevelMaxCoins;				
			
		scale.x = 0.9f * delta / deltaGlobal * UIManager.instance.GetScale();
			ScoreTexture.transform.localScale = scale;
//		Debug.Log("@@@@@ SetScoreTexture ("+coins+"/"+levelCoins+", "+oldLevelMaxCoins+") ["+scale.x+"]");
		}

		void LevelUp (int count, int currentLevelCoins)
		{		
				int newLevelCoinsCount = CalculateLevelCoins (Convert.ToInt32 (Level.UI_Data.Text) + 1);
				int level = SetLevel ();
//				if (AdManager.instance)
//					AdManager.instance.ViewAd ();

				if (level % 10 == 0) {
				} else if (level % 5 == 0) {
//						if (AdManager.instance)
//								AdManager.instance.AppReview ();
				}
				Score.UI_Data.Text = count.ToString () + "/" + newLevelCoinsCount.ToString ();	
				currentLevelMaxCoins =	newLevelCoinsCount;
				SetScoreTexture (level, count, newLevelCoinsCount);	
				if (SoundManager.instance) {
						var type = SoundManager.SoundType.Sound_Levelup;
						SoundManager.instance.PlayPointSound (type, this.transform.position);
				}
				
		}

		int CalculateLevelCoins (int level)
		{
				int count = 0;
				var deltaLevel = 3;	
				int deltsLevelsCoins = 0;							
				var deltaCoinsCount = 100;	
				for (int i = 1; i < level; i++) {						
						deltsLevelsCoins += deltaLevel;
				}					
				count = currentLevelMaxCoins + deltaCoinsCount + deltsLevelsCoins;	
								
				return count;
		}

		int CalculateOldMaxCoinsCount (int level)
		{
				int count = 0;
				var deltaLevel = 3;	
				int deltsLevelsCoins = 0;							
				var deltaCoinsCount = 100;
				for (int i = 1; i < level; i++) {						
						deltsLevelsCoins += deltaLevel;
				}
				count = currentLevelMaxCoins - deltaCoinsCount - deltsLevelsCoins;
				return count;
		}
		int nGiantCount = 0;
		public bool isBasicLevel()
		{
			if (currentLevelMaxCoins >= 310)
				return false;

		int nTemp = UnityEngine.Random.Range(0, 3);
			if (nTemp == 1)
			{
				if (nGiantCount < 4)
				{
					nGiantCount ++;
				return true;
				}
			}
			return false;


		//return true;
	}
}

public class ScoreData
{
		public string Level;
		public string CoinsCount;
		public string Score;
}
