﻿using UnityEngine;
using System;
using System.Collections;

public class ReadWriteDataManager : MonoBehaviour
{
		private GameManager gameManager;
		private SoundManager soundManager;
		private CoinManager coinManager;
		private SaveManager saveManager;
		private ScoreManager scoreManager;
		private PrizeManager prizeManager;

		void Start ()
		{
				gameManager = FindObjectOfType (typeof(GameManager)) as GameManager;
				if (gameManager == null)
						Debug.LogError ("GameManager is not found!");
				soundManager = FindObjectOfType (typeof(SoundManager)) as SoundManager;
				if (soundManager == null)
						Debug.LogError ("SoundManager is not found!");
				coinManager = FindObjectOfType (typeof(CoinManager)) as CoinManager;
				if (coinManager == null)
						Debug.LogError ("CoinManager is not found!");
				if (saveManager == null)
						saveManager = new SaveManager ();
				scoreManager = FindObjectOfType (typeof(ScoreManager)) as ScoreManager;
				if (scoreManager == null)
						Debug.LogError ("ScoreManager is not found!");
				prizeManager = FindObjectOfType (typeof(PrizeManager)) as PrizeManager;
				if (prizeManager == null)
						Debug.LogError ("PrizeManager is not found!");

				DontDestroyOnLoad (this);
		}

		public void SaveToUserData ()
		{
				/*Debug.Log ("++++ Saving global player prefs+++++");		}			
//MyPlayerPrefs.SetInt ("topjoyCoins", g_tapjoy_coins);		
*/
#if UNITY_IOS 
				MyPlayerPrefs.SetBool("notification", gameManager.Notification);
#endif

				if (saveManager != null)
						saveManager.SaveData (gameManager, scoreManager.GetScoreData (), prizeManager.GetPrizeCollection ());
#if UNITY_IOS 
								gameManager.Notification = MyPlayerPrefs.GetBool("notification");
#endif
		}

		public bool LoadFromUserData ()
		{
				if (saveManager == null)
						saveManager = new SaveManager ();
				bool load = false;

				if (saveManager != null) {
						if (saveManager.Convert ()) {
								load = true;
								LoadFromPlayerPrefs ();
						} else {
								UserData userData = saveManager.LoadData ();
								if (userData != null) {
										load = true;
										gameManager.Mute = userData.Mute;
										gameManager.SetPauseTime (userData.PauseTime);
										ScoreData score = new ScoreData ();
										score.Level = userData.Level;
										score.CoinsCount = AddCoinsFromTime (userData.CoinsCount, userData.LastExitTime);
										score.Score = userData.Score;
										scoreManager.SetScoreLoadData (score);
										if (userData.Coins.Count > 0) {
												for (int i = 0; i < userData.Coins.Count; i++) {
														CoinData coinData = userData.Coins [i];
														var pos = new Vector3 (coinData.PositionX, coinData.PositionY, coinData.PositionZ);
														var rot = new Quaternion (coinData.RotationX, coinData.RotationY, coinData.RotationZ, coinData.RotationW);
														CoinBase coin = coinManager.CreateCoin (coinData.CoinType, coinData.PrizeType, coinData.PrizeSubType, pos, rot);
														if (coin != null) {
																coin.soundIsPlay = true;
																coin.EnableParticle (false);
																coin.EnableGiantProcessed (true);
																gameManager.AddCon (coin);
														}
												}
										}
										prizeManager.SetPrizeCollection (userData.PrizeCollection);
								}
						}

				}
		#if UNITY_IOS 
				gameManager.Notification = MyPlayerPrefs.GetBool("notification", true);
		#endif
				return load;
				/*Debug.Log ("+++++Loading global player prefs++++");
			
//g_disable_notification = MyPlayerPrefs.GetBool("disable_notification");

    //g_tapjoy_coins = MyPlayerPrefs.GetInt("topjoyCoins");
}*/
		}

		string AddCoinsFromTime (string _count, string time)
		{
				string _newCount = _count;
				int count = Convert.ToInt32 (_count);
				if (count < 40) {
						DateTime oldTime = Convert.ToDateTime (time);
						DateTime newTime = DateTime.Now;
						var deltaTime = (int)(newTime - oldTime).TotalSeconds;
						int addCoins = deltaTime / 300;			
						int currentCount = Convert.ToInt32 (_count);
						var newCount = currentCount + addCoins;
						if (newCount > 40)
								newCount = 40;
						_newCount = newCount.ToString ();
				}
				return _newCount;
		}

		void LoadFromPlayerPrefs ()
		{
				Debug.Log ("+++++Loading global player prefs++++");
				gameManager.Mute = MyPlayerPrefs.GetBool ("mute");

				//g_disable_notification = MyPlayerPrefs.GetBool("disable_notification");

				int count = MyPlayerPrefs.GetInt ("coin_count");
				if (count == 0) {
						Debug.Log ("Coin count is not saved.");
						return;
				}

				ScoreData score = new ScoreData ();
				score.Level = MyPlayerPrefs.GetInt ("level").ToString ();
				score.CoinsCount = MyPlayerPrefs.GetInt ("available_coins").ToString ();
				score.Score = MyPlayerPrefs.GetInt ("score").ToString ();
				scoreManager.SetScoreLoadData (score);

				int i = 0;
				Vector3 pos;
				Quaternion rot;
				int type;
				int prize_type;
				int prize_subtype;

				for (i = 0; i < count; i++) {
						string name = "coin" + i.ToString ();
						string temp;
						temp = name + "_cointype";
						type = MyPlayerPrefs.GetInt (temp, (int)CoinBase.CoinType.Normal);
						temp = name + "_prizetype";
						prize_type = MyPlayerPrefs.GetInt (temp, (int)CoinBase.PrizeType.Prize1);
						temp = name + "_prizesubtype";
						prize_subtype = MyPlayerPrefs.GetInt (temp, (int)CoinBase.PrizeSubType.PrizeSub_1);
						temp = name + "_posx";
						pos.x = MyPlayerPrefs.GetFloat (temp);
						temp = name + "_posy";
						pos.y = MyPlayerPrefs.GetFloat (temp);
						temp = name + "_posz";
						pos.z = MyPlayerPrefs.GetFloat (temp);
						temp = name + "_rotx";
						rot.x = MyPlayerPrefs.GetFloat (temp);
						temp = name + "_roty";
						rot.y = MyPlayerPrefs.GetFloat (temp);
						temp = name + "_rotz";
						rot.z = MyPlayerPrefs.GetFloat (temp);
						temp = name + "_rotw";
						rot.w = MyPlayerPrefs.GetFloat (temp);
						CoinBase coin = coinManager.CreateCoin (type, prize_type, prize_subtype, pos, rot);

						if (coin != null) {
								coin.soundIsPlay = true;
								coin.EnableParticle (false);
								coin.EnableGiantProcessed (true);
								gameManager.AddCon (coin);
						}
				}

				#region PrizeLoad
				int _i = 0;
				int _j = 0;
				string _temp = "";
				PrizeRootTypeList prizeCollection = new PrizeRootTypeList ();
				for (_i = 0; _i < 10; _i++) {
						for (_j = 0; _j < 4; _j++) {
								_temp = "prize" + _i.ToString () + "_" + _j.ToString ();
								switch (_j) {
								case 0:
										prizeCollection.SubTypeList [_i].SubType1List = MyPlayerPrefs.GetInt (_temp);
										break;
								case 1:
										prizeCollection.SubTypeList [_i].SubType2List = MyPlayerPrefs.GetInt (_temp);
										break;
								case 2:
										prizeCollection.SubTypeList [_i].SubType3List = MyPlayerPrefs.GetInt (_temp);
										break;
								case 3:
										prizeCollection.SubTypeList [_i].SubType4List = MyPlayerPrefs.GetInt (_temp);
										break;
								}
								Debug.Log (_temp + ": " + MyPlayerPrefs.GetInt (_temp));
						}
				}
				prizeManager.SetPrizeCollection (prizeCollection);
				#endregion

				// 20130402
				//	g_tapjoy_coins = MyPlayerPrefs.GetInt("topjoyCoins");
				if (saveManager != null)
						saveManager.DeleteData ();
		}
}
