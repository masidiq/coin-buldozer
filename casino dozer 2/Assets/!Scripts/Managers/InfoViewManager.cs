﻿using UnityEngine;
using System.Collections;

public class InfoViewManager : MonoBehaviour
{
		public enum ViewText
		{
				Coin,
				BonusCoin,
				CoinsWall,
				CoinsShower,
				GiantCoins,
				Gift,
				BonusXP,
				Prize
		}

		public ViewText viewText;
		public ViewTargetObject viewTargetObject;
		public CoinsSeries coinsSeries;
		private GameObject vieNormalCoin;
		private GameManager gameManager;
		private CoinManager coinManager;
		public GameObject LevelUpViewPanel;

		public void SetViewerText (ViewText _viewText, int count)
		{
				switch (_viewText) {
				case ViewText.Coin:
						if (!vieNormalCoin) {
								vieNormalCoin = (GameObject)Instantiate (viewTargetObject.Coin.gameObject);
								ViewCoinsText (vieNormalCoin.transform);
						}
						if (count == 2) {
								FixTexture (vieNormalCoin, coinsSeries.DubleCoins);
						} else if (count == 3) {
								FixTexture (vieNormalCoin, coinsSeries.TripleCoins);
						} else if (count > 3) {
								FixTexture (vieNormalCoin, coinsSeries.CoinsParty);
						}
						break;
				case ViewText.BonusCoin:
						var lImage = (GameObject)Instantiate (viewTargetObject.BonusCoin.gameObject);
						ViewCoinsText (lImage.transform);
						break;
				case ViewText.CoinsWall:
						var wallImage = (GameObject)Instantiate (viewTargetObject.CoinsWall.gameObject);
						ViewCoinsText (wallImage.transform);
						break;
				case ViewText.CoinsShower:
						var showerImage = (GameObject)Instantiate (viewTargetObject.Shower.gameObject);

						ViewCoinsText (showerImage.transform);
						break;
				case ViewText.GiantCoins:
						var giantImage = (GameObject)Instantiate (viewTargetObject.GiantCoins.gameObject);
						ViewCoinsText (giantImage.transform);
						break;
				case ViewText.Gift:
						var giftImage = (GameObject)Instantiate (viewTargetObject.Gift.gameObject);
						ViewCoinsText (giftImage.transform);
						break;
				case ViewText.BonusXP:
						var xpImage = (GameObject)Instantiate (viewTargetObject.BonusXP.gameObject);
						ViewCoinsText (xpImage.transform);
						break;
				case ViewText.Prize:						
						break;

				} 
		}

		void Start ()
		{
				if (!viewTargetObject.Coin) {
						Debug.LogError ("viewTargetObject is not assigned!");
				}				
				gameManager = FindObjectOfType (typeof(GameManager)) as GameManager;
				if (gameManager == null) {
						Debug.LogError ("GameManager not found!");
				}
				coinManager = FindObjectOfType (typeof(CoinManager)) as CoinManager;
				if (coinManager == null) {
						Debug.LogError ("CoinManager not found!");
				}
				if (LevelUpViewPanel == null)
						Debug.LogError ("LevelUpViewPanel is not assigned!");
		}

		void ViewCoinsText (Transform tr)
		{						
				if (tr.gameObject.GetComponent<UIDrawTexture> ()) {
						tr.gameObject.GetComponent<UIDrawTexture> ().enabled = true;
				}
		Vector3 vec = tr.gameObject.transform.localPosition;
		float rat = (UIManager.instance.GetRatio() / (Screen.width/(float)Screen.height));
		tr.gameObject.transform.localPosition = new Vector3 (vec.x, vec.y*rat, vec.z);

				StartCoroutine (MoveText (tr.gameObject, speedMove));
				StartCoroutine (DestroyNormalCoinsText (tr.gameObject));
		}

		float speedMove = 60f;

		IEnumerator MoveText (GameObject obj, float speed)
		{
				yield return null;
				if (obj != null) {
					obj.SetActive(InfoViewPanel.g_fShow);						
						var pos = obj.transform.localPosition;
						pos.y += Time.deltaTime * speed;
						if (speed > 0)
								speed -= Time.deltaTime * 25;
						obj.transform.localPosition = pos;
						StartCoroutine (MoveText (obj, speed));
				}
		}

		void FixTexture (GameObject obj, Texture2D texture)
		{
				var component = obj.GetComponent<UIDrawTexture> ();
				if (component != null) {
						component.UI_Data.Image = texture;
				}
		}

		IEnumerator DestroyNormalCoinsText (GameObject obj)
		{
				yield return new WaitForSeconds (3.5f);
				if (gameManager.CollisionCount > 1)
						coinManager.GenerateRandomBonus ();
				gameManager.CollisionCount = 0;
				Destroy (obj);
		}

		public void ViewLevelUp ()
		{
				LevelUpViewPanel.SetActive (true);
				float time = 0;
				if (SoundManager.instance) {
						time = SoundManager.instance.GetLevelUpSoundLenght ();
				}
				Invoke ("CloseLevelUp", time);
		}

		void CloseLevelUp ()
		{
			LevelUpViewPanel.SetActive (false);				
			AdManager.showAd();
		}
}

[System.Serializable]
public class ViewTargetObject
{
		public Transform Coin;
		public Transform BonusCoin;
		public Transform CoinsWall;
		public Transform GiantCoins;
		public Transform Gift;
		public Transform BonusXP;
		public Transform Prize;
		public Transform Shower;
}

[System.Serializable]
public class CoinsSeries
{
		public Texture2D DubleCoins;
		public Texture2D TripleCoins;
		public Texture2D CoinsParty;
}
