﻿using UnityEngine;
using System.Collections;

public class SpinWeelManager : MonoBehaviour
{
		public static SpinWeelManager instance;
		public UIButton StartButton;
		public UIButton StopButton;
		public int[] coinsCount;
		float angleVelocity = 0;
		float angleAcceleration = 0;
		float angleCurrent = 0;
		float maxVelocity;
		float prevTime;

		enum ActionState
		{
				Start,
				Spinning,
				Stoped,
				Stop
		}

		ActionState actionState;

		void Awake ()
		{
				instance = this;
		}

		public void InitWeelMode ()
		{
				if (GameManager.instance) {
						GameManager.instance.gameMode = GameManager.GameMode.ViewMenu;
				}
				angleVelocity = 0;
				angleAcceleration = 0;
				angleCurrent = 0;
				actionState = ActionState.Start;

		if (ClosePanel.Length > 0) {
			foreach (var item in ClosePanel)
				item.SetActive (false);
		}
		}

		public void SetState (WeelButton.Init init)
		{
				if (init == WeelButton.Init.Start) {
						StartWeel ();
				} else {
						StopWeel ();
				}
		}

		public void StartWeel ()
		{
				if (StartButton != null) {
						StartButton.transform.gameObject.SetActive (false);
				}
				if (StopButton != null) {
						StopButton.transform.gameObject.SetActive (true);
				}
				actionState = ActionState.Spinning;
				angleVelocity = 0;
				angleAcceleration = 250;
				maxVelocity = 360; 
				prevTime = Time.time;
		}

		public void StopWeel ()
		{
				if (actionState == ActionState.Spinning) {
						actionState = ActionState.Stoped;
						angleAcceleration = -180;
				}
		}

		float factor;
		float time;
		public UIDrawTexture texture_wheel;
		public UIDrawTexture texture_wheel_bg;
		public UIDrawTexture texture_pin;
		public UILabel ViewerText;
		public UIPanel SpeenWeel;
		public GameObject[] ClosePanel;


		void WeelRun (bool run)
		{		
				if (run) {
						time = Time.time;
						angleVelocity += (time - prevTime) * angleAcceleration * factor;
						if (angleVelocity >= maxVelocity) { 
								angleVelocity = maxVelocity;
						}
				
						angleCurrent += angleVelocity * (time - prevTime); 
						if (angleCurrent >= 360) { 
								int temp = (int)angleCurrent;
								temp = temp % 360; 

								angleCurrent = temp;
								angleCurrent += angleVelocity * (time - prevTime); 
						}
				}
				prevTime = time;				 
				DrawTexture ((Texture2D)texture_wheel.UI_Data.Image, texture_wheel.RectElement, angleCurrent);			
				DrawTexture ((Texture2D)texture_wheel_bg.UI_Data.Image, texture_wheel_bg.RectElement, 0);
				DrawTexture ((Texture2D)texture_pin.UI_Data.Image, texture_pin.RectElement, 0);
				DrawTexture ((Texture2D)StartButton.UI_Data.Style.normal.background, StartButton.RectElement, 0);
				DrawTexture ((Texture2D)StopButton.UI_Data.Style.normal.background, StopButton.RectElement, 0);			
		}

		void DrawTexture (Texture2D _texture, Rect _rect, float _angle)
		{
				Rect rect = _rect;
				Vector2 pivot = new  Vector2 (texture_wheel.RectElement.x + texture_wheel.RectElement.width / 2, texture_wheel.RectElement.y + texture_wheel.RectElement.height / 2);
				Texture2D texture = _texture;
				Matrix4x4 matrixBackup = GUI.matrix;
				GUIUtility.RotateAroundPivot (_angle, pivot);
				GUI.DrawTexture (rect, texture);
				GUI.matrix = matrixBackup;
		}

		void OnGUI ()
		{
				if (actionState == ActionState.Spinning) {
						factor = 1;
						WeelRun (true);
				} else if (actionState == ActionState.Stoped) {
						if (angleVelocity < 5) { 
								factor = 0.07f;
						} else if (angleVelocity < 10) { 
								factor = 0.085f;
						} else if (angleVelocity < 100) { 
								factor = 0.1f;
						}
						WeelRun (true);
						if (angleVelocity < 0) {
								angleVelocity = 0;
						}
						if (angleVelocity == 0) { 
								actionState = ActionState.Stop;
								var temp1 = angleCurrent;
								var delta = temp1 % 18;
											
								temp1 -= (delta);
								if (delta >= 9)
										temp1 += 18;

								angleCurrent = temp1; 
								int index = (int)angleCurrent / 18;
								var _count = coinsCount [index];

								if (ScoreManager.instance)
										ScoreManager.instance.SetPartyCoinsCount (_count);
								ViewText (_count);
								Invoke ("CloseWeel", 3f);
						}
				} else if (actionState == ActionState.Stop) {
						WeelRun (false);
				}
		}

		void ViewText (int _coinsCount)
		{
				if (ViewerText != null) {
						ViewerText.transform.gameObject.SetActive (true);
						ViewerText.UI_Data.Text = "You just won " + _coinsCount + " coins!";

				}
		}

		void CloseWeel ()
		{
				if (ViewerText != null) {
						ViewerText.transform.gameObject.SetActive (false);
				}
				if (StartButton != null) {
						StartButton.transform.gameObject.SetActive (true);
				}
				if (StopButton != null) {
						StopButton.transform.gameObject.SetActive (false);
				}
				if (SpeenWeel != null)
				{
					SpeenWeel.gameObject.SetActive (false);
					if (ClosePanel.Length > 0) {
						foreach (var item in ClosePanel) {
							item.SetActive (true);
					}
				}

			}
				if (GameManager.instance) {
						GameManager.instance.gameMode = GameManager.GameMode.Playing;
				}
				actionState = ActionState.Start;
		}


		
}



