﻿using UnityEngine;
using System.Collections;

public class TapZoneViewer : MonoBehaviour
{
	MeshRenderer render;
	[HideInInspector]
	public bool IsTouch = false;
	float startViewerTime = 0;

	void Start ()
	{
		render = GetComponent<MeshRenderer> ();
	}
	
	void Update ()
	{
		if (!IsTouch) {
			if (startViewerTime + 1 <= Time.time) {
				render.enabled = !render.enabled;
				startViewerTime = Time.time;
				
			}
		}
	}

	public void SetTouch (bool enable)
	{
		if (!enable) {
			render.enabled = false;
			IsTouch = true;
		} else {
			startViewerTime = Time.time;
			IsTouch = false;
		}		
	}
}
