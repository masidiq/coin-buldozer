﻿using UnityEngine;
using System.Collections;

public class InfoViewPanel : MonoBehaviour {
	public static bool g_fShow = true;
	// Use this for initialization
	void Start () {
		DontDestroyOnLoad(this);
	}
	
	// Update is called once per frame
	void Update () {
	
	}


	void OnEnable()
	{
		g_fShow = true;
	}

	void OnDisable()
	{
		g_fShow = false;
	}
}
