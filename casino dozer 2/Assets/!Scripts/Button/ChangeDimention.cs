﻿using UnityEngine;
using System.Collections;

public class ChangeDimention : MonoBehaviour {

	// Use this for initialization
	void Start () {
		//if(this.gameObject.name == "ContinueButton")
		if(Screen.height > 2000)
			this.gameObject.transform.localScale = new Vector3(0.8352425f * 2,0.97f * 2,1);
		else if(Screen.height > 1600)
			this.gameObject.transform.localScale = new Vector3(0.8352425f * 1.7f,0.97f * 1.7f,1);
		else
			this.gameObject.transform.localScale = new Vector3(0.8352425f,0.97f,1);

	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
