﻿using UnityEngine;
using System.Collections;

public class DisableEnableMenu : MonoBehaviour
{
		public enum ActionState
		{
				Enable,
				Disable
		}

		public ActionState actionState;
		public GameObject[] OpenMenu;
		public GameObject[] ClosedMenu;
	GameObject TargetObject;

	void Awake()
	{
		TargetObject = GameObject.Find("TargetObjects");
	}
		void SetPress ()
		{				

		if(this.gameObject.name == "1.CoinsKey")
		{
			if(TargetObject != null)
				TargetObject.SetActive(false);
		}


				if (OpenMenu.Length > 0) {
						foreach (var item in OpenMenu) {
								item.SetActive (true);
						}
				}
				if (ClosedMenu.Length > 0) {
			if(this.gameObject.name == "BackButton" || this.gameObject.name == "ContinueButton" || this.gameObject.name == "CancelButton" || this.gameObject.name == "OKButton")
			{
				if(TargetObject != null)
					TargetObject.SetActive(true);
			}


						foreach (var item in ClosedMenu) {
								item.SetActive (false);
						}
				}

		}

	void Update ()
	{
		if (Input.GetMouseButtonDown (0)) {
			if(this.gameObject.name == "Help")
			{
				foreach (var item in ClosedMenu) {
					item.SetActive (false);
				}
			}
		}
	}

}
