﻿using UnityEngine;
using System.Collections;

public class SellPrize : MonoBehaviour
{
		[HideInInspector]
		public int Type;
		[HideInInspector]
		public int SubType;

		public void SetPress ()
		{
				if (PrizeManager.instance)
						PrizeManager.instance.SellPrize (Type, SubType);
		}
}
