//#define Amazon_Platform
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ChartboostSDK;

public class AdSocial
{
		public  static AdSocial Instance{ get { return _instance; } }

		private static AdSocial _instance;

		public AdSocial ()
		{
		_instance = this;
		}

		#region Facebook

		public void InitFacebook ()
		{
				#if UNITY_IPHONE
				FacebookBinding.init ();
				#elif UNITY_ANDROID || Amazon_Platform			
				FacebookAndroid.init ();			
				#endif
		}

		public void FacebookPostMessage (string message)
		{
				#if UNITY_IPHONE
				if (FacebookBinding.isSessionValid ()) {
						Facebook.instance.postMessage (message, completionHandler);
				} else {					
						var permissions = new string[] {/* "publish_actions",*/ "user_games_activity" };
						FacebookBinding.loginWithReadPermissions (permissions);
				}
				
				#elif UNITY_ANDROID|| Amazon_Platform
				if (FacebookAndroid.isSessionValid ()) {
						Facebook.instance.postMessage (message, completionHandler);
				} else {
						FacebookAndroid.loginWithReadPermissions (new string[] { "user_games_activity"/*"publish_actions"*/ });
				}
				#endif
		}

		void completionHandler (string error, object result)
		{
				if (error != null) {
						Debug.LogError (error);
						#if UNITY_IPHONE
						EtceteraBinding.showAlertWithTitleMessageAndButton ("Error", "Facebook posting is not completed.", "OK");
						#elif UNITY_ANDROID
						EtceteraAndroid.showAlert ("Error", "Facebook posting is not completed.", "OK");
						#endif 
				} else {
						Prime31.Utils.logObject (result);
						Debug.Log (result.ToString ());
						#if UNITY_IPHONE
						EtceteraBinding.showAlertWithTitleMessageAndButton ("Facebook posting", "Facebook posting is completed successfully.", "OK");
						#elif UNITY_ANDROID
						EtceteraAndroid.showAlert ("Facebook posting", "Facebook posting is completed successfully.", "OK");
						#endif 
				}
		}

		#endregion

		#region View Message

		public void AppReview (string app_id, string _review)
		{
				#if UNITY_IPHONE							
				EtceteraBinding.askForReview ("App review", _review, "https://userpub.itunes.apple.com/WebObjects/MZUserPublishing.woa/wa/addUserReview?id=" + app_id + "&type=Purple+Software");				
				#elif UNITY_ANDROID
//				EtceteraAndroid.askForReviewNow ("App review", _review, AdManager.instance.isAmazon ());	
				#endif
		}

		#endregion

		#region Set, View Data

		#region Game Center

		#if UNITY_IPHONE
		private List<GameCenterLeaderboard> _leaderboards;
		private bool _hasLeaderboardData;
		#endif
	#if UNITY_ANDROID && Amazon_Platform
//		List<AmazonGameCircleExampleBase> gameCircleExampleMenus = new List<AmazonGameCircleExampleBase> ();
	#endif
	public void InitGameCenter ()
	{
	#if UNITY_IPHONE
		GameCenterBinding.authenticateLocalPlayer ();
	#endif
	#if UNITY_ANDROID
		#if Amazon_Platform	
			AGSClient.Init();
		#else
//			PlayGameServices.enableDebugLog( true );
//			PlayGameServices.authenticate();
		#endif
	#endif
	}

		public void AddUserData (int points, string ident)
		{
				#if UNITY_IPHONE	
				GameCenterBinding.reportScore (points, ident);	
				#elif UNITY_ANDROID
				#if Amazon_Platform
				#endif
				#endif
		}

		public void ViedUserData (string ident)
		{
				#if UNITY_IPHONE
				GameCenterBinding.showLeaderboardWithTimeScopeAndLeaderboard (GameCenterLeaderboardTimeScope.AllTime, ident);	
				#elif UNITY_ANDROID && Amazon_Platform
				AGSClient.ShowSignInPage();
				#endif
		}

		#endregion

		#endregion

		#region ChartBoost

		public void ChartBoostInit (string id, string signature)
		{
				#if UNITY_IPHONE
//				ChartboostBinding.init (id, signature);
//				ChartboostBinding.cacheInterstitial(Chartboost_API.chartboost_names [0]);
//				ChartboostBinding.cacheMoreApps ();
//		CBBinding.init(id, signature);
//		CBBinding.cacheInterstitial(Chartboost_API.chartboost_names [0]);
//		CBBinding.cacheMoreApps ();
		Chartboost.cacheInterstitial(CBLocation.Default);
		Chartboost.cacheRewardedVideo(CBLocation.Default);
		Debug.Log ("Chartboost init");
				#elif UNITY_ANDROID
		Debug.Log ("Chartboost init");
//			CBBinding.init();
//			CBBinding.cacheInterstitial(Chartboost_API.chartboost_names [0]);
//			CBBinding.cacheMoreApps ();
		Chartboost.cacheRewardedVideo(CBLocation.Default);
		Chartboost.cacheInterstitial(CBLocation.Default);
//				ChartBoostAndroid.init (id, signature);
//				ChartBoostAndroid.onStart();
//				ChartBoostAndroid.cacheInterstitial(Chartboost_API.chartboost_names [0]);
//				ChartBoostAndroid.cacheMoreApps ();
				#endif
		}

		public void CacheInterval (string i)
		{
				#if UNITY_IPHONE
//		CBBinding.cacheInterstitial(i);
	//	Chartboost.cacheRewardedVideo(CBLocation.Default);

	//	Chartboost.cacheInterstitial(i);

//				ChartboostBinding.cacheInterstitial (i);
				#elif UNITY_ANDROID
//		CBBinding.cacheInterstitial(i);
		Chartboost.cacheInterstitial(CBLocation.Default);
				#endif
		}

		public void ShowMoreApps ()
		{
				#if UNITY_IPHONE
//		CBBinding.showMoreApps ();
		Chartboost.showMoreApps(CBLocation.Default);

//				ChartboostBinding.showMoreApps ();
				#elif UNITY_ANDROID
//		CBBinding.showMoreApps ();
		Chartboost.showMoreApps(CBLocation.Default);
				#endif
		}
	
	public void ShowInterstitial()
	{
		#if UNITY_IPHONE
//		CBBinding.showInterstitial(Chartboost_API.chartboost_names [0]);
		Debug.Log("ShowInterstitial");
		Chartboost.showInterstitial(CBLocation.Default);

//		ChartboostBinding.showInterstitial(Chartboost_API.chartboost_names [0]);
		#else
//		CBBinding.showInterstitial(Chartboost_API.chartboost_names [0]);
		Chartboost.showInterstitial(CBLocation.Default);

		#endif
	}
	public void ShowVideoad()
	{
      #if UNITY_ANDROID
		Debug.Log("show video");
				Chartboost.showRewardedVideo (CBLocation.Default);
      #endif
		}

		#endregion

		#region Purch

		public void InitPurch (string[]products)
		{
			#if UNITY_IPHONE || UNITY_STANDALONE_OSX
				bool canMakePayments = StoreKitBinding.canMakePayments ();			
				StoreKitBinding.requestProductData (products);
			#endif
			#if UNITY_ANDROID
				#if Amazon_Platform
					Debug.Log ("@@@@@ AdSocial.InitPurch () = " + products);
					AmazonIAP.initiateItemDataRequest (products);
				#else
//					Debug.Log ("@@@@@ AdSocial.InitPurch () = " + Inapp_API.inapp_key_android);
					GoogleIAB.init (Inapp_API.inapp_key_android);
				#endif 
			#endif
		}

		public void Purch (string id)
		{
				#if UNITY_IPHONE
				StoreKitBinding.purchaseProduct (id, 1);
				#endif
				#if UNITY_ANDROID
				#if Amazon_Platform
				AmazonIAP.initiatePurchaseRequest( id );
				#else
//				if (AdManager.instance.googleIAB_enabled) {
//						Debug.Log ("@@@@@ AdSocial.Purch () = " + id);
//						AdManager.instance.ConsumeGoogleIABProduct (id);
//				} else {
//						Debug.Log ("@@@@@ AdSocial.Purch enabled false");
//				}
				#endif 
				#endif
		}

		#endregion

		#region View Ad

		#region AppLovin

		AppLovin appLovin;

		public void InitAllAd ()
		{
				#if UNITY_IPHONE
		AppLovin.SetSdkKey ("8GhoShoWG4vEoW5p4CT0NTLy6tuhNgLVy1HPk4w_9qDyLJlPWDQugnqodiT_OqVLsolZseT0Tnyp4hDavdSaPH");
				#elif UNITY_ANDROID && !UNITY_EDITOR
		AppLovin.SetSdkKey ("8GhoShoWG4vEoW5p4CT0NTLy6tuhNgLVy1HPk4w_9qDyLJlPWDQugnqodiT_OqVLsolZseT0Tnyp4hDavdSaPH");
				#endif
		}

		public void ViewBigApplovin ()
		{
				AppLovin.InitializeSdk ();
				#if UNITY_IPHONE
				AppLovin.ShowInterstitial ();
		#elif UNITY_ANDROID && !UNITY_EDITOR
				AppLovin.ShowInterstitial ();
				#endif
		}

		void ViewSmalApplovin ()
		{
		#if !UNITY_EDITOR
				AppLovin.InitializeSdk ();
		#endif
		if (Screen.width >= 768)
		{
			#if UNITY_IPHONE	
			AppLovin.SetAdWidth(728);
			AppLovin.ShowAd (AppLovin.AD_POSITION_CENTER, AppLovin.AD_POSITION_BOTTOM);
			#elif UNITY_ANDROID && !UNITY_EDITOR
			AppLovin.SetAdWidth(728);
			AppLovin.ShowAd (AppLovin.AD_POSITION_CENTER, AppLovin.AD_POSITION_BOTTOM);
			#endif

		}
		else
		{
				#if UNITY_IPHONE				
				AppLovin.ShowAd (AppLovin.AD_POSITION_CENTER, AppLovin.AD_POSITION_BOTTOM);
			#elif UNITY_ANDROID && !UNITY_EDITOR
				AppLovin.ShowAd (AppLovin.AD_POSITION_CENTER, AppLovin.AD_POSITION_BOTTOM);
				#endif
		}
		}
	
		#endregion


		public bool ViewSmalBanner ()
		{
				return	 CheckSmalAd ();
		}

		public bool SmalShow = false;
		static int numberSmalShow = 0;
		
	public void showBanner(bool isShow) {
//#if !UNITY_EDITOR
		if (isShow)
		{
			if (Screen.width < 768)
			{
#if !UNITY_EDITOR

				AppLovin.ShowAd();
#endif
			}
			else {
				#if UNITY_IOS
				MoPubBinding.showBanner(true);
				#endif
				#if UNITY_ANDROID && !UNITY_EDITOR

				MoPubAndroid.hideBanner(false);
				#endif
			}
		}
		else
		{
			if (Screen.width < 768)
			{
				#if !UNITY_EDITOR

				AppLovin.HideAd();
				#endif
			}
			else {
				#if UNITY_IPHONE
				MoPubBinding.showBanner(false);
				#endif
				#if UNITY_ANDROID && !UNITY_EDITOR

				MoPubAndroid.hideBanner(true);
				#endif
			}
		}
//#endif
	}

		bool CheckSmalAd ()
		{
				switch (numberSmalShow) {
				case 0:
						ViewSmalApplovin ();
						numberSmalShow++;
						break;
				case 1:
			#if !UNITY_EDITOR

						if (AppLovin.HasPreloadedInterstitial ()) {
								
						} else {
								if (true) {

										//ViewSmalApplovinBottom();
									if (Screen.width >= 768)
									{
										#if UNITY_IPHONE
										string id = AdManager.instance.GetMoPubByPlatform ();
										if (UnityEngine.Screen.width < 768) {									
												MoPubBinding.createBanner (MoPubBannerType.Size320x50, MoPubAdPosition.BottomCenter, id);
										} else {
												MoPubBinding.createBanner (MoPubBannerType.Size728x90, MoPubAdPosition.BottomCenter, id);
										}
										#endif
										#if UNITY_ANDROID
						string id = "ASU";
						try {
										MoPubAndroid.createBanner (id, MoPubAdPlacement.BottomCenter);
						} catch (System.Exception e) {
							Debug.Log("@@@@@@@ mopubandroid createbanner error");
						}
										#endif
									}

								}
						}
			#endif
						break;
				case 2:

						break;
				case 3:

						break;
				}
				return SmalShow;
		}

		#endregion





}
